ALTER TABLE ndic_traffic_restrictions
ALTER COLUMN "source" TYPE json
USING "source"::json;

ALTER TABLE ndic_traffic_info
ALTER COLUMN "source" TYPE json
USING "source"::json;

ALTER TABLE ndic_traffic_restrictions
ALTER COLUMN general_public_comment TYPE json
USING general_public_comment::json;

ALTER TABLE ndic_traffic_info
ALTER COLUMN general_public_comment TYPE json
USING general_public_comment::json;

ALTER TABLE ndic_traffic_restrictions DROP COLUMN IF EXISTS geom_alert_c_line;
ALTER TABLE ndic_traffic_restrictions DROP COLUMN IF EXISTS alert_c_direction;

ALTER TABLE ndic_traffic_info DROP COLUMN IF EXISTS geom_alert_c_line;
ALTER TABLE ndic_traffic_info DROP COLUMN IF EXISTS alert_c_direction;
