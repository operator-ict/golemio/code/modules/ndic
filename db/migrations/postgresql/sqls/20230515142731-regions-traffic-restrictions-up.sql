CREATE TABLE ndic_traffic_restrictions_regions (
	exchange json NULL,
	publication_time timestamptz NULL,
	publication_creator json NULL,
	situation_id varchar(100) NOT NULL,
	situation_version varchar(100) NOT NULL,
	situation_version_time timestamptz NULL,
	situation_confidentiality varchar(100) NULL,
	situation_information_status varchar(100) NULL,
	situation_urgency varchar(100) NULL,
	situation_record_version varchar(100) NOT NULL,
	situation_record_type varchar(100) NOT NULL,
	situation_record_id varchar(100) NOT NULL,
	situation_record_creation_time timestamptz NOT NULL,
	situation_record_version_time timestamptz NOT NULL,
	probability_of_occurrence varchar(100) NULL,
	"source" varchar(100) NULL,
	validity_status varchar(100) NULL,
	validity_overall_start_time timestamptz NULL,
	validity_overall_end_time timestamptz NULL,
	impact_capacity_remaining float4 NULL,
	impact_number_of_lanes_restricted int4 NULL,
	impact_number_of_operational_lanes int4 NULL,
	impact_original_number_of_lanes int4 NULL,
	impact_traffic_constriction_type varchar(100) NULL,
	impact_delays_type varchar(100) NULL,
	impact_delay_time_value float4 NULL,
	cause json NULL,
	general_public_comment text NULL,
	group_of_locations_type varchar(100) NULL,
	supplementary_positional_description json NULL,
	destination json NULL,
	alert_c_linear json NULL,
	linear_within_linear_element json NULL,
	global_network_linear json NULL,
	linear_extension json NULL,
	geom_gn_line public.geometry NULL,
	geom_openlr_line public.geometry NULL,
	situation_record_extension json NULL,
	number_of_obstructions int4 NULL,
	mobility_of_obstruction json NULL,
	compliance_option varchar(100) NULL,
	applicable_for_traffic_direction varchar(100) NULL,
	applicable_for_traffic_type varchar(100) NULL,
	places_at_which_applicable varchar(100) NULL,
	for_vehicles_with_characteristics_of json NULL,
	roadworks_duration varchar(100) NULL,
	roadworks_scale varchar(100) NULL,
	under_traffic bool NULL,
	urgent_roadworks bool NULL,
	mobility json NULL,
	subjects json NULL,
	maintenance_vehicles json NULL,
	abnormal_traffic_type varchar(100) NULL,
	accident_type varchar(100) NULL,
	group_of_vehicles_involved json NULL,
	car_park_identity text NULL,
	car_park_occupancy varchar(100) NULL,
	number_of_vacant_parking_spaces int4 NULL,
	driving_condition_type varchar(100) NULL,
	construction_work_type varchar(100) NULL,
	disturbance_activity_type varchar(100) NULL,
	environmental_obstruction_type varchar(100) NULL,
	general_instruction_to_road_users_type varchar(100) NULL,
	general_message_to_road_users json NULL,
	general_network_management_type varchar(100) NULL,
	traffic_manually_directed_by varchar(100) NULL,
	infrastructure_damage_type varchar(100) NULL,
	road_maintenance_type varchar(100) NULL,
	non_weather_related_road_condition_type varchar(100) NULL,
	poor_environment_type varchar(100) NULL,
	public_event_type varchar(100) NULL,
	rerouting_management_type varchar(100) NULL,
	rerouting_itinerary_description json NULL,
	signed_rerouting bool NULL,
	entry text NULL,
	"exit" text NULL,
	road_or_junction_number text NULL,
	road_or_carriageway_or_lane_management_type varchar(100) NULL,
	minimum_car_occupancy int4 NULL,
	roadside_assistance_type varchar(100) NULL,
	speed_management_type varchar(100) NULL,
	temporary_speed_limit float4 NULL,
	vehicle_obstruction_type varchar(100) NULL,
	obstructing_vehicle json NULL,
	winter_equipment_management_type varchar(100) NULL,
	create_batch_id int8 NULL,
	created_at timestamptz NULL,
	created_by varchar(150) NULL,
	update_batch_id int8 NULL,
	updated_at timestamptz NULL,
	updated_by varchar(150) NULL,
	geom_alert_c_line public.geometry NULL,
	alert_c_direction varchar(100) NULL,
	CONSTRAINT ndic_traffic_restrictions_regions_pk PRIMARY KEY (situation_record_id, situation_record_version_time)
);

CREATE PROCEDURE regions_data_retention(dataretentionminutes integer)
 LANGUAGE plpgsql
AS $procedure$    
	begin
		delete from ndic.ndic_traffic_restrictions_regions where situation_record_version_time < (NOW() - (dataretentionminutes || ' minutes')::interval);
	end;
$procedure$
;

create view v_traffic_restrictions_all as 
	select * from ndic_traffic_restrictions
	union all 
	select * from ndic_traffic_restrictions_regions;

COMMENT ON TABLE ndic_traffic_restrictions IS 'Obsahuje ndic data z Hlavního města Praha a Středočeského kraje. Historická data se zachovávají.';
COMMENT ON TABLE ndic_traffic_restrictions_regions IS 'Obsahuje ndic data z krajů: Liberecký, Královehradecký, Pardubický, Vysočina, Jihočeský, Plzeňský, Karlovarský, Ústecký. Integrační engine pouští pravidelnou retenci dat.';
