ALTER TABLE ndic_traffic_restrictions
ALTER COLUMN "source" TYPE varchar(100)
USING "source"::varchar(100);

ALTER TABLE ndic_traffic_info
ALTER COLUMN "source" TYPE varchar(100)
USING "source"::varchar(100);

ALTER TABLE ndic_traffic_restrictions
ALTER COLUMN general_public_comment TYPE text
USING general_public_comment::text;

ALTER TABLE ndic_traffic_info
ALTER COLUMN general_public_comment TYPE text
USING general_public_comment::text;

ALTER TABLE ndic_traffic_restrictions ADD geom_alert_c_line geometry NULL;
ALTER TABLE ndic_traffic_restrictions ADD alert_c_direction varchar(100) NULL;

ALTER TABLE ndic_traffic_info ADD geom_alert_c_line geometry NULL;
ALTER TABLE ndic_traffic_info ADD alert_c_direction varchar(100) NULL;
