CREATE TABLE global_network_osm_mapping (
	road_id varchar(100) NOT NULL,
	direction varchar(100) NOT NULL,
	osm_path text NOT NULL,
	CONSTRAINT global_network_osm_mapping_pk PRIMARY KEY (road_id, direction)
);

ALTER TABLE ndic_traffic_restrictions ADD osm_path json NULL;

ALTER TABLE ndic_traffic_restrictions_regions ADD osm_path json NULL;

create or replace view v_traffic_restrictions_all as 
	select * from ndic_traffic_restrictions
	union all 
	select * from ndic_traffic_restrictions_regions;
