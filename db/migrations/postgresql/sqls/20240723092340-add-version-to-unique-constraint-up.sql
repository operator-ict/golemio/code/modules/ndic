ALTER TABLE ndic_traffic_restrictions_regions
    DROP CONSTRAINT ndic_traffic_restrictions_regions_pk;

ALTER TABLE ndic_traffic_restrictions_regions
    ADD CONSTRAINT ndic_traffic_restrictions_regions_pk PRIMARY KEY (situation_record_id, situation_record_version_time, situation_version);

ALTER TABLE ndic_traffic_info
    DROP CONSTRAINT ndic_traffic_info_pk;

ALTER TABLE ndic_traffic_info
    ADD CONSTRAINT ndic_traffic_info_pk PRIMARY KEY (situation_id, situation_record_type, situation_record_version_time, situation_version)
