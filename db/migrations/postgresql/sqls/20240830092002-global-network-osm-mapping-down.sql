drop table global_network_osm_mapping;

drop view v_traffic_restrictions_all;

ALTER TABLE ndic_traffic_restrictions DROP COLUMN osm_path;

ALTER TABLE ndic_traffic_restrictions_regions DROP COLUMN osm_path;

create view v_traffic_restrictions_all as 
	select * from ndic_traffic_restrictions
	union all 
	select * from ndic_traffic_restrictions_regions;