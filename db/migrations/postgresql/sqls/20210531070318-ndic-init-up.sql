CREATE TABLE ndic_traffic_info (
    exchange json,
    publication_time timestamp with time zone,
    publication_creator json,
    situation_id varchar(100) NOT NULL,
    situation_version varchar(100) NOT NULL,
    situation_version_time timestamp with time zone,
    -- Header --
    situation_confidentiality varchar(100),
    situation_information_status varchar(100),
    situation_urgency varchar(100),
    --- Situation record ---
    -- identification
    situation_record_version varchar(100) NOT NULL,
    situation_record_type varchar(100) NOT NULL,
    -- content
    situation_record_creation_time timestamp with time zone NOT NULL,
    situation_record_version_time timestamp with time zone NOT NULL,
    probability_of_occurrence varchar(100),
    source json,
    -- validity JSON --
    validity_status varchar(100),
    validity_overall_start_time timestamp with time zone,
    validity_overall_end_time timestamp with time zone,
    -- impact JSON --
    impact_capacity_remaining real,
    impact_number_of_lanes_restricted int,
    impact_number_of_operational_lanes int,
    impact_original_number_of_lanes int,
    impact_traffic_construction_type varchar(100),
    impact_delays_type varchar(100),
    impact_delay_time_value real,
    cause json,
    general_public_comment json,
    -- group_of_locations JSON --
    group_of_locations_type varchar(100),
    supplementary_positional_description json,
    destination json,
    alert_c_linear json,
    linear_within_linear_element json,
    global_network_linear json,
    linear_extension json,
    geom_gn_line GEOMETRY,
    geom_openlr_line GEOMETRY,
    --- Situation record extension ---
    situation_record_extension json,
    --- Situation record abstract types ---
    -- Obstruction
    number_of_obstructions int,
    mobility_of_obstruction json,
    -- NetworkManagement
    compliance_option varchar(100),
    applicable_for_traffic_direction varchar(100),
    applicable_for_traffic_type varchar(100),
    places_at_which_applicable varchar(100),
    for_vehicles_with_characteristics_of json,
    -- RoadWorks
    roadworks_duration varchar(100),
    roadworks_scale varchar(100),
    under_traffic bool,
    urgent_roadworks bool,
    mobility json,
    subjects json,
    maintenance_vehicles json,
    --- Situation record types ---
    -- AbnormalTraffic
    abnormal_traffic_type varchar(100),
    relative_traffic_flow varchar(100),
    traffic_trend_type varchar(100),
    -- Accident
    accident_type varchar(100),
    total_number_of_vehicles_involved int,
    vehicle_involved json,
    group_of_vehicles_involved json,
    group_of_people_involved json,
    -- AnimalPresenceObstruction
    animal_presence_type varchar(100),
    alive bool,
    -- AuthorityOperation
    authority_operation_type varchar(100),
    -- CarParks
    car_park_configuration varchar(100),
    car_park_identity text,
    car_park_occupancy varchar(100),
    car_park_status varchar(100),
    number_of_vacant_parking_spaces int,
    occupied_spaces int,
    total_capacity int,
    -- Conditions
    driving_condition_type varchar(100),
    -- ConstructionWorks
    construction_work_type varchar(100),
    -- DisturbanceActivity
    disturbance_activity_type varchar(100),
    -- EnvironmentalObstruction
    depth real,
    environmental_obstruction_type varchar(100),
    -- EquipmentOrSystemFault
    equipment_or_system_fault_type varchar(100),
    faulty_equipment_or_system_type varchar(100),
    -- GeneralInstructionOrMessageToRoadUsers
    general_instruction_to_road_users_type varchar(100),
    general_message_to_road_users json,
    -- GeneralNetworkManagement
    general_network_management_type varchar(100),
    traffic_manually_directed_by varchar(100),
    -- GeneralObstruction
    obstruction_type varchar(100),
    --group_of_people_involved JSON, already in Accident
    -- InfrastructureDamageObstruction
    infrastructure_damage_type varchar(100),
    -- MaintenanceWorks
    road_maintenance_type varchar(100),
    -- NonWeatherRelatedRoadConditions
    non_weather_related_road_condition_type varchar(100),
    -- PoorEnvironmentConditions
    poor_environment_type varchar(100),
    precipitation_detail json,
    visibility json,
    temperature json,
    wind json,
    -- PublicEvent
    public_event_type varchar(100),
    -- ReroutingManagement
    rerouting_management_type varchar(100),
    rerouting_itinerary_description json,
    signed_rerouting bool,
    entry text,
    exit text,
    road_or_junction_number text,
    -- RoadOperatorServiceDisruption
    road_operator_service_disruption_type varchar(100),
    -- RoadOrCarriagewayOrLaneManagement
    road_or_carriageway_or_lane_management_type varchar(100),
    minimum_car_occupancy int,
    -- RoadsideAssistance
    roadside_assistance_type varchar(100),
    -- RoadsideServiceDisruption
    roadside_service_disruption_type varchar(100),
    -- SpeedManagement
    speed_management_type varchar(100),
    temporary_speed_limit real,
    -- TransitInformation
    transit_service_information varchar(100),
    transit_service_type varchar(100),
    -- VehicleObstruction
    vehicle_obstruction_type varchar(100),
    obstructing_vehicle json,
    -- WeatherRelatedRoadConditions;
    weather_related_road_condition_type varchar(100),
    road_surface_condition_measurements json,
    -- WinterDrivingManagement
    winter_equipment_management_type varchar(100),

    -- Audit fields --
    create_batch_id bigint,
    created_at timestamp with time zone,
    created_by varchar(150),
    update_batch_id bigint,
    updated_at timestamp with time zone,
    updated_by varchar(150),

    CONSTRAINT ndic_traffic_info_pk PRIMARY KEY (situation_id, situation_record_type, situation_record_version_time)
);
