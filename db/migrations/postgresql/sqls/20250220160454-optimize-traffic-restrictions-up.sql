create or replace function fetch_valid_traffic_restrictions(validity_datetime timestamptz)
    returns setof v_traffic_restrictions_all
    language sql
    set search_path from current
as $function$
    select *
    from (
        select distinct on (situation_record_id) *
        from v_traffic_restrictions_all
        where situation_record_id in (
            select distinct situation_record_id
            from v_traffic_restrictions_all
            where
                validity_overall_start_time < validity_datetime
                and validity_overall_end_time > validity_datetime
        )
        order by situation_record_id, situation_record_version_time desc
    ) tmp
    where
      tmp.validity_overall_start_time < validity_datetime
        and tmp.validity_overall_end_time > validity_datetime
    ;
$function$
;

comment on function fetch_valid_traffic_restrictions(timestamptz) is E''
    'Get all traffic restrictions (both from Prague and from regions) with'
    ' validity_overall_start_time < validity_datetime < validity_overall_end_time. Only one row per situation_record_id will be'
    ' returned - the one with the highest situation_record_version_time.'
    ;

create index ndic_traffic_restrictions_regions_situation_record_id_idx
    on ndic_traffic_restrictions_regions
    using hash (situation_record_id)
    ;
create index ndic_traffic_restrictions_situation_record_id_idx
    on ndic_traffic_restrictions
    using hash (situation_record_id)
    ;

create index ndic_traffic_restrictions_regions_validity_overall_time_idx
    on ndic_traffic_restrictions_regions
    using btree (validity_overall_start_time, validity_overall_end_time)
    ;
create index ndic_traffic_restrictions_validity_overall_time_idx
    on ndic_traffic_restrictions
    using btree (validity_overall_start_time, validity_overall_end_time)
    ;
