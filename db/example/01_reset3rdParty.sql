-- Drop table

DROP TABLE if exists public.rsd_tmc_points;

CREATE TABLE public.rsd_tmc_points (
	cid int4 NULL,
	"table" int4 NULL,
	lcd int4 NOT NULL,
	"class" varchar(1024) NULL,
	tcd int4 NULL,
	stcd int4 NULL,
	jnumber varchar(50) NULL,
	roadnumber varchar(1024) NULL,
	roadname varchar(1024) NULL,
	firstname varchar(1024) NULL,
	secondname varchar(1024) NULL,
	area_ref int4 NULL,
	area_name varchar(1024) NULL,
	roa_lcd int4 NULL,
	seg_lcd int4 NULL,
	roa_type varchar(1024) NULL,
	inpos int4 NULL,
	outpos int4 NULL,
	inneg int4 NULL,
	outneg int4 NULL,
	presentpos int4 NULL,
	presentneg int4 NULL,
	interrupt int4 NULL,
	urban int4 NULL,
	int_lcd varchar(1024) NULL,
	neg_off int4 NULL,
	pos_off int4 NULL,
	wgs84_x numeric NULL,
	wgs84_y numeric NULL,
	sjtsk_x numeric NULL,
	sjtsk_y numeric NULL,
	isolated int4 NULL,
	version_id varchar(50) NOT NULL,
	wgs84_point geography(point, 4326) NULL,
	CONSTRAINT rsd_tmc_points_pkey PRIMARY KEY (lcd, version_id)
);
