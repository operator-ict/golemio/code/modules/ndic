# Implementační dokumentace modulu *Ndic*

## Záměr

Stručný popis, k čemu modul slouží.

Pokud má modul více oblastí, každá oblast je popsána zvlášť. Dále jsem popsány vazby mezi jednotlivými oblastmi.



## Vstupní data

### Data nám jsou posílána

Popis dat, jak nám jsou posíláná na input-gateway. Pokud je vystaveno více endpointů (pro různé datové sady), každý endpoint je popsán zvlášť.

#### */traffic-info / NdicTrafficInfo*

- formát dat
  - protokol http
  - datový typ xml
  - [odkaz na validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/blob/master/schemas/traffic_info_schema.xsd)
  - [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/blob/master/test/data/traffic_info_data.xml)
- endpoint v input-gateway `/traffic-info`
  - příchozí xml je na input gateway pomocí obecného middlewaru rozparsováno body parsererem z xml na json. Již předpřipravený request se dostane skrze NdicRouter do TrafficInfoController a je zvalidován oproti json schématu [link](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/blob/master/src/schema-definitions/schemas/traffic_info_datasource_schema.ts)
- (volitelně) nastavení práv v permission-proxy
  - zvláštní požadaky na řízení přístupu
  - TBD
- transformace raw dat
  - standardní body parser xml->json
- název rabbitmq fronty
  - `dataplatform.ndic.saveTrafficInfo` - zprávy v queue obsahují přichozí data v json formátu
- (volitelně) odhadovaná zátěž
  - n/a

#### */traffic-restrictions / NdicTrafficRestrictions*

- formát dat
  - protokol http
  - datový typ xml
  - [odkaz na validační schéma](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/blob/master/schemas/traffic_restrictions_schema.xsd)
  - [příklad vstupních dat](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/blob/master/test/data/traffic_restrictions_data.xml)
- endpoint v input-gateway
  - popis endpointu /traffic-restrictions
- (volitelně) nastavení práv v permission-proxy
  - zvláštní požadaky na řízení přístupu
- transformace raw dat
  - body parser xml->json
- název rabbitmq fronty
  - `dataplatform.ndic.saveTrafficRestrictions` - zprávy v queue obsahují přichozí data v json formátu
- (volitelně) odhadovaná zátěž
  - n/a

### */traffic-restrictions-regions

  - stejné jako `traffic-restrictions` s tím rozdílem, že na tento endpoint chodí data kolem Středočeského kraje: tj Liberecký, Královehradecký, Pardubický, Vysočina, Jihočeský, Plzeňský, Karlovarský, Ústecký
  - název rabbitmq fronty
  - `dataplatform.ndic.saveTrafficRestrictionsRegions` - zprávy v queue obsahují přichozí data v json formátu


## Zpracování dat / transformace

Popis transformace a případného obohacení dat. Zaměřeno hlavně na workery a jejich metody.

Interní RabbitMQ fronty jsou popsány v [AsyncAPI](./asyncapi.yaml).

### *LegacyNdicWorker*

Worker slouží pro zpracování příchozích dat NdicTrafficInfo a NdicTrafficRestrictions.

#### *saveTrafficInfo*

- vstupní rabbitmq fronta
  - `dataplatform.ndic.saveTrafficInfo`
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - žádné
- datové zdroje
  - rsdTmcPointsModel - pro zjištění geo souřadnic
- transformace
  - [odkaz na transformaci dat](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/blob/development/src/integration-engine/transformations/TrafficInfoTransformation.ts)
- (volitelně) obohacení dat
  - po tranformaci se data obohacují o geosouřadnice (např. úsek silnice), kdy data z alertCLinear části slouží jako filtrovací parametry pro lookup datasource rsdTmcPointsModel
- data modely
  - `ndicTrafficInfoModel` - ukládá se do tabulky `ndic_traffic_info`

#### *saveTrafficRestrictions*

- vstupní rabbitmq fronta
  - `dataplatform.ndic.saveTrafficRestrictions`
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - žádné
- datové zdroje
  - `rsdTmcPointsModel` - pro zjištění geo souřadnic
- transformace
  - [odkaz na transformaci dat](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/blob/master/src/integration-engine/transformations/TrafficRestrictionsTransformation.ts)
- (volitelně) obohacení dat
  - stejné jako u předešlé metody *`saveTrafficInfo`*
- data modely
  - `ndicTrafficRestrictionsModel` se ukládá do tabulky ndic_traffic_restrictions

#### Task *SaveTrafficRestrictionsRegions*

- vstupní rabbitmq fronta
  - `dataplatform.ndic.saveTrafficRestrictionsRegions`
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - žádné
- datové zdroje
  - `rsdTmcPointsModel` - pro zjištění geo souřadnic
- transformace
  - [odkaz na transformaci dat](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/blob/master/src/integration-engine/transformations/TrafficRestrictionsTransformation.ts)
- (volitelně) obohacení dat
  - stejné jako u předešlé metody *`saveTrafficInfo`*
- repository
  - ukládá do tabulky `ndic_traffic_restrictions_regions`
  - předpis stejný jako u saveTrafficRestrictions
- retence
  - 48h

#### Task *RegionsDataRetention*
- spouští procedura `regions_data_retention` pro retenci dat v tabulce `ndic_traffic_restrictions_regions`.
  - procedura smaže všechny záznamy, které jsou starší než 48h podle `situation_record_version_time`
- vstupní rabbitmq fronta
  - `dataplatform.ndic.regionsDataRetention`
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - žádné
- datové zdroje
  - n/a
- transformace
  - n/a
- (volitelně) obohacení dat
  - n/a

#### Task *RefreshGlobalNetworkDataTask*
- spustí přenačtení dat pro tabulku `global_network_osm_mapping`
- vstupní rabbitmq fronta
  - `dataplatform.ndic.refreshGlobalNetworkData`
- (volitelně) závislé fronty (do kterých jsou odesílány zprávy z metody workeru)
  - žádné
- datové zdroje
  - `GlobalNetworkMapperDataSource` stáhne cca 50MB csv soubor na blob storage
- transformace
  - n/a ukládáme ve stejném formatu do db

## Uložení dat

Popis ukládání dat.

### Obecné

- typ databáze
  - PSQL
- datábázové schéma
  - tabulka `ndic_traffic_info`
  - tabulka `ndic_traffic_restrictions` (obsahuje kompletní informace z Hlavního města Prahy a Středočeského kraje)
  - tabulka `ndic_traffic_restrictions_regions` (obsahuje časově omezené informace z regionů okolo Středočeského kraje)
  - view `v_traffic_restrictions_all`
  - procedura `regions_data_retention`
  ![ndic er diagram](ndic_erdiagram.png "ERD")
 - retence dat
  - n/a

### *ndicTrafficInfoModel*

- schéma a název tabulky / kolekce
    - `ndic.ndic_traffic_info`
- struktura tabulky / kolekce
  - odkaz na schéma, případně na migrační skripty
    - schéma viz výše
    - [odkaz na migrační skripty]((./../db/migrations/postgresql/sqls))
- ukázka dat
    n/a

### *ndicTrafficInfoModel*

- schéma a název tabulky / kolekce
    - `ndic.ndic_traffic_restrictions`
- struktura tabulky / kolekce
  - odkaz na schéma, případně na migrační skripty
    - schéma viz výše
    - [odkaz na migrační skripty](./../db/migrations/postgresql/sqls)
- [ukázka dat](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/blob/master/db/example/sql_dump.sql)


## Output API

Popis output-api.

### Obecné

- API Blueprint / OpenAPI dokumentace
  - [odkaz na dokumentaci](./openapi.yaml)
- veřejné / neveřejné endpointy
  - api je neveřejné, endpoint /restrictions je přístupný pro uživatele s rolí NDIC
- (volitelně) ACL

#### */restrictions*

- zdrojové tabulky
    - `v_traffic_restrictions_all` - view vzniklo spojením tabulek `ndic_traffic_restrictions` a `ndic_traffic_restrictions_regions`
- (volitlně) nestandardní dodatečná transformace
    - vrací pouze poslední známe stavy ( max `situation_record_version_time` ) všech dopravních situací podle klíče `situation_record_id`, které jsou platné ve vybraný čas
- (volitelně) nestandardní url a query parametry

    | query parameter | popis |
    | --- |---|
    | reqMoment | datum a čas pro který zobrazit platné situace ve formátu ISO8601 |
    | situationRecordType |  filtruje podle typu situace |
