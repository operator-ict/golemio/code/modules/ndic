import { GlobalNetworkMapperDataSource } from "#ie/datasources/GlobalNetworkMapperDataSource";
import { ModuleContainerToken } from "#ie/ioc/ContainerToken";
import { NdicContainer } from "#ie/ioc/Di";
import { GlobalNetworkMappingRepository } from "#ie/repository/GlobalNetworkMappingRepository";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { Readable } from "stream";

export class RefreshGlobalNetworkDataTask extends AbstractEmptyTask {
    public queueName: string = "refreshGlobalNetworkData";
    private repository: GlobalNetworkMappingRepository;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.repository = NdicContainer.resolve<GlobalNetworkMappingRepository>(
            ModuleContainerToken.GlobalNetworkMappingRepository
        );
    }

    protected async execute(): Promise<void> {
        const datasource = NdicContainer.resolve<GlobalNetworkMapperDataSource>(
            ModuleContainerToken.GlobalNetworkMapperDataSource
        ).datasource;

        let dataStream: { data: Readable };

        dataStream = await datasource.getRawData();
        await this.repository.streamData(dataStream.data);
    }
}
