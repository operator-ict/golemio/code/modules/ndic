import { Ndic } from "#sch";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import { AbstractEmptyTask } from "@golemio/core/dist/integration-engine/workers/AbstractEmptyTask";
import { QueryTypes } from "@golemio/core/dist/shared/sequelize";

export class RegionsDataRetention extends AbstractEmptyTask {
    public queueName: string = "regionsDataRetention";
    private DATA_RETENTION_IN_MINUTES = 2880; // 2 days

    protected async execute(): Promise<void> {
        const sequelize = PostgresConnector.getConnection();
        await sequelize.query(`CALL ${Ndic.pgSchema}.regions_data_retention(${this.DATA_RETENTION_IN_MINUTES});`, {
            plain: true,
            type: QueryTypes.SELECT,
        });
    }
}
