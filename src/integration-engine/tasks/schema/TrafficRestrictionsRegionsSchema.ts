import { ISituation, ITrafficRestrictions } from "#ie/transformations/TrafficRestrictionsInterface";
import { IsObject } from "@golemio/core/dist/shared/class-validator";

//Just mockup schema. data are validated on IG side.
export default class TrafficRestrictionsRegionsSchema implements ITrafficRestrictions {
    @IsObject()
    $!: {
        xmlns: string;
        "xmlns:xsi": string;
        modelBaseVersion: string;
    };
    @IsObject()
    exchange!: Record<string, any>;
    @IsObject()
    payloadPublication!: {
        $: { "xsi:type": string; lang: string };
        publicationTime: string;
        publicationCreator: Record<string, any>;
        situation: ISituation | ISituation[];
    };
}
