import { LteCze90PointsChecker } from "#ie/helpers/LteCze90PointsChecker";
import { OsmPathService } from "#ie/helpers/OsmPathService";
import { TrafficRestrictionsHelper } from "#ie/helpers/TrafficRestrictionsHelper";
import { ModuleContainerToken } from "#ie/ioc/ContainerToken";
import { NdicContainer } from "#ie/ioc/Di";
import { ITrafficRestrictions, ITrafficRestrictionsModel } from "#ie/transformations/TrafficRestrictionsInterface";
import { TrafficRestrictionsTransformation } from "#ie/transformations/TrafficRestrictionsTransformation";
import { Ndic } from "#sch";
import { AbstractTask, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import TrafficRestrictionsRegionsSchema from "./schema/TrafficRestrictionsRegionsSchema";

export class SaveTrafficRestrictionsRegions extends AbstractTask<ITrafficRestrictions> {
    public queueName: string = "saveTrafficRestrictionsRegions";
    public queueTtl = 24 * 60 * 60 * 1000; // 24h
    protected schema = TrafficRestrictionsRegionsSchema; // Data already validated in Input Gateway
    private transformation: TrafficRestrictionsTransformation;
    private repository: PostgresModel;
    private filtrationChecker: LteCze90PointsChecker;
    private osmPathService: OsmPathService;

    constructor(queuePrefix: string) {
        super(queuePrefix);
        this.transformation = new TrafficRestrictionsTransformation();
        this.repository = new PostgresModel(
            Ndic.traffic_restrictions.name + "Model",
            {
                outputSequelizeAttributes: Ndic.traffic_restrictions.outputSequelizeAttributes,
                pgSchema: Ndic.pgSchema,
                pgTableName: "ndic_traffic_restrictions_regions",
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(Ndic.traffic_restrictions.name + "PgModelValidator", Ndic.traffic_restrictions.outputSchema)
        );
        this.filtrationChecker = new LteCze90PointsChecker();
        this.osmPathService = NdicContainer.resolve<OsmPathService>(ModuleContainerToken.OsmPathService);
    }

    protected execute = async (data: ITrafficRestrictions): Promise<void> => {
        const transformedData = await this.transformation.transform(data);

        let dataToSave = await this.filterData(transformedData);
        dataToSave = await TrafficRestrictionsHelper.populateWithGeomAlertCLineRestrictions(dataToSave);
        dataToSave = await this.osmPathService.addOsmPath(dataToSave);

        await this.repository.save(TrafficRestrictionsHelper.uniqueRestrictionsArray(dataToSave));
    };

    private filterData = async (transformedData: ITrafficRestrictionsModel[]) => {
        await this.filtrationChecker.initialize();

        return transformedData.filter((element) => this.filtrationChecker.shouldSave(element));
    };
}
