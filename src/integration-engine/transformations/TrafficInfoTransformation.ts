import {
    IAlertCLinear,
    IGlobalNetworkLinear,
    ILinearExtension,
    ISituation,
    ISituationModel,
    ISituationRecordGeneral,
    ISituationRecordModel,
    ITrafficInfo,
    ITrafficInfoModel,
} from "#ie/transformations/TrafficInfoInterface";
import { Ndic } from "#sch";
import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { parseNumber } from "./ParserHelper";

export class TrafficInfoTransformation extends BaseTransformation implements ITransformation {
    name: string;

    constructor() {
        super();
        this.name = Ndic.traffic_info.name;
    }

    public transform = async (data: ITrafficInfo): Promise<ITrafficInfoModel[]> => {
        const logicalModelInfo = {
            exchange: data.exchange,
            publication_time: data.payloadPublication.publicationTime,
            publication_creator: data.payloadPublication.publicationCreator,
        };

        const situationArr = !Array.isArray(data.payloadPublication.situation)
            ? [data.payloadPublication.situation]
            : data.payloadPublication.situation;

        const result: ITrafficInfoModel[] = [];
        for (const situation of situationArr) {
            const situationTransformed: ISituationModel[] = await this.transformElement(situation);
            result.push(...situationTransformed.map((el) => ({ ...logicalModelInfo, ...el })));
        }

        return result;
    };

    protected transformElement = async (situation: ISituation): Promise<ISituationModel[]> => {
        const situationInfo = {
            situation_id: situation.$.id,
            situation_version: situation.$.version,
            situation_version_time: situation.situationVersionTime,
            situation_confidentiality: situation.headerInformation.confidentiality,
            situation_information_status: situation.headerInformation.informationStatus,
            situation_urgency: situation.headerInformation.urgency,
        };

        const situationRecordArr = !Array.isArray(situation.situationRecord)
            ? [situation.situationRecord]
            : situation.situationRecord;
        const result = [];
        for (const situationRecord of situationRecordArr) {
            const situationRecordMapObj = await this.situationRecordMap(situationRecord as ISituationRecordGeneral);
            result.push({
                ...situationInfo,
                ...situationRecordMapObj,
            });
        }
        return result;
    };

    private parseGeneralPublicComment = (generalPublicComment: Record<string, any> | undefined): string | undefined => {
        if (!generalPublicComment) return undefined;

        const generalPublicCommentArr = !Array.isArray(generalPublicComment) ? [generalPublicComment] : generalPublicComment;

        let resultCommentsString = "";

        for (const generalPublicCommentObj of generalPublicCommentArr) {
            const values: any[] = !Array.isArray(generalPublicCommentObj.comment.values)
                ? [generalPublicCommentObj.comment.values]
                : generalPublicCommentObj.comment.values;
            if (resultCommentsString.length) {
                resultCommentsString += "|";
            }
            resultCommentsString += values.map((element) => element.value._).join("|");
        }
        return resultCommentsString;
    };

    private parseAlertCDirection = (alertCDirectionObj: IAlertCLinear | undefined): string | undefined => {
        return alertCDirectionObj?.alertCDirection.alertCDirectionCoded;
    };

    private parseGeomOpenlrLine = (
        linearExtension: ILinearExtension | undefined
    ): { coordinates: any[][]; type: string } | undefined => {
        if (!linearExtension || !linearExtension.openlrExtendedLinear) return undefined;

        const openlrLocationReferencePointArr: any[] = !Array.isArray(
            linearExtension.openlrExtendedLinear.firstDirection.openlrLocationReferencePoint
        )
            ? [linearExtension.openlrExtendedLinear.firstDirection.openlrLocationReferencePoint]
            : linearExtension.openlrExtendedLinear.firstDirection.openlrLocationReferencePoint;

        let coordinatesArr = openlrLocationReferencePointArr.map((element) => [
            element.openlrCoordinate.longitude,
            element.openlrCoordinate.latitude,
        ]);
        let openlrLastLocationObj =
            linearExtension.openlrExtendedLinear.firstDirection.openlrLastLocationReferencePoint.openlrCoordinate;
        coordinatesArr.push([openlrLastLocationObj.longitude, openlrLastLocationObj.latitude]);

        return {
            coordinates: coordinatesArr,
            type: "LineString",
        };
    };

    private parseGeomGnLine = (
        globalNetworkLinear: IGlobalNetworkLinear | undefined
    ): { coordinates: any[][]; type: string } | undefined => {
        if (!globalNetworkLinear) return undefined;

        const startPointObj = globalNetworkLinear.startPoint.sjtskPointCoordinates;
        const endPointObj = globalNetworkLinear.endPoint.sjtskPointCoordinates;

        if (!startPointObj || !endPointObj) return undefined;
        let coordinatesArr = [
            [startPointObj.sjtskX, startPointObj.sjtskY],
            [endPointObj.sjtskX, endPointObj.sjtskY],
        ];
        return {
            coordinates: coordinatesArr,
            type: "LineString",
        };
    };

    private situationRecordMap = async (situationRecord: ISituationRecordGeneral): Promise<ISituationRecordModel> => {
        return {
            situation_record_version: situationRecord.$.version,
            situation_record_type: situationRecord.$["xsi:type"],
            situation_record_creation_time: situationRecord.situationRecordCreationTime,
            situation_record_version_time: situationRecord.situationRecordVersionTime,
            probability_of_occurrence: situationRecord.probabilityOfOccurrence,
            source: situationRecord.source?.sourceIdentification,
            validity_status: situationRecord.validity?.validityStatus,
            validity_overall_start_time: situationRecord.validity?.validityTimeSpecification?.overallStartTime,
            validity_overall_end_time: situationRecord.validity?.validityTimeSpecification?.overallEndTime,
            impact_capacity_remaining: parseNumber(situationRecord.impact?.capacityRemaining),
            impact_number_of_lanes_restricted: parseNumber(situationRecord.impact?.numberOfLanesRestricted),
            impact_number_of_operational_lanes: parseNumber(situationRecord.impact?.numberOfOperationalLanes),
            impact_original_number_of_lanes: parseNumber(situationRecord.impact?.originalNumberOfLanes),
            impact_traffic_constriction_type: situationRecord.impact?.trafficConstrictionType,
            impact_delays_type: situationRecord.impact?.delays?.delaysType,
            impact_delay_time_value: parseNumber(situationRecord.impact?.delays?.delayTimeValue),
            cause: situationRecord.cause,
            general_public_comment: this.parseGeneralPublicComment(situationRecord.generalPublicComment),
            // Group of locations
            group_of_locations_type: situationRecord.groupOfLocations.$["xsi:type"],
            supplementary_positional_description: situationRecord.groupOfLocations.supplementaryPositionalDescription,
            destination: situationRecord.groupOfLocations.destination,
            alert_c_linear: situationRecord.groupOfLocations.alertCLinear,
            linear_within_linear_element: situationRecord.groupOfLocations.linearWithinLinearElement,
            global_network_linear: situationRecord.groupOfLocations.globalNetworkLinear,
            linear_extension: situationRecord.groupOfLocations.linearExtension,

            // Geom
            alert_c_direction: this.parseAlertCDirection(situationRecord.groupOfLocations.alertCLinear),
            geom_openlr_line: this.parseGeomOpenlrLine(situationRecord.groupOfLocations.linearExtension),
            geom_gn_line: this.parseGeomGnLine(situationRecord.groupOfLocations.globalNetworkLinear),

            // SR extension
            situation_record_extension: situationRecord.situationRecordExtension,
            // Situation record specific type params
            number_of_obstructions: parseNumber(situationRecord.numberOfObstructions),
            mobility_of_obstruction: situationRecord.mobilityOfObstruction,
            compliance_option: situationRecord.complianceOption,
            applicable_for_traffic_direction: situationRecord.applicableForTrafficDirection?.toString(),
            applicable_for_traffic_type: situationRecord.applicableForTrafficType?.toString(),
            places_at_which_applicable: situationRecord.placesAtWhichApplicable?.toString(),
            for_vehicles_with_characteristics_of: situationRecord.forVehiclesWithCharacteristicsOf,
            roadworks_duration: situationRecord.roadworksDuration,
            roadworks_scale: situationRecord.roadworksScale,
            under_traffic: situationRecord.underTraffic,
            urgent_roadworks: situationRecord.urgentRoadworks,
            mobility: situationRecord.mobility,
            subjects: situationRecord.subjects,
            maintenance_vehicles: situationRecord.maintenanceVehicles,
            abnormal_traffic_type: situationRecord.abnormalTrafficType,
            relative_traffic_flow: situationRecord.relativeTrafficFlow,
            traffic_trend_type: situationRecord.trafficTrendType,
            accident_type: situationRecord.accidentType?.toString(),
            total_number_of_vehicles_involved: parseNumber(situationRecord.totalNumberOfVehiclesInvolved),
            vehicle_involved: situationRecord.vehicleInvolved,
            group_of_vehicles_involved: situationRecord.groupOfVehiclesInvolved,
            group_of_people_involved: situationRecord.groupOfPeopleInvolved,
            animal_presence_type: situationRecord.animalPresenceType,
            alive: situationRecord.alive,
            authority_operation_type: situationRecord.authorityOperationType,
            car_park_configuration: situationRecord.carParkConfiguration,
            car_park_identity: situationRecord.carParkIdentity,
            car_park_occupancy: situationRecord.carParkOccupancy,
            car_park_status: situationRecord.carParkStatus,
            number_of_vacant_parking_spaces: parseNumber(situationRecord.numberOfVacantParkingSpaces),
            occupied_spaces: parseNumber(situationRecord.occupiedSpaces),
            total_capacity: parseNumber(situationRecord.totalCapacity),
            driving_condition_type: situationRecord.drivingConditionType,
            construction_work_type: situationRecord.constructionWorkType,
            disturbance_activity_type: situationRecord.disturbanceActivityType,
            depth: parseNumber(situationRecord.depth),
            environmental_obstruction_type: situationRecord.environmentalObstructionType,
            equipment_or_system_fault_type: situationRecord.equipmentOrSystemFaultType,
            faulty_equipment_or_system_type: situationRecord.faultyEquipmentOrSystemType,
            general_instruction_to_road_users_type: situationRecord.generalInstructionToRoadUsersType,
            general_message_to_road_users: situationRecord.generalMessageToRoadUsers,
            general_network_management_type: situationRecord.generalNetworkManagementType,
            traffic_manually_directed_by: situationRecord.trafficManuallyDirectedBy,
            obstruction_type: situationRecord.obstructionType?.toString(),
            infrastructure_damage_type: situationRecord.infrastructureDamageType,
            road_maintenance_type: situationRecord.roadMaintenanceType?.toString(),
            non_weather_related_road_condition_type: situationRecord.nonWeatherRelatedRoadConditionType?.toString(),
            poor_environment_type: situationRecord.poorEnvironmentType?.toString(),
            precipitation_detail: situationRecord.precipitationDetail,
            visibility: situationRecord.visibility,
            temperature: situationRecord.temperature,
            wind: situationRecord.wind,
            public_event_type: situationRecord.publicEventType,
            rerouting_management_type: situationRecord.reroutingManagementType?.toString(),
            rerouting_itinerary_description: situationRecord.reroutingItineraryDescription,
            signed_rerouting: situationRecord.signedRerouting,
            entry: situationRecord.entry,
            exit: situationRecord.exit,
            road_or_junction_number: situationRecord.roadOrJunctionNumber,
            road_operator_service_disruption_type: situationRecord.roadOperatorServiceDisruptionType?.toString(),
            road_or_carriageway_or_lane_management_type: situationRecord.roadOrCarriagewayOrLaneManagementType,
            minimum_car_occupancy: parseNumber(situationRecord.minimumCarOccupancy),
            roadside_assistance_type: situationRecord.roadsideAssistanceType,
            roadside_service_disruption_type: situationRecord.roadsideServiceDisruptionType?.toString(),
            speed_management_type: situationRecord.speedManagementType,
            temporary_speed_limit: parseNumber(situationRecord.temporarySpeedLimit),
            transit_service_information: situationRecord.transitServiceInformation,
            transit_service_type: situationRecord.transitServiceType,
            vehicle_obstruction_type: situationRecord.vehicleObstructionType,
            obstructing_vehicle: situationRecord.obstructingVehicle,
            weather_related_road_condition_type: situationRecord.weatherRelatedRoadConditionType?.toString(),
            road_surface_condition_measurements: situationRecord.roadSurfaceConditionMeasurements,
            winter_equipment_management_type: situationRecord.winterEquipmentManagementType,
        };
    };
}
