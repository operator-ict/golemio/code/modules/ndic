export function parseNumber(value: string | undefined): number | undefined {
    return value ? Number(value) : undefined;
}
