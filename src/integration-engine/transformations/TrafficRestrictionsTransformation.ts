import { BaseTransformation, ITransformation } from "@golemio/core/dist/integration-engine";
import { Ndic } from "#sch";
import {
    ISituationModel,
    ISituationRecordGeneral,
    ISituationRecordModel,
    ITrafficRestrictions,
    ITrafficRestrictionsModel,
    ISituation,
    IAlertCLinear,
    ILinearExtension,
    IGlobalNetworkLinear,
} from "#ie/transformations/TrafficRestrictionsInterface";
import { parseNumber } from "./ParserHelper";

export class TrafficRestrictionsTransformation extends BaseTransformation implements ITransformation {
    name: string;

    constructor() {
        super();
        this.name = Ndic.traffic_restrictions.name;
    }

    public transform = async (data: ITrafficRestrictions): Promise<ITrafficRestrictionsModel[]> => {
        const logicalModelInfo = {
            exchange: data.exchange,
            publication_time: data.payloadPublication.publicationTime,
            publication_creator: data.payloadPublication.publicationCreator,
        };

        const situationArr = !Array.isArray(data.payloadPublication.situation)
            ? [data.payloadPublication.situation]
            : data.payloadPublication.situation;

        const result: ITrafficRestrictionsModel[] = [];
        for (const situation of situationArr) {
            const situationTransformed: ISituationModel[] = await this.transformElement(situation);
            result.push(...situationTransformed.map((el) => ({ ...logicalModelInfo, ...el })));
        }

        return result;
    };

    protected transformElement = async (situation: ISituation): Promise<ISituationModel[]> => {
        const situationInfo = {
            situation_id: situation.$.id,
            situation_version: situation.$.version,
            situation_version_time: situation.situationVersionTime,
            situation_confidentiality: situation.headerInformation.confidentiality,
            situation_information_status: situation.headerInformation.informationStatus,
            situation_urgency: situation.headerInformation.urgency,
        };

        const situationRecordArr = !Array.isArray(situation.situationRecord)
            ? [situation.situationRecord]
            : situation.situationRecord;

        const result = [];
        for (const situationRecord of situationRecordArr) {
            const situationRecordMapObj = await this.situationRecordMap(situationRecord as ISituationRecordGeneral);
            result.push({
                ...situationInfo,
                ...situationRecordMapObj,
            });
        }
        return result;
    };

    private parseGeneralPublicComment = (generalPublicComment: Record<string, any> | undefined): string | undefined => {
        if (!generalPublicComment) return undefined;

        const generalPublicCommentArr = !Array.isArray(generalPublicComment) ? [generalPublicComment] : generalPublicComment;

        let resultCommentsString = "";

        for (const generalPublicCommentObj of generalPublicCommentArr) {
            const values: any[] = !Array.isArray(generalPublicCommentObj.comment.values)
                ? [generalPublicCommentObj.comment.values]
                : generalPublicCommentObj.comment.values;
            if (resultCommentsString.length) {
                resultCommentsString += "|";
            }
            resultCommentsString += values.map((element) => element.value._).join("|");
        }
        return resultCommentsString;
    };

    private parseAlertCDirection = (alertCDirectionObj: IAlertCLinear | undefined): string | undefined => {
        return alertCDirectionObj?.alertCDirection.alertCDirectionCoded.toUpperCase();
    };

    private parseGeomOpenlrLine = (
        linearExtension: ILinearExtension | undefined
    ): { coordinates: any[][]; type: string } | undefined => {
        if (!linearExtension) return undefined;

        const openlrLocationReferencePointArr: any[] = !Array.isArray(
            linearExtension.openlrExtendedLinear.firstDirection.openlrLocationReferencePoint
        )
            ? [linearExtension.openlrExtendedLinear.firstDirection.openlrLocationReferencePoint]
            : linearExtension.openlrExtendedLinear.firstDirection.openlrLocationReferencePoint;

        let coordinatesArr = openlrLocationReferencePointArr.map((element) => [
            element.openlrCoordinate.longitude,
            element.openlrCoordinate.latitude,
        ]);
        let openlrLastLocationObj =
            linearExtension.openlrExtendedLinear.firstDirection.openlrLastLocationReferencePoint.openlrCoordinate;
        coordinatesArr.push([openlrLastLocationObj.longitude, openlrLastLocationObj.latitude]);

        return {
            coordinates: coordinatesArr,
            type: "LineString",
        };
    };

    private parseGeomGnLine = (
        globalNetworkLinear: IGlobalNetworkLinear | undefined
    ): { coordinates: any[][]; type: string } | undefined => {
        if (!globalNetworkLinear) return undefined;
        const startPointObj = globalNetworkLinear.startPoint?.sjtskPointCoordinates;
        const endPointObj = globalNetworkLinear.endPoint?.sjtskPointCoordinates;

        if (!startPointObj || !endPointObj) return undefined;
        let coordinatesArr = [
            [startPointObj.sjtskX, startPointObj.sjtskY],
            [endPointObj.sjtskX, endPointObj.sjtskY],
        ];
        return {
            coordinates: coordinatesArr,
            type: "LineString",
        };
    };

    private situationRecordMap = async (situationRecord: ISituationRecordGeneral): Promise<ISituationRecordModel> => {
        return {
            situation_record_version: situationRecord.$.version,
            situation_record_type: situationRecord.$["xsi:type"],
            situation_record_id: situationRecord.$.id,
            situation_record_creation_time: situationRecord.situationRecordCreationTime,
            situation_record_version_time: situationRecord.situationRecordVersionTime,
            probability_of_occurrence: situationRecord.probabilityOfOccurrence,
            source: situationRecord.source?.sourceIdentification,
            validity_status: situationRecord.validity?.validityStatus,
            validity_overall_start_time: situationRecord.validity?.validityTimeSpecification?.overallStartTime,
            validity_overall_end_time: situationRecord.validity?.validityTimeSpecification?.overallEndTime,
            impact_capacity_remaining: parseNumber(situationRecord.impact?.capacityRemaining),
            impact_number_of_lanes_restricted: parseNumber(situationRecord.impact?.numberOfLanesRestricted),
            impact_number_of_operational_lanes: parseNumber(situationRecord.impact?.numberOfOperationalLanes),
            impact_original_number_of_lanes: parseNumber(situationRecord.impact?.originalNumberOfLanes),
            impact_traffic_constriction_type: situationRecord.impact?.trafficConstrictionType,
            impact_delays_type: situationRecord.impact?.delays?.delaysType,
            impact_delay_time_value: parseNumber(situationRecord.impact?.delays?.delayTimeValue),
            general_public_comment: this.parseGeneralPublicComment(situationRecord.generalPublicComment),
            compliance_option: situationRecord.complianceOption,
            road_or_carriageway_or_lane_management_type: situationRecord.roadOrCarriagewayOrLaneManagementType,
            cause: situationRecord.cause,

            // Group of locations
            group_of_locations_type: situationRecord.groupOfLocations.$["xsi:type"],
            supplementary_positional_description: situationRecord.groupOfLocations.supplementaryPositionalDescription,
            alert_c_linear: situationRecord.groupOfLocations.alertCLinear,
            linear_within_linear_element: situationRecord.groupOfLocations.linearWithinLinearElement,
            global_network_linear: situationRecord.groupOfLocations.globalNetworkLinear,
            linear_extension: situationRecord.groupOfLocations.linearExtension,
            destination: situationRecord.groupOfLocations.destination,

            // Geom
            alert_c_direction: this.parseAlertCDirection(situationRecord.groupOfLocations.alertCLinear),
            geom_openlr_line: this.parseGeomOpenlrLine(situationRecord.groupOfLocations.linearExtension),
            geom_gn_line: this.parseGeomGnLine(situationRecord.groupOfLocations.globalNetworkLinear),

            // SR extension
            situation_record_extension: situationRecord.situationRecordExtension,

            // Situation record specific type params
            abnormal_traffic_type: situationRecord.abnormalTrafficType,
            accident_type: situationRecord.accidentType?.toString(),
            car_park_identity: situationRecord.carParkIdentity,
            car_park_occupancy: parseNumber(situationRecord.carParkOccupancy),
            driving_condition_type: situationRecord.drivingConditionType,
            construction_work_type: situationRecord.constructionWorkType,
            disturbance_activity_type: situationRecord.disturbanceActivityType,
            general_network_management_type: situationRecord.generalNetworkManagementType,
            group_of_vehicles_involved: situationRecord.groupOfVehiclesInvolved,
            infrastructure_damage_type: situationRecord.infrastructureDamageType,
            number_of_obstructions: parseNumber(situationRecord.numberOfObstructions),
            mobility_of_obstruction: situationRecord.mobilityOfObstruction,
            non_weather_related_road_condition_type: situationRecord.nonWeatherRelatedRoadConditionType?.toString(),
            number_of_vacant_parking_spaces: parseNumber(situationRecord.numberOfVacantParkingSpaces),
            obstructing_vehicle: situationRecord.obstructingVehicle,
            environmental_obstruction_type: situationRecord.environmentalObstructionType,
            public_event_type: situationRecord.publicEventType,
            road_maintenance_type: situationRecord.roadMaintenanceType?.toString(),
            subjects: situationRecord.subjects,
            poor_environment_type: situationRecord.poorEnvironmentType?.toString(),
            vehicle_obstruction_type: situationRecord.vehicleObstructionType,
            applicable_for_traffic_direction: situationRecord.applicableForTrafficDirection?.toString(),
            applicable_for_traffic_type: situationRecord.applicableForTrafficType?.toString(),
            places_at_which_applicable: situationRecord.placesAtWhichApplicable?.toString(),
            for_vehicles_with_characteristics_of: situationRecord.forVehiclesWithCharacteristicsOf,
            roadworks_duration: situationRecord.roadworksDuration,
            roadworks_scale: situationRecord.roadworksScale,
            under_traffic: situationRecord.underTraffic,
            urgent_roadworks: situationRecord.urgentRoadworks,
            mobility: situationRecord.mobility,
            maintenance_vehicles: situationRecord.maintenanceVehicles,
            general_instruction_to_road_users_type: situationRecord.generalInstructionToRoadUsersType,
            general_message_to_road_users: situationRecord.generalMessageToRoadUsers,
            traffic_manually_directed_by: situationRecord.trafficManuallyDirectedBy,
            rerouting_management_type: situationRecord.reroutingManagementType?.toString(),
            rerouting_itinerary_description: situationRecord.reroutingItineraryDescription,
            signed_rerouting: situationRecord.signedRerouting,
            entry: situationRecord.entry,
            exit: situationRecord.exit,
            road_or_junction_number: situationRecord.roadOrJunctionNumber,
            minimum_car_occupancy: parseNumber(situationRecord.minimumCarOccupancy),
            roadside_assistance_type: situationRecord.roadsideAssistanceType,
            speed_management_type: situationRecord.speedManagementType,
            temporary_speed_limit: parseNumber(situationRecord.temporarySpeedLimit),
            winter_equipment_management_type: situationRecord.winterEquipmentManagementType,
        };
    };
}
