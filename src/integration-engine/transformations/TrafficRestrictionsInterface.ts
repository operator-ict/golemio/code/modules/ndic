export interface ITrafficRestrictions {
    $: {
        xmlns: string;
        "xmlns:xsi": string;
        modelBaseVersion: string;
    };
    exchange: Record<string, any>;
    payloadPublication: {
        $: {
            "xsi:type": string;
            lang: string;
        };
        publicationTime: string;
        publicationCreator: Record<string, any>;
        situation: ISituation | ISituation[];
    };
}

export interface ISituation {
    $: {
        id: string;
        version: string;
    };
    situationVersionTime: string;
    headerInformation: {
        confidentiality: string;
        informationStatus: string;
        urgency: string;
    };
    situationRecord: ISituationRecord | ISituationRecord[];
}

interface ISituationRecordIdentifier {
    version: string;
    "xsi:type": string;
    id: string;
}

interface IAlertCMethod2PointLocation {
    alertCLocation: {
        alertCLocationName?: Record<string, any>;
        specificLocation: string;
    };
}

export interface IAlertCLinear {
    $: {
        "xsi:type": string;
    };
    alertCLocationCountryCode: string;
    alertCLocationTableNumber: string;
    alertCLocationTableVersion: string;
    alertCDirection: {
        alertCDirectionCoded: string;
        alertCDirectionNamed?: Record<string, any>;
        alertCDirectionSense?: boolean;
    };
    alertCMethod2PrimaryPointLocation: IAlertCMethod2PointLocation;
    alertCMethod2SecondaryPointLocation: IAlertCMethod2PointLocation;
}

interface IPointByMultiCoordinates {
    sjtskPointCoordinates?: {
        sjtskX: string;
        sjtskY: string;
    };
    pointCoordinates?: ILatLonCoordinate;
}

export interface IGlobalNetworkLinear {
    networkVersion: Record<string, any>;
    linearGeometryType: string;
    startPoint?: IPointByMultiCoordinates;
    endPoint?: IPointByMultiCoordinates;
    linearWithinLinearGNElement: Record<string, any> | Array<Record<string, any>>;
}

interface ILatLonCoordinate {
    latitude: string;
    longitude: string;
}

interface IOpenlrLineAttributes {
    openlrFunctionalRoadClass: string;
    openlrFormOfWay: string;
    openlrBearing: string;
}

interface IOpenlrLastLocationReferencePoint {
    openlrCoordinate: ILatLonCoordinate;
    openlrLineAttributes: IOpenlrLineAttributes;
}

interface IOpenlrLocationReferencePoint extends IOpenlrLastLocationReferencePoint {
    openlrPathAttributes: {
        openlrLowestFRCToNextLRPoint: string;
        openlrDistanceToNextLRPoint: string; // number
    };
}

interface IOpenlrLineLocationReference {
    openlrLocationReferencePoint: IOpenlrLocationReferencePoint | IOpenlrLocationReferencePoint[];
    openlrLastLocationReferencePoint: IOpenlrLastLocationReferencePoint;
    openlrOffsets?: {
        openlrPositiveOffset: string; // number
        openlrNegativeOffset: string; // number
    };
}

export interface ILinearExtension extends Record<string, any> {
    openlrExtendedLinear: {
        firstDirection: IOpenlrLineLocationReference;
        oppositeDirection?: IOpenlrLineLocationReference;
    };
}

export interface IOutputApiSituationRecord {
    id: string;
    situationRecordCreationTime: string;
    situationRecordVersionTime: string;
    startTime: string;
    endTime: string;
    type: OutputApiSituationRecordType | null;
    version: number;
    generalPublicComment: { cs: string } | null;
    sourceName?: string;
    impact: Record<string, any>;
    forVehiclesWithCharacteristicsOf?: Record<string, any> | Array<Record<string, any>>;
    alertCLinear?: {
        type: string | null;
        alertCLocationCountryCode: string | null;
        alertCLocationTableNumber: string | null;
        alertCLocationTableVersion: string | null;
        alertCDirection: {
            alertCDirectionCoded: string | null;
        };
        alertCMethod2PrimaryPointLocation: {
            alertCLocation: {
                specificLocation: string;
            };
        };
        alertCMethod2SecondaryPointLocation: {
            alertCLocation: {
                specificLocation: string;
            };
        };
    };
    osmPath?: number[][];
}

export enum OutputApiSituationRecordType {
    ConstructionWorks = "CONSTRUCTION_WORKS",
    GeneralObstruction = "GENERAL_OBSTRUCTION",
    MaintenanceWorks = "MAINTENANCE_WORKS",
    RoadOrCarriagewayOrLaneManagement = "ROAD_OR_CARRIAGE_WAY_OR_LANE_MANAGEMENT",
}

export interface IOutputApiSituation {
    id: string;
    version: number;
    situationRecord: IOutputApiSituationRecord[];
}

export interface IOutputApiRestrictions {
    modelBaseVersion: string;
    situationPublicationLight: {
        lang: string;
        publicationTime: string;
        publicationCreator: {
            country: string;
            nationalIdentifier: string;
        };
        situation: IOutputApiSituation[];
    };
}

interface IGroupOfLocations {
    $: {
        "xsi:type": string;
    };
    supplementaryPositionalDescription?: Record<string, any>;
    destination?: Record<string, any>;
    alertCLinear?: IAlertCLinear;
    linearWithinLinearElement?: Record<string, any>;
    globalNetworkLinear?: IGlobalNetworkLinear;
    linearExtension?: ILinearExtension;
}

interface ISituationRecordCommon {
    $: ISituationRecordIdentifier;
    situationRecordCreationTime: string;
    situationRecordVersionTime: string;
    probabilityOfOccurrence: string;
    source?: Record<string, any>;
    validity: {
        validityStatus: string;
        validityTimeSpecification: {
            overallStartTime: string;
            overallEndTime: string;
        };
    };
    impact?: {
        capacityRemaining?: string; // number;
        numberOfLanesRestricted?: string; // number;
        numberOfOperationalLanes?: string; // number;
        originalNumberOfLanes?: string; // number;
        trafficConstrictionType?: string;
        delays?: {
            delaysType: string;
            delayTimeValue?: string; // number;
        };
    };
    cause?: Record<string, any>;
    generalPublicComment?: Record<string, any>;
    groupOfLocations: IGroupOfLocations;
    situationRecordExtension?: Record<string, any>;
}

// Abstract types
interface IObstruction {
    numberOfObstructions?: string; //number;
    mobilityOfObstruction?: Record<string, any>;
}

interface INetworkManagement {
    complianceOption: string;
    applicableForTrafficDirection?: string;
    applicableForTrafficType?: string;
    placesAtWhichApplicable?: string;
    forVehiclesWithCharacteristicsOf?: Record<string, any>;
}

interface IRoadworks {
    roadworksDuration?: string;
    roadworksScale?: string;
    underTraffic?: boolean;
    urgentRoadworks?: boolean;
    mobility?: Record<string, any>;
    subjects?: Record<string, any>;
    maintenanceVehicles?: Record<string, any>;
}

// Situation Record Types
interface IAbnormalTraffic extends ISituationRecordCommon {
    abnormalTrafficType?: string;
    relativeTrafficFlow?: string;
    trafficTrendType?: string;
}

interface IAccident extends ISituationRecordCommon {
    accidentType: string | string[];
    totalNumberOfVehiclesInvolved?: string; // number
    vehicleInvolved?: Record<string, any>;
    groupOfVehiclesInvolved?: Record<string, any>;
    groupOfPeopleInvolved?: Record<string, any>;
}

interface IAnimalPresenceObstruction extends IObstruction, ISituationRecordCommon {
    animalPresenceType: string;
    alive?: boolean;
}

interface IAuthorityOperation extends ISituationRecordCommon {
    authorityOperationType: string;
}

interface ICarParks extends ISituationRecordCommon {
    carParkConfiguration?: string;
    carParkIdentity: string;
    carParkOccupancy?: string;
    carParkStatus?: string;
    numberOfVacantParkingSpaces?: string; // number
    occupiedSpaces?: string; // number
    totalCapacity?: string; // number
}

interface IConditions extends ISituationRecordCommon {
    drivingConditionType: string;
}

interface IConstructionWorks extends IRoadworks, ISituationRecordCommon {
    constructionWorkType: string;
}

interface IDisturbanceActivity extends ISituationRecordCommon {
    disturbanceActivityType: string;
}

interface IEnvironmentalObstruction extends IObstruction, ISituationRecordCommon {
    depth?: string; // number
    environmentalObstructionType: string;
}

interface IEquipmentOrSystemFault extends ISituationRecordCommon {
    equipmentOrSystemFaultType: string;
    faultyEquipmentOrSystemType: string;
}

interface IGeneralInstructionOrMessageToRoadUsers extends INetworkManagement, ISituationRecordCommon {
    generalInstructionToRoadUsersType?: string;
    generalMessageToRoadUsers?: Record<string, any>;
}

interface IGeneralNetworkManagement extends INetworkManagement, ISituationRecordCommon {
    generalNetworkManagementType: string;
    trafficManuallyDirectedBy: string;
}

interface IGeneralObstruction extends IObstruction, ISituationRecordCommon {
    obstructionType: string | string[];
    groupOfPeopleInvolved?: Record<string, any>;
}

interface IInfrastructureDamageObstruction extends IObstruction, ISituationRecordCommon {
    infrastructureDamageType: string;
}

interface IMaintenanceWorks extends IRoadworks, ISituationRecordCommon {
    roadMaintenanceType: string | string[];
}
interface INonWeatherRelatedRoadConditions extends ISituationRecordCommon {
    nonWeatherRelatedRoadConditionType: string | string[];
}

interface IPoorEnvironmentConditions extends ISituationRecordCommon {
    poorEnvironmentType: string | string[];
    precipitationDetail?: Record<string, any>;
    visibility?: Record<string, any>;
    temperature?: Record<string, any>;
    wind?: Record<string, any>;
}

interface IPublicEvent extends ISituationRecordCommon {
    publicEventType: string;
}

interface IReroutingManagement extends INetworkManagement, ISituationRecordCommon {
    reroutingManagementType: string | string[];
    reroutingItineraryDescription?: Record<string, any>;
    signedRerouting?: boolean;
    entry?: string;
    exit?: string;
    roadOrJunctionNumber?: string;
}

interface IRoadOperatorServiceDisruption extends ISituationRecordCommon {
    roadOperatorServiceDisruptionType: string | string[];
}

interface IRoadOrCarriagewayOrLaneManagement extends INetworkManagement, ISituationRecordCommon {
    roadOrCarriagewayOrLaneManagementType: string;
    minimumCarOccupancy?: string; // number
}

interface IRoadsideAssistance extends ISituationRecordCommon {
    roadsideAssistanceType: string;
}

interface IRoadsideServiceDisruption extends ISituationRecordCommon {
    roadsideServiceDisruptionType: string | string[];
}

interface ISpeedManagement extends INetworkManagement, ISituationRecordCommon {
    speedManagementType: string;
    temporarySpeedLimit: string; // number
}

interface ITransitInformation extends ISituationRecordCommon {
    transitServiceInformation: string;
    transitServiceType: string;
}

interface IVehicleObstruction extends IObstruction, ISituationRecordCommon {
    vehicleObstructionType: string;
    obstructingVehicle?: Record<string, any>;
}

interface IWeatherRelatedRoadConditions extends ISituationRecordCommon {
    weatherRelatedRoadConditionType: string | string[];
    roadSurfaceConditionMeasurements?: Record<string, any>;
}

interface IWinterDrivingManagement extends INetworkManagement, ISituationRecordCommon {
    winterEquipmentManagementType: string;
}

export type ISituationRecord =
    | IAbnormalTraffic
    | IAccident
    | IAnimalPresenceObstruction
    | IAuthorityOperation
    | ICarParks
    | IConditions
    | IConstructionWorks
    | IDisturbanceActivity
    | IEnvironmentalObstruction
    | IEquipmentOrSystemFault
    | IGeneralInstructionOrMessageToRoadUsers
    | IGeneralNetworkManagement
    | IGeneralObstruction
    | IInfrastructureDamageObstruction
    | IMaintenanceWorks
    | INonWeatherRelatedRoadConditions
    | IPoorEnvironmentConditions
    | IPublicEvent
    | IReroutingManagement // no data
    | IRoadOperatorServiceDisruption // no data
    | IRoadOrCarriagewayOrLaneManagement
    | IRoadsideAssistance // no data
    | IRoadsideServiceDisruption
    | ISpeedManagement // no data
    | ITransitInformation // no data
    | IVehicleObstruction
    | IWeatherRelatedRoadConditions
    | IWinterDrivingManagement; // no data

export type ISituationRecordGeneral = IAbnormalTraffic &
    IAccident &
    IAnimalPresenceObstruction &
    IAuthorityOperation &
    ICarParks &
    IConditions &
    IConstructionWorks &
    IDisturbanceActivity &
    IEnvironmentalObstruction &
    IEquipmentOrSystemFault &
    IGeneralInstructionOrMessageToRoadUsers &
    IGeneralNetworkManagement &
    IGeneralObstruction &
    IInfrastructureDamageObstruction &
    IMaintenanceWorks &
    INonWeatherRelatedRoadConditions &
    IPoorEnvironmentConditions &
    IPublicEvent &
    IReroutingManagement & // no data
    IRoadOperatorServiceDisruption & // no data
    IRoadOrCarriagewayOrLaneManagement &
    IRoadsideAssistance & // no data
    IRoadsideServiceDisruption &
    ISpeedManagement & // no data
    ITransitInformation & // no data
    IVehicleObstruction &
    IWeatherRelatedRoadConditions &
    IWinterDrivingManagement; // no data

export interface ITrafficRestrictionsModel extends ISituationModel {
    exchange: Record<string, any>;
    publication_time: string; // date
    publication_creator: Record<string, any>;
}

export interface ISituationModel extends ISituationRecordModel {
    situation_id: string;
    situation_version: string;
    situation_version_time: string; // date
    situation_confidentiality: string;
    situation_information_status: string;
    situation_urgency: string;
}

export interface ISituationRecordLocationModel {
    group_of_locations_type: string;
    supplementary_positional_description?: Record<string, any>;
    destination?: Record<string, any>;
    alert_c_linear?: Record<string, any>;
    linear_within_linear_element?: Record<string, any>;
    global_network_linear?: Record<string, any>;
    linear_extension?: Record<string, any>;
    osm_path?: number[][];
}

export interface ISituationRecordModel extends ISituationRecordLocationModel {
    situation_record_id: string;
    situation_record_version: string;
    situation_record_type: string;
    situation_record_creation_time: string; // date
    situation_record_version_time: string; // date
    probability_of_occurrence: string;
    source?: string;
    validity_status: string;
    validity_overall_start_time: string;
    validity_overall_end_time: string;
    impact_capacity_remaining?: number;
    impact_number_of_lanes_restricted?: number;
    impact_number_of_operational_lanes?: number;
    impact_original_number_of_lanes?: number;
    impact_traffic_constriction_type?: string;
    impact_delays_type?: string;
    impact_delay_time_value?: number;
    cause?: Record<string, any>;
    general_public_comment?: string;
    situation_record_extension?: Record<string, any>;
    number_of_obstructions?: number;
    mobility_of_obstruction?: Record<string, any>;
    compliance_option?: string;
    applicable_for_traffic_direction?: string;
    applicable_for_traffic_type?: string;
    places_at_which_applicable?: string;
    for_vehicles_with_characteristics_of?: Record<string, any>;
    roadworks_duration?: string;
    roadworks_scale?: string;
    under_traffic?: boolean;
    urgent_roadworks?: boolean;
    mobility?: Record<string, any>;
    subjects?: Record<string, any>;
    maintenance_vehicles?: Record<string, any>;
    abnormal_traffic_type?: string;
    accident_type?: string;
    group_of_vehicles_involved?: Record<string, any>;
    car_park_identity?: string;
    car_park_occupancy?: number;
    number_of_vacant_parking_spaces?: number;
    driving_condition_type?: string;
    construction_work_type?: string;
    disturbance_activity_type?: string;
    environmental_obstruction_type?: string;
    general_instruction_to_road_users_type?: string;
    general_message_to_road_users?: Record<string, any>;
    general_network_management_type?: string;
    traffic_manually_directed_by?: string;
    infrastructure_damage_type?: string;
    road_maintenance_type?: string;
    non_weather_related_road_condition_type?: string;
    poor_environment_type?: string;
    precipitation_detail?: Record<string, any>;
    public_event_type?: string;
    rerouting_management_type?: string;
    rerouting_itinerary_description?: Record<string, any>;
    signed_rerouting?: boolean;
    entry?: string;
    exit?: string;
    road_or_junction_number?: string;
    road_or_carriageway_or_lane_management_type?: string;
    minimum_car_occupancy?: number;
    roadside_assistance_type?: string;
    speed_management_type?: string;
    temporary_speed_limit?: number;
    vehicle_obstruction_type?: string;
    obstructing_vehicle?: Record<string, any>;
    winter_equipment_management_type?: string;
    alert_c_direction?: string;
    geom_openlr_line?: { coordinates: any[][]; type: string };
    geom_gn_line?: { coordinates: any[][]; type: string };
    geom_alert_c_line?: { coordinates: any[][]; type: string };
}
