import { Ndic } from "#sch";
import { PostgresModel } from "@golemio/core/dist/integration-engine";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class RsdTmcPointsModel extends PostgresModel {
    public constructor() {
        super(
            Ndic.rsd_tmc_points.name + "Model",
            {
                outputSequelizeAttributes: Ndic.rsd_tmc_points.outputSequelizeAttributes,
                pgSchema: "traffic",
                pgTableName: Ndic.rsd_tmc_points.pgTableName,
                savingType: "insertOnly",
                sequelizeAdditionalSettings: {
                    timestamps: false,
                },
            },
            new JSONSchemaValidator("RsdTmcPointsModel", {})
        );
    }

    /**
     * @param {number} lcdValue Options object with params
     * @returns Array of the retrieved records
     */

    public GetAll = async (lcdValue: number): Promise<any> => {
        try {
            const rsdTmcPointsFromDB: any[] = await this.sequelizeModel.findAll({
                raw: true,
                where: { lcd: lcdValue },
            });

            return rsdTmcPointsFromDB;
        } catch (err) {
            throw new GeneralError("Database error", "rsdTmcPointsModel", err, 500);
        }
    };

    public GetOne = async (lcdValue: number): Promise<any | null> => {
        const objFromDb: any[] = await this.GetAll(lcdValue);
        return objFromDb[0];
    };
}
