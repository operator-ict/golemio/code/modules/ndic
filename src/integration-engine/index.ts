import { NdicWorker } from "./NdicWorker";

/* ie/index.ts */
export * from "./LegacyNdicWorker";
export * from "./queueDefinitions";

export const workers = [NdicWorker];
