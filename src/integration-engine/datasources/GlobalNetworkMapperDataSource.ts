import { ISimpleConfig } from "@golemio/core/dist/helpers/configuration/ISimpleConfig";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { HTTPRequestProtocolStrategyStreamed } from "@golemio/core/dist/integration-engine/datasources/protocol-strategy/HTTPRequestProtocolStrategyStreamed";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";

@injectable()
export class GlobalNetworkMapperDataSource {
    public datasource: HTTPRequestProtocolStrategyStreamed;

    constructor(@inject(CoreToken.SimpleConfig) private config: ISimpleConfig) {
        this.datasource = new HTTPRequestProtocolStrategyStreamed({
            headers: {},
            method: "GET",
            url: this.config.getValue<string>("module.ndic.globalNetworkMapperUrl"),
        });
    }
}
