import { GlobalNetworkMapperDataSource } from "#ie/datasources/GlobalNetworkMapperDataSource";
import { OsmPathService } from "#ie/helpers/OsmPathService";
import { RsdTmcPointsModel } from "#ie/models/RsdTmcPointsModel";
import { GlobalNetworkMappingRepository } from "#ie/repository/GlobalNetworkMappingRepository";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { IntegrationEngineContainer } from "@golemio/core/dist/integration-engine/ioc/Di";
import { DependencyContainer, instanceCachingFactory, Lifecycle } from "@golemio/core/dist/shared/tsyringe";
import { RsdTmcOsmMappingRepository } from "@golemio/traffic-common/dist/integration-engine/repositories/RsdTmcOsmMappingRepository";
import { ModuleContainerToken } from "./ContainerToken";

//#region Initialization
const NdicContainer: DependencyContainer = IntegrationEngineContainer.createChildContainer();
//#endregion

//#region Datasource
NdicContainer.register(ModuleContainerToken.GlobalNetworkMapperDataSource, GlobalNetworkMapperDataSource, {
    lifecycle: Lifecycle.Transient,
});
//#endregion

//#region Transformation
//#endregion

//#region Repositories
NdicContainer.register(ModuleContainerToken.RsdTmcPointsRepository, RsdTmcPointsModel, { lifecycle: Lifecycle.Singleton });
NdicContainer.register(ModuleContainerToken.GlobalNetworkMappingRepository, GlobalNetworkMappingRepository, {
    lifecycle: Lifecycle.Singleton,
});
//#endregion

//#region Services
NdicContainer.register(ModuleContainerToken.OsmPathService, {
    useFactory: instanceCachingFactory((c) => {
        return new OsmPathService(
            c.resolve(ModuleContainerToken.GlobalNetworkMappingRepository),
            new RsdTmcOsmMappingRepository(
                c.resolve<IDatabaseConnector>(CoreToken.PostgresConnector),
                c.resolve<ILogger>(CoreToken.Logger)
            ),
            c.resolve<ILogger>(CoreToken.Logger)
        );
    }),
});
//#endregion

//#region Tasks
//#endregion

export { NdicContainer };
