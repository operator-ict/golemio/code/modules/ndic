const ModuleContainerToken = {
    GlobalNetworkMapperDataSource: Symbol(),
    RsdTmcPointsRepository: Symbol(),
    GlobalNetworkMappingRepository: Symbol(),
    OsmPathService: Symbol(),
};

export { ModuleContainerToken };
