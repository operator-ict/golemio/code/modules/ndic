import { config, IQueueDefinition } from "@golemio/core/dist/integration-engine";
import { Ndic } from "#sch";
import { LegacyNdicWorker } from "#ie/LegacyNdicWorker";

const queueDefinitions: IQueueDefinition[] = [
    {
        name: Ndic.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + Ndic.name.toLowerCase(),
        queues: [
            {
                name: "saveTrafficInfo",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 59 * 60 * 1000, // 59 minutes
                },
                worker: LegacyNdicWorker,
                workerMethod: "saveTrafficInfo",
            },
        ],
    },
    {
        name: Ndic.name,
        queuePrefix: config.RABBIT_EXCHANGE_NAME + "." + Ndic.name.toLowerCase(),
        queues: [
            {
                name: "saveTrafficRestrictions",
                options: {
                    deadLetterExchange: config.RABBIT_EXCHANGE_NAME,
                    deadLetterRoutingKey: "dead",
                    messageTtl: 59 * 60 * 1000, // 59 minutes
                },
                worker: LegacyNdicWorker,
                workerMethod: "saveTrafficRestrictions",
            },
        ],
    },
];

export { queueDefinitions };
