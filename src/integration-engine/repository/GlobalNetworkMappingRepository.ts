import { Ndic } from "#sch";
import { IGlobalNetworkOsmMapping } from "#sch/schemas/interfaces/IGlobalNetworkOsmMapping";
import { GlobalNetworkOsmMappingModel } from "#sch/schemas/models/GlobalNetworkOsmMapping";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { AbstractBasicRepository } from "@golemio/core/dist/helpers/data-access/postgres/repositories/AbstractBasicRepository";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { ModelStatic, QueryTypes, Sequelize } from "@golemio/core/dist/shared/sequelize";
import { inject, injectable } from "@golemio/core/dist/shared/tsyringe";
import { from as copyFrom } from "pg-copy-streams";
import { Readable } from "stream";

@injectable()
export class GlobalNetworkMappingRepository extends AbstractBasicRepository {
    public schema: string = Ndic.pgSchema;
    public tableName: string = GlobalNetworkOsmMappingModel.TABLE_NAME;
    public model: ModelStatic<GlobalNetworkOsmMappingModel>;

    constructor(@inject(CoreToken.PostgresConnector) connector: IDatabaseConnector, @inject(CoreToken.Logger) logger: ILogger) {
        super(connector, logger);
        this.model = GlobalNetworkOsmMappingModel.init(GlobalNetworkOsmMappingModel.attributeModel, {
            tableName: this.tableName,
            schema: this.schema,
            sequelize: this.connector.getConnection(),
            timestamps: false,
        });
    }

    public async streamData(data: Readable) {
        const connection = this.connector.getConnection();
        const tableIdentifier = `"${this.schema}"."${this.tableName}"`;
        const tmpTableIdentifier = `"${this.schema}"."${this.tableName}_tmp"`;
        try {
            await connection.query(`DROP TABLE IF EXISTS ${tmpTableIdentifier};`, {
                plain: true,
                type: QueryTypes.RAW,
            });
            await connection.query(`CREATE TABLE ${tmpTableIdentifier} (LIKE ${tableIdentifier} INCLUDING ALL);`, {
                plain: true,
                type: QueryTypes.RAW,
            });

            await this.streamDataToTmp(tmpTableIdentifier, data);
            const oldTableIdentifier = `"${this.schema}"."${this.tableName}_old"`;

            await connection.query(
                `begin;
                alter table ${tableIdentifier} rename to ${this.tableName}_old;
                alter table ${tmpTableIdentifier} rename to ${this.tableName};
            end;`,
                {
                    plain: true,
                    type: QueryTypes.RAW,
                }
            );
            await connection.query(`DROP TABLE IF EXISTS ${oldTableIdentifier};`, {
                plain: true,
                type: QueryTypes.RAW,
            });
        } catch (err) {
            this.cleanAfterError(connection, tmpTableIdentifier);
            throw new GeneralError(`Error during streaming data: ${err.toString()}`, this.constructor.name, err);
        }
    }

    public async findAll(roadIds: string[]): Promise<IGlobalNetworkOsmMapping[]> {
        return this.model.findAll({
            where: {
                road_id: roadIds,
            },
            raw: true,
        });
    }

    private async streamDataToTmp(tableName: string, data: Readable) {
        const connection = this.connector.getConnection();
        const client: any = await connection.connectionManager.getConnection({ type: "write" });

        // copy transformed data to tmp table by stream
        await new Promise<void>((resolve, reject) => {
            const stream = client
                .query(
                    copyFrom(
                        `COPY ${tableName}
                        FROM STDIN DELIMITER ',' CSV HEADER;`
                    )
                )
                .on("error", async (err: any) => {
                    this.log.error(`Copying data error (${this.constructor.name}): ${err.toString()}`);
                    await connection.connectionManager.releaseConnection(client);
                    return reject(err);
                })
                .on("finish", async () => {
                    this.log.info(`Done copying data (${this.constructor.name})`);
                    await connection.connectionManager.releaseConnection(client);
                    return resolve();
                });
            data.pipe(stream);
        });
    }

    private async cleanAfterError(connection: Sequelize, tmpTableIdentifier: string) {
        try {
            await connection.query(`DROP TABLE IF EXISTS ${tmpTableIdentifier};`, {
                plain: true,
                type: QueryTypes.RAW,
            });
        } catch (err) {
            this.log.error(new GeneralError(`Error during cleaning after error: ${err.toString()}`, this.constructor.name, err));
        }
    }
}
