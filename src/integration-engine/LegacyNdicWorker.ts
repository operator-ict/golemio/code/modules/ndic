import { ITrafficInfoModel } from "#ie/transformations/TrafficInfoInterface";
import { TrafficInfoTransformation } from "#ie/transformations/TrafficInfoTransformation";
import { TrafficRestrictionsTransformation } from "#ie/transformations/TrafficRestrictionsTransformation";
import { Ndic } from "#sch";
import { BaseWorker, config, PostgresModel } from "@golemio/core/dist/integration-engine";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { OsmPathService } from "./helpers/OsmPathService";
import { TrafficRestrictionsHelper } from "./helpers/TrafficRestrictionsHelper";
import { ModuleContainerToken } from "./ioc/ContainerToken";
import { NdicContainer } from "./ioc/Di";

export class LegacyNdicWorker extends BaseWorker {
    private readonly queuePrefix: string;
    private ndicTrafficInfoTransformation: TrafficInfoTransformation;
    private ndicTrafficRestrictionsTransformation: TrafficRestrictionsTransformation;

    private ndicTrafficInfoModel: PostgresModel;
    private ndicTrafficRestrictionsModel: PostgresModel;

    constructor() {
        super();
        this.queuePrefix = config.RABBIT_EXCHANGE_NAME + "." + Ndic.name.toLowerCase();

        this.ndicTrafficInfoTransformation = new TrafficInfoTransformation();
        this.ndicTrafficRestrictionsTransformation = new TrafficRestrictionsTransformation();

        this.ndicTrafficInfoModel = new PostgresModel(
            Ndic.traffic_info.name + "Model",
            {
                outputSequelizeAttributes: Ndic.traffic_info.outputSequelizeAttributes,
                pgSchema: Ndic.pgSchema,
                pgTableName: Ndic.traffic_info.pgTableName,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(Ndic.traffic_info.name + "PgModelValidator", Ndic.traffic_info.outputSchema)
        );

        this.ndicTrafficRestrictionsModel = new PostgresModel(
            Ndic.traffic_restrictions.name + "Model",
            {
                outputSequelizeAttributes: Ndic.traffic_restrictions.outputSequelizeAttributes,
                pgSchema: Ndic.pgSchema,
                pgTableName: Ndic.traffic_restrictions.pgTableName,
                savingType: "insertOnly",
            },
            new JSONSchemaValidator(Ndic.traffic_restrictions.name + "PgModelValidator", Ndic.traffic_restrictions.outputSchema)
        );
    }

    public saveTrafficInfo = async (msg: any): Promise<void> => {
        const data = JSON.parse(msg.content.toString());
        const transformedData = await this.ndicTrafficInfoTransformation.transform(data);
        const dataToSave = await this.populateWithGeomAlertCLineInfo(transformedData);

        await this.ndicTrafficInfoModel.save(this.uniqueInfoArray(dataToSave));
    };

    public saveTrafficRestrictions = async (msg: any): Promise<void> => {
        const osmPathService = NdicContainer.resolve<OsmPathService>(ModuleContainerToken.OsmPathService);
        const data = JSON.parse(msg.content.toString());
        const transformedData = await this.ndicTrafficRestrictionsTransformation.transform(data);
        let dataToSave = await TrafficRestrictionsHelper.populateWithGeomAlertCLineRestrictions(transformedData);
        dataToSave = await osmPathService.addOsmPath(dataToSave);

        await this.ndicTrafficRestrictionsModel.save(TrafficRestrictionsHelper.uniqueRestrictionsArray(dataToSave));
    };

    private uniqueInfoArray = (arr: ITrafficInfoModel[]) => {
        const constraintsArr: string[] = [];
        const resultArr: ITrafficInfoModel[] = [];
        for (const obj of arr) {
            if (!constraintsArr.includes(obj.situation_id + obj.situation_record_type + obj.situation_record_version_time)) {
                constraintsArr.push(obj.situation_id + obj.situation_record_type + obj.situation_record_version_time);
                resultArr.push(obj);
            }
        }
        return resultArr;
    };

    private populateWithGeomAlertCLineInfo = async (transformedData: ITrafficInfoModel[]): Promise<ITrafficInfoModel[]> => {
        for (const dataObj of transformedData) {
            dataObj.geom_alert_c_line = await TrafficRestrictionsHelper.parseGeomAlertCLine(dataObj.alert_c_linear);
        }
        return transformedData;
    };
}
