import { GlobalNetworkMappingRepository } from "#ie/repository/GlobalNetworkMappingRepository";
import { ITrafficRestrictionsModel } from "#ie/transformations/TrafficRestrictionsInterface";
import { ILogger } from "@golemio/core/dist/helpers/logger/LoggerProvider";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { injectable } from "@golemio/core/dist/shared/tsyringe";
import { RsdTmcOsmMappingRepository } from "@golemio/traffic-common/dist/integration-engine/repositories/RsdTmcOsmMappingRepository";

@injectable()
export class OsmPathService {
    constructor(
        private globalNetworkMappingRepository: GlobalNetworkMappingRepository,
        private rsdTmcOsmMappingRepository: RsdTmcOsmMappingRepository,
        private logger: ILogger
    ) {}

    public async addOsmPath(transformedData: ITrafficRestrictionsModel[]): Promise<ITrafficRestrictionsModel[]> {
        for (let index = 0; index < transformedData.length; index++) {
            const record = transformedData[index];
            try {
                if (record.alert_c_linear) {
                    let ltStart: string =
                        record.alert_c_linear?.alertCMethod2SecondaryPointLocation.alertCLocation.specificLocation || null;
                    let ltEnd: string =
                        record.alert_c_linear?.alertCMethod2PrimaryPointLocation.alertCLocation.specificLocation || null;

                    if (ltStart === "0") {
                        // handles special case for freeway ramps
                        ltStart = ltEnd;
                    } else if (ltEnd === "0") {
                        ltEnd = ltStart;
                    }

                    const osmPath = await this.rsdTmcOsmMappingRepository.GetOne({
                        ltStart: Number(ltStart),
                        ltEnd: Number(ltEnd),
                    });
                    record.osm_path = osmPath ? [osmPath] : undefined;
                }

                if (!record.osm_path && record.global_network_linear) {
                    const roadIds = this.getGlobalNetworkSectionIds(record);
                    // global_network_linear, currently we do not differentiate between positive and negative direction
                    const result = await this.globalNetworkMappingRepository.findAll(roadIds);
                    record.osm_path = result.map((element) => element.osm_path.split(",").map((text) => +text));

                    if (record.osm_path.length === 0) {
                        record.osm_path = undefined;
                    }
                }
            } catch (error) {
                if (error instanceof AbstractGolemioError) {
                    this.logger.warn(error);
                } else {
                    this.logger.warn(
                        new GeneralError(
                            // eslint-disable-next-line max-len
                            `Error while adding OSM path to the record ${record.situation_record_id} ${record.situation_version_time}`,
                            this.constructor.name,
                            error
                        )
                    );
                }
            }
        }

        return transformedData;
    }

    private getGlobalNetworkSectionIds(record: ITrafficRestrictionsModel): string[] {
        // if array
        if (Array.isArray(record.global_network_linear!.linearWithinLinearGNElement)) {
            return record.global_network_linear!.linearWithinLinearGNElement.map((element) => element.sectionId);
        }
        // if object
        return [record.global_network_linear!.linearWithinLinearGNElement.sectionId];
    }
}
