import { ITrafficRestrictionsModel } from "#ie/transformations/TrafficRestrictionsInterface";
import { TrafficCommonContainer } from "@golemio/traffic-common/dist/integration-engine/ioc/Di";
import { ModuleContainerToken as TrafficCommonToken } from "@golemio/traffic-common/dist/integration-engine/ioc/ModuleContainerToken";
import { LteCze90PointsSjtskInAreaRepository } from "@golemio/traffic-common/dist/integration-engine/repositories/LteCze90PointsSjtskInAreaRepository";

export class LteCze90PointsChecker {
    private filtrationLookup: LteCze90PointsSjtskInAreaRepository;
    private relevantLcds: number[] = [];

    constructor() {
        this.filtrationLookup = TrafficCommonContainer.resolve<LteCze90PointsSjtskInAreaRepository>(
            TrafficCommonToken.LteCze90PointsSjtskInAreaRepository
        );
    }

    public async initialize(): Promise<void> {
        this.relevantLcds = (await this.filtrationLookup.getFiltrationData()).map((element) => element.lcd);
    }

    public shouldSave(input: ITrafficRestrictionsModel): boolean {
        const primaryPointSpecificLocation =
            input.alert_c_linear?.alertCMethod2PrimaryPointLocation.alertCLocation.specificLocation;
        const secondaryPointSpecificLocation =
            input.alert_c_linear?.alertCMethod2SecondaryPointLocation.alertCLocation.specificLocation;

        if (
            primaryPointSpecificLocation === undefined ||
            primaryPointSpecificLocation === null ||
            secondaryPointSpecificLocation === undefined ||
            secondaryPointSpecificLocation === null
        ) {
            return false;
        }

        return (
            this.relevantLcds.includes(Number(primaryPointSpecificLocation)) ||
            this.relevantLcds.includes(Number(secondaryPointSpecificLocation))
        );
    }
}
