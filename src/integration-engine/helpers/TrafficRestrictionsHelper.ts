import { ModuleContainerToken } from "#ie/ioc/ContainerToken";
import { NdicContainer } from "#ie/ioc/Di";
import { RsdTmcPointsModel } from "#ie/models/RsdTmcPointsModel";
import { ITrafficRestrictionsModel } from "#ie/transformations/TrafficRestrictionsInterface";
import { LineString, Position } from "@golemio/core/dist/shared/geojson";

export class TrafficRestrictionsHelper {
    //#region Public Methods
    public static uniqueRestrictionsArray = (arr: ITrafficRestrictionsModel[]) => {
        const constraintsArr: string[] = [];
        const resultArr: ITrafficRestrictionsModel[] = [];
        for (const obj of arr) {
            if (!constraintsArr.includes(obj.situation_record_id + obj.situation_record_version_time)) {
                constraintsArr.push(obj.situation_record_id + obj.situation_record_version_time);
                resultArr.push(obj);
            }
        }
        return resultArr;
    };

    public static populateWithGeomAlertCLineRestrictions = async (
        transformedData: ITrafficRestrictionsModel[]
    ): Promise<ITrafficRestrictionsModel[]> => {
        for (const dataObj of transformedData) {
            dataObj.geom_alert_c_line = await this.parseGeomAlertCLine(dataObj.alert_c_linear);
        }
        return transformedData;
    };

    public static parseGeomAlertCLine = async (
        alertCLinearObj: Record<string, any> | undefined
    ): Promise<LineString | undefined> => {
        if (!alertCLinearObj) return undefined;

        const rsTmcRepository = NdicContainer.resolve<RsdTmcPointsModel>(ModuleContainerToken.RsdTmcPointsRepository);
        const primaryPointSpecificLocation = alertCLinearObj.alertCMethod2PrimaryPointLocation.alertCLocation.specificLocation;
        const secondaryPointSpecificLocation =
            alertCLinearObj.alertCMethod2SecondaryPointLocation.alertCLocation.specificLocation;

        const primaryPointObj = await rsTmcRepository.GetOne(primaryPointSpecificLocation);
        const secondaryPointObj = await rsTmcRepository.GetOne(secondaryPointSpecificLocation);

        if (primaryPointObj && secondaryPointObj) {
            const coordinatesArr: Position[] = [
                [Number(primaryPointObj.wgs84_x.replace(",", ".")), Number(primaryPointObj.wgs84_y.replace(",", "."))],
                [Number(secondaryPointObj.wgs84_x.replace(",", ".")), Number(secondaryPointObj.wgs84_y.replace(",", "."))],
            ];
            return {
                coordinates: coordinatesArr,
                type: "LineString",
            } as LineString;
        }
        return undefined;
    };
    //#endregion
}
