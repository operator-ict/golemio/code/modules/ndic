import { Ndic } from "#sch";
import { AbstractWorker } from "@golemio/core/dist/integration-engine";
import { RefreshGlobalNetworkDataTask } from "./tasks/RefreshGlobalNetworkDataTask";
import { RegionsDataRetention } from "./tasks/RegionsDataRetention";
import { SaveTrafficRestrictionsRegions } from "./tasks/SaveTrafficRestrictionsRegions";

export class NdicWorker extends AbstractWorker {
    protected name: string = Ndic.name;

    constructor() {
        super();

        this.registerTask(new SaveTrafficRestrictionsRegions(this.getQueuePrefix()));
        this.registerTask(new RegionsDataRetention(this.getQueuePrefix()));
        this.registerTask(new RefreshGlobalNetworkDataTask(this.getQueuePrefix()));
    }
}
