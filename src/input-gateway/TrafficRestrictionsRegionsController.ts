import { Ndic } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { AbstractTrafficRestrictionsController } from "./AbstractTrafficRestrictionsController";

export class TrafficRestrictionsRegionsController extends AbstractTrafficRestrictionsController {
    protected saveQueueKey = "saveTrafficRestrictionsRegions";

    constructor() {
        super(new JSONSchemaValidator("NdicTrafficRestrictionsRegionsValidator", Ndic.traffic_restrictions.datasourceSchema));
    }
}
