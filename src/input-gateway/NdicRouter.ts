import { TrafficInfoController } from "#ig/TrafficInfoController";
import { TrafficRestrictionsPragueController } from "#ig/TrafficRestrictionsPragueController";
import { LoggerEmitter, LoggerEventType } from "@golemio/core/dist/helpers/logger/LoggerEmitter";
import { NumberOfRecordsEventData } from "@golemio/core/dist/helpers/logger/interfaces/INumberOfRecordsEventData";
import { checkContentTypeMiddleware } from "@golemio/core/dist/input-gateway/helpers";
import { ContainerToken, InputGatewayContainer } from "@golemio/core/dist/input-gateway/ioc";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { AbstractTrafficRestrictionsController } from "./AbstractTrafficRestrictionsController";
import { TrafficRestrictionsRegionsController } from "./TrafficRestrictionsRegionsController";

class NdicRouter {
    public router: Router = Router();
    private loggerEmitter: LoggerEmitter;

    private trafficInfoController = new TrafficInfoController();
    private trafficRestrictionsPragueController = new TrafficRestrictionsPragueController();
    private trafficRestrictionsRegionsController = new TrafficRestrictionsRegionsController();

    constructor() {
        this.loggerEmitter = InputGatewayContainer.resolve<LoggerEmitter>(ContainerToken.LoggerEmitter);
        this.initRoutes();
    }

    private initRoutes = (): void => {
        this.router.post("/traffic-info", checkContentTypeMiddleware(["text/xml", "application/xml"]), this.trafficInfo);
        this.router.post(
            "/traffic-restrictions",
            checkContentTypeMiddleware(["text/xml", "application/xml"]),
            this.trafficRestrictionPrague
        );
        this.router.post(
            "/traffic-restrictions-regions",
            checkContentTypeMiddleware(["text/xml", "application/xml"]),
            this.trafficRestrictionRegions
        );
    };

    private trafficInfo = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        try {
            await this.trafficInfoController.processData(req.body);

            const numberOfRecords = (
                JSON.stringify(req.body.d2LogicalModel.payloadPublication.situation || {}).match(/situationRecord/g) || []
            ).length;

            const metrics: NumberOfRecordsEventData = {
                numberOfRecords,
                req,
            };
            this.loggerEmitter.emit(LoggerEventType.NumberOfRecords, metrics);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };

    private trafficRestrictionPrague = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        await this.trafficRestriction(this.trafficRestrictionsPragueController, req, res, next);
    };

    private trafficRestrictionRegions = async (req: Request, res: Response, next: NextFunction): Promise<void> => {
        await this.trafficRestriction(this.trafficRestrictionsRegionsController, req, res, next);
    };

    private trafficRestriction = async (
        controller: AbstractTrafficRestrictionsController,
        req: Request,
        res: Response,
        next: NextFunction
    ): Promise<void> => {
        try {
            await controller.processData(req.body);

            const numberOfRecords = (
                JSON.stringify(req.body.d2LogicalModel.payloadPublication.situation || {}).match(/situationRecord/g) || []
            ).length;

            const metrics: NumberOfRecordsEventData = {
                numberOfRecords,
                req,
            };
            this.loggerEmitter.emit(LoggerEventType.NumberOfRecords, metrics);

            res.sendStatus(204);
        } catch (err) {
            next(err);
        }
    };
}

const ndicRouter = new NdicRouter().router;

export { ndicRouter };
