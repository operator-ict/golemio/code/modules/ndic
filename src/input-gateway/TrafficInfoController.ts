import { BaseController } from "@golemio/core/dist/input-gateway";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { Ndic } from "#sch";

export class TrafficInfoController extends BaseController {
    constructor() {
        super(Ndic.name, new JSONSchemaValidator(Ndic.traffic_info.name, Ndic.traffic_info.datasourceSchema));
    }

    public processData = async (data: any): Promise<void> => {
        try {
            await this.validator.Validate(data);

            await this.sendMessageToExchange(
                "input." + this.queuePrefix + ".saveTrafficInfo",
                JSON.stringify(data.d2LogicalModel),
                {
                    persistent: true,
                }
            );
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.name, err, 422);
            }
        }
    };
}
