import { Ndic } from "#sch";
import { BaseController } from "@golemio/core/dist/input-gateway";
import { AbstractGolemioError, GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { IValidator } from "@golemio/core/dist/shared/golemio-validator";

export abstract class AbstractTrafficRestrictionsController extends BaseController {
    protected abstract saveQueueKey: string;

    constructor(validator: IValidator) {
        super(Ndic.name, validator);
    }

    public processData = async (data: any): Promise<void> => {
        try {
            await this.validator.Validate(data);

            await this.sendMessageToExchange(
                "input." + this.queuePrefix + "." + this.saveQueueKey,
                JSON.stringify(data.d2LogicalModel),
                {
                    persistent: true,
                }
            );
        } catch (err) {
            if (err instanceof AbstractGolemioError) {
                throw err;
            } else {
                throw new GeneralError("Error while validating input data.", this.name, err, 422);
            }
        }
    };
}
