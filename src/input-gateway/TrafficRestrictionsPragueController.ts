import { Ndic } from "#sch";
import { JSONSchemaValidator } from "@golemio/core/dist/shared/golemio-validator";
import { AbstractTrafficRestrictionsController } from "./AbstractTrafficRestrictionsController";

export class TrafficRestrictionsPragueController extends AbstractTrafficRestrictionsController {
    protected saveQueueKey = "saveTrafficRestrictions";

    constructor() {
        super(new JSONSchemaValidator("NdicTrafficRestrictionsPragueValidator", Ndic.traffic_restrictions.datasourceSchema));
    }
}
