import Sequelize from "@golemio/core/dist/shared/sequelize";

export const outputRsdTmcPointsSDMA: Sequelize.ModelAttributes<any> = {
    lcd: { type: Sequelize.NUMBER, primaryKey: true },
    wgs84_x: { type: Sequelize.STRING(1024) },
    wgs84_y: { type: Sequelize.STRING(1024) },
};

export const outputRsdTmcPointsSchema = {
    type: "object",
    required: ["lcd", "wgs84_x", "wgs84_y"],
    additionalProperties: true,
    properties: {
        lcd: { type: "number" },
        wgs84_x: { type: "string" },
        wgs84_y: { type: "string" },
    },
};
