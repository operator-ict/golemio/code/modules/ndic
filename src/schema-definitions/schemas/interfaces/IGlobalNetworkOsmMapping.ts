export interface IGlobalNetworkOsmMapping {
    road_id: string;
    direction: string;
    osm_path: string;
}
