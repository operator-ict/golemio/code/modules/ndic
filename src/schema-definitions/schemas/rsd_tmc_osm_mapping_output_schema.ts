import Sequelize from "@golemio/core/dist/shared/sequelize";

// outputSequelizeAttributes
export const outputRsdTmcOsmMappingSDMA: Sequelize.ModelAttributes<any> = {
    lt_start: { type: Sequelize.INTEGER },
    lt_end: { type: Sequelize.INTEGER },
    osm_path: {
        type: Sequelize.STRING,
    },
};

export const outputRsdTmcOsmMappingSchema = {
    type: "object",
    required: ["lt_start", "lt_end", "osm_path"],
    additionalProperties: false,
    properties: {
        lt_start: { type: "number" },
        lt_end: { type: "number" },
        osm_path: { type: "string" },
    },
};
