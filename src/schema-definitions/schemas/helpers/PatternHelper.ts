export const INTEGER_REGEX_PATTERN = "^[0-9]+$";
export const NUMBER_REGEX_PATTERN = "^[0-9]+(.[0-9]+){0,1}$";
