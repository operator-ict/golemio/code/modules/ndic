import { JSONSchemaType } from "@golemio/core/dist/shared/ajv";
import { Model, ModelAttributes } from "@golemio/core/dist/shared/sequelize";
import { IGlobalNetworkOsmMapping } from "../interfaces/IGlobalNetworkOsmMapping";

export class GlobalNetworkOsmMappingModel extends Model<GlobalNetworkOsmMappingModel> implements IGlobalNetworkOsmMapping {
    public static TABLE_NAME = "global_network_osm_mapping";

    declare road_id: string;
    declare direction: string;
    declare osm_path: string;

    public static attributeModel: ModelAttributes<GlobalNetworkOsmMappingModel> = {
        road_id: { type: "string", primaryKey: true },
        direction: { type: "string", primaryKey: true },
        osm_path: { type: "string" },
    };

    public static jsonSchema: JSONSchemaType<IGlobalNetworkOsmMapping[]> = {
        type: "array",
        items: {
            type: "object",
            required: ["road_id", "direction", "osm_path"],
            additionalProperties: false,
            properties: {
                road_id: { type: "string" },
                direction: { type: "string" },
                osm_path: { type: "string" },
            },
        },
    };
}
