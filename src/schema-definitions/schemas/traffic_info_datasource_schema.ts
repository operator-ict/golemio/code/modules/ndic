import { INTEGER_REGEX_PATTERN, NUMBER_REGEX_PATTERN } from "./helpers/PatternHelper";

export const datasourceTrafficInfoSchema = {
    title: "Traffic Info schema",
    type: "object",
    properties: {
        d2LogicalModel: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        modelBaseVersion: {
                            type: "string",
                            enum: ["2"],
                        },
                    },
                    required: ["modelBaseVersion"],
                    additionalProperties: true,
                },
                exchange: {
                    $ref: "#/definitions/Exchange",
                },
                payloadPublication: {
                    $ref: "#/definitions/PayloadPublication",
                },
            },
            required: ["$", "exchange", "payloadPublication"],
            additionalProperties: false,
        },
    },
    required: ["d2LogicalModel"],
    additionalProperties: false,
    definitions: {
        _ExtensionType: {
            type: "object",
        },
        AbnormalTraffic: {
            type: "object",
            properties: {
                abnormalTrafficType: {
                    type: "string",
                },
                relativeTrafficFlow: {
                    type: "string",
                },
                trafficTrendType: {
                    type: "string",
                },
                abnormalTrafficExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
        },
        Accident: {
            type: "object",
            properties: {
                accidentType: {
                    oneOf: [
                        {
                            type: "string",
                        },
                        {
                            items: {
                                type: "string",
                            },
                            type: "array",
                        },
                    ],
                },
                totalNumberOfVehiclesInvolved: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                vehicleInvolved: {
                    oneOf: [
                        {
                            type: "object",
                        },
                        {
                            items: {
                                type: "object",
                            },
                            type: "array",
                        },
                    ],
                },
                groupOfVehiclesInvolved: {
                    oneOf: [
                        {
                            type: "object",
                        },
                        {
                            items: {
                                type: "object",
                            },
                            type: "array",
                        },
                    ],
                },
                groupOfPeopleInvolved: {
                    oneOf: [
                        {
                            type: "object",
                        },
                        {
                            items: {
                                type: "object",
                            },
                            type: "array",
                        },
                    ],
                },
                accidentExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["accidentType"],
        },
        AnimalPresenceObstruction: {
            type: "object",
            properties: {
                alive: {
                    type: "boolean",
                },
                animalPresenceType: {
                    type: "string",
                },
                animalPresenceObstructionExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["animalPresenceType"],
        },
        AuthorityOperation: {
            type: "object",
            properties: {
                authorityOperationType: {
                    type: "string",
                },
                authorityOperationExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["authorityOperationType"],
        },
        CarParks: {
            type: "object",
            properties: {
                carParkConfiguration: {
                    type: "string",
                },
                carParkIdentity: {
                    type: "string",
                },
                carParkOccupancy: {
                    type: "string",
                    pattern: NUMBER_REGEX_PATTERN,
                },
                carParkStatus: {
                    type: "string",
                },
                numberOfVacantParkingSpaces: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                occupiedSpaces: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                totalCapacity: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                carParksExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["carParkIdentity"],
        },
        Conditions: {
            type: "object",
            properties: {
                drivingConditionType: {
                    type: "string",
                },
                conditionsExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
        },
        ConstructionWorks: {
            allOf: [
                {
                    $ref: "#/definitions/Roadworks",
                },
                {
                    type: "object",
                    properties: {
                        constructionWorkType: {
                            type: "string",
                        },
                        constructionWorksExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                },
            ],
        },
        Delays: {
            type: "object",
            properties: {
                delaysType: {
                    type: "string",
                },
                delayTimeValue: {
                    type: "string",
                    pattern: NUMBER_REGEX_PATTERN,
                },
            },
            additionalProperties: false,
        },
        DisturbanceActivity: {
            type: "object",
            properties: {
                disturbanceActivityType: {
                    type: "string",
                },
                disturbanceActivityExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["disturbanceActivityType"],
        },
        EnvironmentalObstruction: {
            allOf: [
                {
                    $ref: "#/definitions/Obstruction",
                },
                {
                    type: "object",
                    properties: {
                        depth: {
                            type: "string",
                            pattern: NUMBER_REGEX_PATTERN,
                        },
                        environmentalObstructionType: {
                            type: "string",
                        },
                        environmentalObstructionExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["environmentalObstructionType"],
                },
            ],
        },
        EquipmentOrSystemFault: {
            type: "object",
            properties: {
                equipmentOrSystemFaultType: {
                    type: "string",
                },
                faultyEquipmentOrSystemType: {
                    type: "string",
                },
                equipmentOrSystemFaultExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["equipmentOrSystemFaultType", "faultyEquipmentOrSystemType"],
        },
        Exchange: {
            type: "object",
            properties: {
                supplierIdentification: {
                    $ref: "#/definitions/InternationalIdentifier",
                },
                exchangeExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["supplierIdentification"],
        },
        GeneralInstructionOrMessageToRoadUsers: {
            allOf: [
                {
                    $ref: "#/definitions/NetworkManagement",
                },
                {
                    type: "object",
                    properties: {
                        generalInstructionToRoadUsersType: {
                            type: "string",
                        },
                        generalMessageToRoadUsers: {
                            type: "object",
                        },
                        generalInstructionOrMessageToRoadUsersExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                },
            ],
        },
        GeneralNetworkManagement: {
            allOf: [
                {
                    $ref: "#/definitions/NetworkManagement",
                },
                {
                    type: "object",
                    properties: {
                        generalNetworkManagementType: {
                            type: "string",
                        },
                        trafficManuallyDirectedBy: {
                            type: "string",
                        },
                        generalNetworkManagementExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["generalNetworkManagementType"],
                },
            ],
        },
        GeneralObstruction: {
            allOf: [
                {
                    $ref: "#/definitions/Obstruction",
                },
                {
                    type: "object",
                    properties: {
                        obstructionType: {
                            oneOf: [
                                {
                                    type: "string",
                                },
                                {
                                    items: {
                                        type: "string",
                                    },
                                    type: "array",
                                },
                            ],
                        },
                        groupOfPeopleInvolved: {
                            oneOf: [
                                {
                                    type: "object",
                                },
                                {
                                    items: {
                                        type: "object",
                                    },
                                    type: "array",
                                },
                            ],
                        },
                        generalObstructionExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["obstructionType"],
                },
            ],
        },
        GroupOfLocations: {
            allOf: [
                {
                    type: "object",
                    properties: {
                        $: {
                            type: "object",
                            properties: {
                                "xsi:type": {
                                    type: "string",
                                },
                                lang: {
                                    type: "string",
                                },
                            },
                            required: ["xsi:type"],
                            additionalProperties: false,
                        },
                    },
                    required: ["$"],
                },
                {
                    anyOf: [
                        {
                            $ref: "#/definitions/Linear",
                        },
                    ],
                },
            ],
        },
        HeaderInformation: {
            type: "object",
            properties: {
                confidentiality: {
                    type: "string",
                },
                informationStatus: {
                    type: "string",
                },
                urgency: {
                    type: "string",
                },
            },
            required: ["confidentiality", "informationStatus"],
            additionalProperties: false,
        },
        Impact: {
            type: "object",
            properties: {
                capacityRemaining: {
                    type: "string",
                    pattern: NUMBER_REGEX_PATTERN,
                },
                numberOfLanesRestricted: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                numberOfOperationalLanes: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                originalNumberOfLanes: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                trafficConstrictionType: {
                    type: "string",
                },
                delays: {
                    $ref: "#/definitions/Delays",
                },
            },
            additionalProperties: false,
        },
        InfrastructureDamageObstruction: {
            allOf: [
                {
                    $ref: "#/definitions/Obstruction",
                },
                {
                    type: "object",
                    properties: {
                        infrastructureDamageType: {
                            type: "string",
                        },
                        infrastructureDamageObstructionExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["infrastructureDamageType"],
                },
            ],
        },
        InternationalIdentifier: {
            type: "object",
            properties: {
                country: {
                    type: "string",
                },
                nationalIdentifier: {
                    type: "string",
                },
                internationalIdentifierExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["country", "nationalIdentifier"],
            additionalProperties: false,
        },
        Linear: {
            allOf: [
                {
                    $ref: "#/definitions/NetworkLocation",
                },
                {
                    type: "object",
                    properties: {
                        alertCLinear: {
                            type: "object",
                        },
                        linearWithinLinearElement: {
                            type: "object",
                        },
                        globalNetworkLinear: {
                            type: "object",
                        },
                        linearExtension: {
                            type: "object",
                        },
                    },
                },
            ],
        },
        MaintenanceWorks: {
            allOf: [
                {
                    $ref: "#/definitions/Roadworks",
                },
                {
                    type: "object",
                    properties: {
                        roadMaintenanceType: {
                            oneOf: [
                                {
                                    type: "string",
                                },
                                {
                                    items: {
                                        type: "string",
                                    },
                                    type: "array",
                                },
                            ],
                        },
                        maintenanceWorksExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["roadMaintenanceType"],
                },
            ],
        },
        NetworkLocation: {
            type: "object",
            properties: {
                supplementaryPositionalDescription: {
                    type: "object",
                },
                destination: {
                    type: "object",
                },
            },
        },
        NetworkManagement: {
            type: "object",
            properties: {
                complianceOption: {
                    type: "string",
                },
                applicableForTrafficDirection: {
                    oneOf: [
                        {
                            type: "string",
                        },
                        {
                            items: {
                                type: "string",
                            },
                            type: "array",
                        },
                    ],
                },
                applicableForTrafficType: {
                    oneOf: [
                        {
                            type: "string",
                        },
                        {
                            items: {
                                type: "string",
                            },
                            type: "array",
                        },
                    ],
                },
                placesAtWhichApplicable: {
                    oneOf: [
                        {
                            type: "string",
                        },
                        {
                            items: {
                                type: "string",
                            },
                            type: "array",
                        },
                    ],
                },
                forVehiclesWithCharacteristicsOf: {
                    oneOf: [
                        {
                            type: "object",
                        },
                        {
                            items: {
                                type: "object",
                            },
                            type: "array",
                        },
                    ],
                },
            },
            required: ["complianceOption"],
        },
        NonWeatherRelatedRoadConditions: {
            allOf: [
                {
                    $ref: "#/definitions/Conditions",
                },
                {
                    type: "object",
                    properties: {
                        nonWeatherRelatedRoadConditionType: {
                            oneOf: [
                                {
                                    type: "string",
                                },
                                {
                                    items: {
                                        type: "string",
                                    },
                                    type: "array",
                                },
                            ],
                        },
                        nonWeatherRelatedRoadConditionsExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["nonWeatherRelatedRoadConditionType"],
                },
            ],
        },
        Obstruction: {
            type: "object",
            properties: {
                numberOfObstructions: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                mobilityOfObstruction: {
                    type: "object",
                },
            },
        },
        OverallPeriod: {
            type: "object",
            properties: {
                overallStartTime: {
                    $ref: "#/definitions/DateTime",
                },
                overallEndTime: {
                    $ref: "#/definitions/DateTime",
                },
            },
            required: ["overallStartTime"],
            additionalProperties: false,
        },
        PayloadPublication: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        "xsi:type": {
                            type: "string",
                            enum: ["SituationPublication"],
                        },
                        lang: {
                            type: "string",
                        },
                    },
                    required: ["xsi:type"],
                    additionalProperties: false,
                },
                publicationTime: {
                    $ref: "#/definitions/DateTime",
                },
                publicationCreator: {
                    $ref: "#/definitions/InternationalIdentifier",
                },
                situation: {
                    oneOf: [
                        {
                            $ref: "#/definitions/Situation",
                        },
                        {
                            items: {
                                $ref: "#/definitions/Situation",
                            },
                            type: "array",
                        },
                    ],
                },
                situationPublicationExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["$", "publicationTime", "publicationCreator", "situation"],
            additionalProperties: false,
        },
        PoorEnvironmentConditions: {
            allOf: [
                {
                    $ref: "#/definitions/Conditions",
                },
                {
                    type: "object",
                    properties: {
                        poorEnvironmentType: {
                            oneOf: [
                                {
                                    type: "string",
                                },
                                {
                                    items: {
                                        type: "string",
                                    },
                                    type: "array",
                                },
                            ],
                        },
                        precipitationDetail: {
                            type: "object",
                        },
                        visibility: {
                            type: "object",
                        },
                        temperature: {
                            type: "object",
                        },
                        wind: {
                            type: "object",
                        },
                        poorEnvironmentConditionsExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["poorEnvironmentType"],
                },
            ],
        },
        PublicEvent: {
            type: "object",
            properties: {
                publicEventType: {
                    type: "string",
                },
                publicEventExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["publicEventType"],
        },
        ReroutingManagement: {
            allOf: [
                {
                    $ref: "#/definitions/NetworkManagement",
                },
                {
                    type: "object",
                    properties: {
                        reroutingManagementType: {
                            oneOf: [
                                {
                                    type: "string",
                                },
                                {
                                    items: {
                                        type: "string",
                                    },
                                    type: "array",
                                },
                            ],
                        },
                        reroutingItineraryDescription: {
                            type: "object",
                        },
                        signedRerouting: {
                            type: "boolean",
                        },
                        entry: {
                            type: "string",
                        },
                        exit: {
                            type: "string",
                        },
                        roadOrJunctionNumber: {
                            type: "string",
                        },
                        reroutingManagementExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["reroutingManagementType"],
                },
            ],
        },
        RoadOperatorServiceDisruption: {
            type: "object",
            properties: {
                roadOperatorServiceDisruptionType: {
                    oneOf: [
                        {
                            type: "string",
                        },
                        {
                            items: {
                                type: "string",
                            },
                            type: "array",
                        },
                    ],
                },
                roadOperatorServiceDisruptionExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["roadOperatorServiceDisruptionType"],
        },
        RoadOrCarriagewayOrLaneManagement: {
            allOf: [
                {
                    $ref: "#/definitions/NetworkManagement",
                },
                {
                    type: "object",
                    properties: {
                        roadOrCarriagewayOrLaneManagementType: {
                            type: "string",
                        },
                        minimumCarOccupancy: {
                            type: "string",
                            pattern: INTEGER_REGEX_PATTERN,
                        },
                        roadOrCarriagewayOrLaneManagementExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["roadOrCarriagewayOrLaneManagementType"],
                },
            ],
        },
        RoadsideAssistance: {
            type: "object",
            properties: {
                roadsideAssistanceType: {
                    type: "string",
                },
                roadsideAssistanceExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["roadsideAssistanceType"],
        },
        RoadsideServiceDisruption: {
            type: "object",
            properties: {
                roadsideServiceDisruptionType: {
                    oneOf: [
                        {
                            type: "string",
                        },
                        {
                            items: {
                                type: "string",
                            },
                            type: "array",
                        },
                    ],
                },
                roadsideServiceDisruptionExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["roadsideServiceDisruptionType"],
        },
        Roadworks: {
            type: "object",
            properties: {
                roadworksDuration: {
                    type: "string",
                },
                roadworksScale: {
                    type: "string",
                },
                underTraffic: {
                    type: "boolean",
                },
                urgentRoadworks: {
                    type: "boolean",
                },
                mobility: {
                    type: "object",
                },
                subjects: {
                    type: "object",
                },
                maintenanceVehicles: {
                    type: "object",
                },
            },
        },
        Situation: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        id: {
                            type: "string",
                        },
                        version: {
                            type: "string",
                        },
                    },
                    required: ["id", "version"],
                    additionalProperties: false,
                },
                situationVersionTime: {
                    $ref: "#/definitions/DateTime",
                },
                headerInformation: {
                    $ref: "#/definitions/HeaderInformation",
                },
                situationRecord: {
                    oneOf: [
                        {
                            $ref: "#/definitions/SituationRecord",
                        },
                        {
                            items: {
                                $ref: "#/definitions/SituationRecord",
                            },
                            type: "array",
                        },
                    ],
                },
                situationExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["$", "headerInformation", "situationRecord"],
            additionalProperties: false,
        },
        SituationRecord: {
            allOf: [
                {
                    type: "object",
                    properties: {
                        $: {
                            type: "object",
                            properties: {
                                id: {
                                    type: "string",
                                },
                                "xsi:type": {
                                    type: "string",
                                },
                                version: {
                                    type: "string",
                                },
                            },
                            required: ["id", "version"],
                            additionalProperties: false,
                        },
                        situationRecordCreationTime: {
                            $ref: "#/definitions/DateTime",
                        },
                        situationRecordVersionTime: {
                            $ref: "#/definitions/DateTime",
                        },
                        probabilityOfOccurrence: {
                            type: "string",
                        },
                        source: {
                            type: "object",
                        },
                        validity: {
                            $ref: "#/definitions/Validity",
                        },
                        impact: {
                            $ref: "#/definitions/Impact",
                        },
                        cause: {
                            type: "object",
                        },
                        generalPublicComment: {
                            oneOf: [
                                {
                                    type: "object",
                                },
                                {
                                    items: {
                                        type: "object",
                                    },
                                    type: "array",
                                },
                            ],
                        },
                        groupOfLocations: {
                            $ref: "#/definitions/GroupOfLocations",
                        },
                        situationRecordExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: [
                        "situationRecordCreationTime",
                        "situationRecordVersionTime",
                        "probabilityOfOccurrence",
                        "validity",
                        "groupOfLocations",
                    ],
                },
                {
                    anyOf: [
                        {
                            $ref: "#/definitions/AbnormalTraffic",
                        },
                        {
                            $ref: "#/definitions/Accident",
                        },
                        {
                            $ref: "#/definitions/AnimalPresenceObstruction",
                        },
                        {
                            $ref: "#/definitions/AuthorityOperation",
                        },
                        {
                            $ref: "#/definitions/CarParks",
                        },
                        {
                            $ref: "#/definitions/Conditions",
                        },
                        {
                            $ref: "#/definitions/ConstructionWorks",
                        },
                        {
                            $ref: "#/definitions/DisturbanceActivity",
                        },
                        {
                            $ref: "#/definitions/EnvironmentalObstruction",
                        },
                        {
                            $ref: "#/definitions/EquipmentOrSystemFault",
                        },
                        {
                            $ref: "#/definitions/GeneralInstructionOrMessageToRoadUsers",
                        },
                        {
                            $ref: "#/definitions/GeneralNetworkManagement",
                        },
                        {
                            $ref: "#/definitions/GeneralObstruction",
                        },
                        {
                            $ref: "#/definitions/InfrastructureDamageObstruction",
                        },
                        {
                            $ref: "#/definitions/MaintenanceWorks",
                        },
                        {
                            $ref: "#/definitions/NonWeatherRelatedRoadConditions",
                        },
                        {
                            $ref: "#/definitions/PoorEnvironmentConditions",
                        },
                        {
                            $ref: "#/definitions/PublicEvent",
                        },
                        {
                            $ref: "#/definitions/ReroutingManagement",
                        },
                        {
                            $ref: "#/definitions/RoadOperatorServiceDisruption",
                        },
                        {
                            $ref: "#/definitions/RoadOrCarriagewayOrLaneManagement",
                        },
                        {
                            $ref: "#/definitions/RoadsideAssistance",
                        },
                        {
                            $ref: "#/definitions/RoadsideServiceDisruption",
                        },
                        {
                            $ref: "#/definitions/SpeedManagement",
                        },
                        {
                            $ref: "#/definitions/TransitInformation",
                        },
                        {
                            $ref: "#/definitions/VehicleObstruction",
                        },
                        {
                            $ref: "#/definitions/WeatherRelatedRoadConditions",
                        },
                        {
                            $ref: "#/definitions/WinterDrivingManagement",
                        },
                    ],
                },
            ],
        },
        SpeedManagement: {
            allOf: [
                {
                    $ref: "#/definitions/NetworkManagement",
                },
                {
                    type: "object",
                    properties: {
                        speedManagementType: {
                            type: "string",
                        },
                        temporarySpeedLimit: {
                            type: "string",
                            pattern: NUMBER_REGEX_PATTERN,
                        },
                        speedManagementExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                },
            ],
        },
        TransitInformation: {
            type: "object",
            properties: {
                transitServiceInformation: {
                    type: "string",
                },
                transitServiceType: {
                    type: "string",
                },
                transitInformationExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["transitServiceInformation", "transitServiceType"],
        },
        Validity: {
            type: "object",
            properties: {
                validityStatus: {
                    type: "string",
                },
                validityTimeSpecification: {
                    $ref: "#/definitions/OverallPeriod",
                },
            },
            required: ["validityStatus", "validityTimeSpecification"],
            additionalProperties: false,
        },
        VehicleObstruction: {
            allOf: [
                {
                    $ref: "#/definitions/Obstruction",
                },
                {
                    type: "object",
                    properties: {
                        vehicleObstructionType: {
                            type: "string",
                        },
                        obstructingVehicle: {
                            oneOf: [
                                {
                                    type: "object",
                                },
                                {
                                    items: {
                                        type: "object",
                                    },
                                    type: "array",
                                },
                            ],
                        },
                        vehicleObstructionExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["vehicleObstructionType"],
                },
            ],
        },
        WeatherRelatedRoadConditions: {
            allOf: [
                {
                    $ref: "#/definitions/Conditions",
                },
                {
                    type: "object",
                    properties: {
                        weatherRelatedRoadConditionType: {
                            oneOf: [
                                {
                                    type: "string",
                                },
                                {
                                    items: {
                                        type: "string",
                                    },
                                    type: "array",
                                },
                            ],
                        },
                        roadSurfaceConditionMeasurements: {
                            type: "object",
                        },
                        weatherRelatedRoadConditionsExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["weatherRelatedRoadConditionType"],
                },
            ],
        },
        WinterDrivingManagement: {
            allOf: [
                {
                    $ref: "#/definitions/NetworkManagement",
                },
                {
                    type: "object",
                    properties: {
                        winterEquipmentManagementType: {
                            type: "string",
                        },
                        winterDrivingManagementExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: ["winterEquipmentManagementType"],
                },
            ],
        },
        DateTime: {
            type: "string",
            format: "date-time",
        },
        openlrCoordinate: {
            type: "object",
            properties: {
                latitude: { type: "string" },
                longitude: { type: "string" },
            },
            required: ["latitude", "longitude"],
        },
        openlrLineAttributes: {
            type: "object",
            properties: {
                openlrFunctionalRoadClass: { type: "string" },
                openlrFormOfWay: { type: "string" },
                openlrBearing: { type: "string" },
            },
        },
        openlrPathAttributes: {
            type: "object",
            properties: {
                openlrLowestFRCToNextLRPoint: { type: "string" },
                openlrDistanceToNextLRPoint: { type: "string" },
            },
        },
        openlrLocationReferencePoint: {
            type: "object",
            properties: {
                openlrCoordinate: { $ref: "#/definitions/openlrCoordinate" },
                openlrLineAttributes: { $ref: "#/definitions/openlrLineAttributes" },
                openlrPathAttributes: { $ref: "#/definitions/openlrPathAttributes" },
            },
            required: ["openlrCoordinate"],
        },
        openlrOffsets: {
            type: "object",
            properties: {
                openlrPositiveOffset: { type: "string" },
                openlrNegativeOffset: { type: "string" },
            },
        },
    },
};
