import { INTEGER_REGEX_PATTERN, NUMBER_REGEX_PATTERN } from "./helpers/PatternHelper";

export const datasourceTrafficRestrictionsSchema = {
    title: "Traffic Restrictions schema",
    type: "object",
    properties: {
        d2LogicalModel: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        modelBaseVersion: {
                            type: "string",
                            enum: ["2"],
                        },
                    },
                    required: ["modelBaseVersion"],
                    additionalProperties: true,
                },
                exchange: {
                    $ref: "#/definitions/Exchange",
                },
                payloadPublication: {
                    $ref: "#/definitions/PayloadPublication",
                },
            },
            required: ["$", "exchange", "payloadPublication"],
            additionalProperties: false,
        },
    },
    required: ["d2LogicalModel"],
    additionalProperties: false,
    definitions: {
        _ExtensionType: {
            type: "object",
        },
        Exchange: {
            type: "object",
            properties: {
                supplierIdentification: {
                    $ref: "#/definitions/InternationalIdentifier",
                },
                exchangeExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["supplierIdentification"],
        },
        InternationalIdentifier: {
            type: "object",
            properties: {
                country: {
                    type: "string",
                },
                nationalIdentifier: {
                    type: "string",
                },
                internationalIdentifierExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["country", "nationalIdentifier"],
            additionalProperties: false,
        },
        PayloadPublication: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        "xsi:type": {
                            type: "string",
                            enum: ["SituationPublication"],
                        },
                        lang: {
                            type: "string",
                        },
                    },
                    required: ["xsi:type"],
                    additionalProperties: false,
                },
                publicationTime: {
                    $ref: "#/definitions/DateTime",
                },
                publicationCreator: {
                    $ref: "#/definitions/InternationalIdentifier",
                },
                situation: {
                    oneOf: [
                        {
                            $ref: "#/definitions/Situation",
                        },
                        {
                            items: {
                                $ref: "#/definitions/Situation",
                            },
                            type: "array",
                        },
                    ],
                },
                situationPublicationExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["$", "publicationTime", "publicationCreator", "situation"],
            additionalProperties: false,
        },
        DateTime: {
            type: "string",
            format: "date-time",
        },
        HeaderInformation: {
            type: "object",
            properties: {
                confidentiality: {
                    type: "string",
                },
                informationStatus: {
                    type: "string",
                },
                urgency: {
                    type: "string",
                },
            },
            required: ["confidentiality", "informationStatus"],
            additionalProperties: false,
        },
        Situation: {
            type: "object",
            properties: {
                $: {
                    type: "object",
                    properties: {
                        id: {
                            type: "string",
                        },
                        version: {
                            type: "string",
                        },
                    },
                    required: ["id", "version"],
                    additionalProperties: false,
                },
                situationVersionTime: {
                    $ref: "#/definitions/DateTime",
                },
                headerInformation: {
                    $ref: "#/definitions/HeaderInformation",
                },
                situationRecord: {
                    oneOf: [
                        {
                            $ref: "#/definitions/SituationRecord",
                        },
                        {
                            items: {
                                $ref: "#/definitions/SituationRecord",
                            },
                            type: "array",
                        },
                    ],
                },
                situationExtension: {
                    $ref: "#/definitions/_ExtensionType",
                },
            },
            required: ["$", "headerInformation", "situationRecord"],
            additionalProperties: false,
        },
        SituationRecord: {
            allOf: [
                {
                    type: "object",
                    properties: {
                        $: {
                            type: "object",
                            properties: {
                                id: {
                                    type: "string",
                                },
                                "xsi:type": {
                                    type: "string",
                                },
                                version: {
                                    type: "string",
                                },
                            },
                            required: ["id", "version"],
                            additionalProperties: false,
                        },
                        situationRecordCreationTime: {
                            $ref: "#/definitions/DateTime",
                        },
                        situationRecordVersionTime: {
                            $ref: "#/definitions/DateTime",
                        },
                        probabilityOfOccurrence: {
                            type: "string",
                        },
                        source: {
                            type: "object",
                        },
                        validity: {
                            $ref: "#/definitions/Validity",
                        },
                        impact: {
                            $ref: "#/definitions/Impact",
                        },
                        cause: {
                            type: "object",
                        },
                        generalPublicComment: {
                            oneOf: [
                                {
                                    type: "object",
                                },
                                {
                                    items: {
                                        type: "object",
                                    },
                                    type: "array",
                                },
                            ],
                        },
                        groupOfLocations: {
                            $ref: "#/definitions/GroupOfLocations",
                        },
                        situationRecordExtension: {
                            $ref: "#/definitions/_ExtensionType",
                        },
                    },
                    required: [
                        "situationRecordCreationTime",
                        "situationRecordVersionTime",
                        "probabilityOfOccurrence",
                        "validity",
                        "groupOfLocations",
                    ],
                },
            ],
        },
        Validity: {
            type: "object",
            properties: {
                validityStatus: {
                    type: "string",
                },
                validityTimeSpecification: {
                    $ref: "#/definitions/OverallPeriod",
                },
            },
            required: ["validityStatus", "validityTimeSpecification"],
            additionalProperties: false,
        },
        OverallPeriod: {
            type: "object",
            properties: {
                overallStartTime: {
                    $ref: "#/definitions/DateTime",
                },
                overallEndTime: {
                    $ref: "#/definitions/DateTime",
                },
            },
            required: ["overallStartTime"],
            additionalProperties: false,
        },
        Impact: {
            type: "object",
            properties: {
                capacityRemaining: {
                    type: "string",
                    pattern: NUMBER_REGEX_PATTERN,
                },
                numberOfLanesRestricted: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                numberOfOperationalLanes: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                originalNumberOfLanes: {
                    type: "string",
                    pattern: INTEGER_REGEX_PATTERN,
                },
                trafficConstrictionType: {
                    type: "string",
                },
                delays: {
                    $ref: "#/definitions/Delays",
                },
            },
            additionalProperties: false,
        },
        Delays: {
            type: "object",
            properties: {
                delaysType: {
                    type: "string",
                },
                delayTimeValue: {
                    type: "string",
                    pattern: NUMBER_REGEX_PATTERN,
                },
            },
            additionalProperties: false,
        },
        GroupOfLocations: {
            allOf: [
                {
                    type: "object",
                    properties: {
                        $: {
                            type: "object",
                            properties: {
                                "xsi:type": {
                                    type: "string",
                                },
                                lang: {
                                    type: "string",
                                },
                            },
                            required: ["xsi:type"],
                            additionalProperties: false,
                        },
                    },
                    required: ["$"],
                },
                {
                    anyOf: [
                        {
                            $ref: "#/definitions/Linear",
                        },
                    ],
                },
            ],
        },
        Linear: {
            allOf: [
                {
                    type: "object",
                    properties: {
                        alertCLinear: {
                            type: "object",
                        },
                        linearWithinLinearElement: {
                            type: "object",
                        },
                        globalNetworkLinear: {
                            type: "object",
                        },
                        linearExtension: {
                            type: "object",
                        },
                    },
                },
            ],
        },
    },
};
