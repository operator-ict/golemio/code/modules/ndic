import { outputRsdTmcPointsSchema, outputRsdTmcPointsSDMA } from "#sch/schemas/rsd_tmc_points_output_schema";
import { datasourceTrafficInfoSchema } from "#sch/schemas/traffic_info_datasource_schema";
import { outputTrafficInfoSchema, outputTrafficInfoSDMA } from "#sch/schemas/traffic_info_output_schema";
import { datasourceTrafficRestrictionsSchema } from "#sch/schemas/traffic_restrictions_datasource_schema";
import { outputTrafficRestrictionsSchema, outputTrafficRestrictionsSDMA } from "#sch/schemas/traffic_restrictions_output_schema";
import { ModelAttributes } from "@golemio/core/dist/shared/sequelize";

const forExport = {
    name: "Ndic",
    pgSchema: "ndic",
    traffic_info: {
        name: "NdicTrafficInfo",
        datasourceSchema: datasourceTrafficInfoSchema,
        outputSchema: outputTrafficInfoSchema,
        outputSequelizeAttributes: outputTrafficInfoSDMA as ModelAttributes,
        pgTableName: "ndic_traffic_info",
    },
    traffic_restrictions: {
        name: "NdicTrafficRestrictions",
        datasourceSchema: datasourceTrafficRestrictionsSchema,
        outputSchema: outputTrafficRestrictionsSchema,
        outputSequelizeAttributes: outputTrafficRestrictionsSDMA,
        pgTableName: "ndic_traffic_restrictions",
    },
    rsd_tmc_points: {
        name: "RsdTmcPoints",
        outputSchema: outputRsdTmcPointsSchema,
        outputSequelizeAttributes: outputRsdTmcPointsSDMA,
        pgTableName: "rsd_tmc_points",
    },
};

export { forExport as Ndic };
