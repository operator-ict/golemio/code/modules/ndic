import { parseNumber } from "#ie/transformations/ParserHelper";
import {
    IOutputApiRestrictions,
    IOutputApiSituation,
    IOutputApiSituationRecord,
    ITrafficRestrictionsModel,
    OutputApiSituationRecordType,
} from "#ie/transformations/TrafficRestrictionsInterface";
import { Ndic } from "#sch";
import { dateTime } from "@golemio/core/dist/helpers";
import { RsdTmcOsmMappingRepository } from "@golemio/traffic-common/dist/output-gateway";

export class OutputTrafficRestrictionsTransformation {
    name: string;
    protected rsdTmcOsmMappingRepository: RsdTmcOsmMappingRepository;

    constructor(rsdTmcOsmMappingRepository: RsdTmcOsmMappingRepository) {
        this.name = Ndic.traffic_restrictions.name;
        this.rsdTmcOsmMappingRepository = rsdTmcOsmMappingRepository;
    }

    public transform = async (data: ITrafficRestrictionsModel[]) => {
        // Format needed: "publicationTime": "2022-03-31T14:12:11+02:00",
        const publicationTime: Date = data.reduce((acc, curr) => (acc.publication_time > curr.publication_time ? acc : curr))
            .publication_time as unknown as Date;

        const result: IOutputApiRestrictions = {
            modelBaseVersion: "2",
            situationPublicationLight: {
                lang: "cs",
                publicationTime: dateTime(publicationTime).setTimeZone("Europe/Prague").toISOString(),
                publicationCreator: {
                    country: "cz",
                    nationalIdentifier: "NDIC",
                },
                situation: await this.transformSituationsArr(data),
            },
        };

        return result;
    };

    private async transformSituationsArr(dataFromDbArr: ITrafficRestrictionsModel[]) {
        const resultSituationsArr: IOutputApiSituation[] = [];

        // group by situation_id
        const recorsdsBySituationIdObj = this.groupByOneProperty(dataFromDbArr, (i) => i.situation_id);

        // Select only records with the latest version
        // situation_record_version = situation_version
        for (const key in recorsdsBySituationIdObj) {
            let recordsArr = [];
            for (const record of recorsdsBySituationIdObj[key]) {
                if (!recordsArr.length) {
                    recordsArr.push(record);
                } else if (record.situation_record_version == recordsArr[0].situation_record_version) {
                    recordsArr.push(record);
                } else if (record.situation_record_version > recordsArr[0].situation_record_version) {
                    recordsArr = [];
                    recordsArr.push(record);
                }
            }
            const situationRecord = await this.transformRecordsArr(recordsArr);

            //  if situation has no records => do not show the situation
            if (!situationRecord.length) {
                continue;
            }
            resultSituationsArr.push({
                id: key,
                version: +recordsArr[0].situation_version,
                situationRecord,
            });
        }

        return resultSituationsArr;
    }

    private async transformRecordsArr(oneSituationArr: ITrafficRestrictionsModel[]) {
        const resultRecordsArr: IOutputApiSituationRecord[] = [];

        for (const record of oneSituationArr) {
            const ltStart: string =
                record.alert_c_linear?.alertCMethod2SecondaryPointLocation.alertCLocation.specificLocation || null;
            const ltEnd: string =
                record.alert_c_linear?.alertCMethod2PrimaryPointLocation.alertCLocation.specificLocation || null;

            const newLine: IOutputApiSituationRecord = {
                id: record.situation_record_id,
                situationRecordCreationTime: dateTime(record.situation_record_creation_time as unknown as Date)
                    .setTimeZone("Europe/Prague")
                    .toISOString(),
                situationRecordVersionTime: dateTime(record.situation_record_version_time as unknown as Date)
                    .setTimeZone("Europe/Prague")
                    .toISOString(),
                startTime: dateTime(record.validity_overall_start_time as unknown as Date)
                    .setTimeZone("Europe/Prague")
                    .toISOString(),
                endTime: dateTime(record.validity_overall_end_time as unknown as Date)
                    .setTimeZone("Europe/Prague")
                    .toISOString(),
                type: this.getOutputApiSituationRecordType(record.situation_record_type),
                version: parseNumber(record.situation_record_version) as number,
                generalPublicComment: this.generalPublicCommentParsing(record.general_public_comment),
                sourceName: this.sourceNameParsing(record.source),
                impact: {
                    delays: {
                        type: record.impact_delays_type,
                        timeValue: record.impact_delay_time_value,
                    },
                    numberOfLanesRestricted: record.impact_number_of_lanes_restricted,
                    numberOfOperationalLanes: record.impact_number_of_operational_lanes,
                    trafficConstrictionType: record.impact_traffic_constriction_type,
                    capacityRemaining: record.impact_capacity_remaining,
                },
                forVehiclesWithCharacteristicsOf: record.for_vehicles_with_characteristics_of || undefined,
            };
            if (record.alert_c_linear) {
                newLine.alertCLinear = {
                    type: record.alert_c_linear?.$["xsi:type"] || null,
                    alertCLocationCountryCode: record.alert_c_linear?.alertCLocationCountryCode || null,
                    alertCLocationTableNumber: record.alert_c_linear?.alertCLocationTableNumber || null,
                    alertCLocationTableVersion: record.alert_c_linear?.alertCLocationTableVersion || null,
                    alertCDirection: {
                        alertCDirectionCoded: record.alert_c_linear?.alertCDirection.alertCDirectionCoded.toUpperCase() || null,
                    },
                    alertCMethod2PrimaryPointLocation: {
                        alertCLocation: {
                            specificLocation: ltEnd,
                        },
                    },
                    alertCMethod2SecondaryPointLocation: {
                        alertCLocation: {
                            specificLocation: ltStart,
                        },
                    },
                };
                newLine.osmPath = record.osm_path;
            } else if (record.global_network_linear) {
                newLine.osmPath = record.osm_path;
            }

            resultRecordsArr.push(newLine);
        }
        return resultRecordsArr;
    }

    public getOutputApiSituationRecordType(recordType: string): OutputApiSituationRecordType | null {
        switch (recordType) {
            case "ConstructionWorks":
                return OutputApiSituationRecordType.ConstructionWorks;
            case "GeneralObstruction":
                return OutputApiSituationRecordType.GeneralObstruction;
            case "MaintenanceWorks":
                return OutputApiSituationRecordType.MaintenanceWorks;
            case "RoadOrCarriagewayOrLaneManagement":
                return OutputApiSituationRecordType.RoadOrCarriagewayOrLaneManagement;
            default:
                return null;
        }
    }

    private generalPublicCommentParsing(comment: string | undefined) {
        if (!comment) return null;
        if (comment[0] === "{") {
            try {
                const commentsStrArr = comment.split("|");
                const resultObj: any = {};
                for (const oneLanguageCommentStr of commentsStrArr) {
                    const commentObj = JSON.parse(oneLanguageCommentStr);
                    resultObj[commentObj.comment.values.value["$"].lang] = commentObj.comment.values.value["_"];
                }
                return resultObj;
            } catch (error) {
                return null;
            }
        }
        return {
            cs: comment,
        };
    }

    private sourceNameParsing(source: string | undefined) {
        if (!source) return null;
        if (source[0] === "{") {
            try {
                const sourceObj = JSON.parse(source);
                return sourceObj.sourceIdentification;
            } catch (error) {
                return null;
            }
        }
        return source;
    }

    private groupByOneProperty = <T, K extends keyof any>(list: T[], getKey: (item: T) => K) =>
        list.reduce((previous, currentItem) => {
            const group = getKey(currentItem);
            if (!previous[group]) previous[group] = [];
            previous[group].push(currentItem);
            return previous;
        }, {} as Record<K, T[]>);

    private async getRsdTmcOsmMapping(ltStart: number, ltEnd: number): Promise<number[] | null> {
        return await this.rsdTmcOsmMappingRepository.GetOne({ ltStart, ltEnd });
    }
}
