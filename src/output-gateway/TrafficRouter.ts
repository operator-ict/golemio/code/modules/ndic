import { NdicContainer } from "#og/ioc/Di";
import { OutputTrafficRestrictionsTransformation } from "#og/transformations/OutputTrafficRestrictionsTransformation";
import { CacheHeaderMiddleware } from "@golemio/core/dist/output-gateway/CacheHeaderMiddleware";
import { checkErrors, pagination, paginationLimitMiddleware } from "@golemio/core/dist/output-gateway/Validation";
import { ContainerToken } from "@golemio/core/dist/output-gateway/ioc";
import { BaseRouter } from "@golemio/core/dist/output-gateway/routes/BaseRouter";
import { NextFunction, Request, Response, Router } from "@golemio/core/dist/shared/express";
import { query } from "@golemio/core/dist/shared/express-validator";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { RsdTmcOsmMappingRepository } from "@golemio/traffic-common/dist/output-gateway";
import { TrafficRestrictionsModel } from "./models";

export class TrafficRouter extends BaseRouter {
    protected cacheHeaderMiddleware: CacheHeaderMiddleware;

    public constructor(
        protected trafficRestrictionsModel: TrafficRestrictionsModel,
        private outputTrafficRestrictionsTransformation: OutputTrafficRestrictionsTransformation
    ) {
        super();

        this.cacheHeaderMiddleware = NdicContainer.resolve<CacheHeaderMiddleware>(ContainerToken.CacheHeaderMiddleware);

        this.router.get(
            "/restrictions",
            [
                query("moment").optional().isISO8601().not().isArray(),
                query("situationRecordType").optional().not().isEmpty({ ignore_whitespace: true }).not().isArray(),
            ],
            pagination,
            checkErrors,
            paginationLimitMiddleware("TrafficRouter"),
            this.cacheHeaderMiddleware.getMiddleware(3 * 60, 60),
            this.GetTrafficRestrictions
        );
    }

    public GetTrafficRestrictions = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const dataFromDb = await this.trafficRestrictionsModel.GetAll({
                limit: req.query.limit ? +req.query.limit : undefined,
                offset: req.query.offset ? +req.query.offset : undefined,
                reqMoment: req.query.moment as string,
                situationRecordType: req.query.situationRecordType as string,
            });
            if (!dataFromDb?.length) {
                throw new GeneralError("No data found", "TrafficRouter", undefined, 404);
            }
            const transformedData = await this.outputTrafficRestrictionsTransformation.transform(dataFromDb);
            res.status(200).send(transformedData);
        } catch (err) {
            next(err);
        }
    };
}

const trafficRouter: Router = new TrafficRouter(
    new TrafficRestrictionsModel(),
    new OutputTrafficRestrictionsTransformation(new RsdTmcOsmMappingRepository())
).router;

export { trafficRouter };
