import { ITrafficRestrictionsModel } from "#ie/transformations/TrafficRestrictionsInterface";
import { Ndic } from "#sch/index";
import { sequelizeConnection } from "@golemio/core/dist/output-gateway/database";
import { SequelizeModel } from "@golemio/core/dist/output-gateway/models";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import { dateTime } from "@golemio/core/dist/helpers";

export class TrafficRestrictionsModel extends SequelizeModel {
    public constructor() {
        super(Ndic.traffic_restrictions.name, "v_traffic_restrictions_all", Ndic.traffic_restrictions.outputSequelizeAttributes, {
            schema: Ndic.pgSchema,
        });
    }

    /**
     * @param {object} [options] Options object with params
     * @param {number} [options.limit] Limit
     * @param {number} [options.offset] Offset
     * @param {string} [options.reqMoment] ISO date
     * @param {string} [options.situationRecordType] Situation Record Type
     * @returns Array of the retrieved records
     */

    public GetAll = async (
        options: {
            limit?: number;
            offset?: number;
            reqMoment?: string;
            situationRecordType?: string;
        } = {}
    ): Promise<ITrafficRestrictionsModel[]> => {
        const { limit, offset, reqMoment, situationRecordType } = options;

        try {
            // take moment from request or now
            let queryMoment = reqMoment ? reqMoment : dateTime(new Date()).setTimeZone("Europe/Prague").toISOString();

            let query = `
            SELECT *
            FROM ${Ndic.pgSchema}.fetch_valid_traffic_restrictions(:queryMoment)
            `;

            if (situationRecordType) {
                query += "WHERE situation_record_type = :situationRecordType";
            }

            if (limit) {
                query += ` LIMIT ${limit}`;
            }

            if (offset) {
                query += ` OFFSET ${offset}`;
            }

            const trafficRestrictionsFromDB = (await sequelizeConnection?.query(query, {
                replacements: { queryMoment, situationRecordType },
            })) as ITrafficRestrictionsModel[][];

            return trafficRestrictionsFromDB[0] || [];
        } catch (err) {
            throw new GeneralError("Database error", "TrafficRestrictionsModel", err, 500);
        }
    };

    public GetOne = async (id: number): Promise<object | null> => {
        throw new Error("Not implemented");
    };
}
