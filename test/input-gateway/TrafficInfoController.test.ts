import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import sinon, { SinonSandbox, SinonSpy } from "sinon";
import { TrafficInfoController } from "#ig/TrafficInfoController";
import { sourceDataTrafficInfo } from "../data/traffic_info_07_00_12.989";
import { config } from "@golemio/core/dist/input-gateway";
import { Ndic } from "#sch";

chai.use(chaiAsPromised);

describe("TrafficInfoController", () => {
    let sandbox: SinonSandbox;
    let controller: TrafficInfoController;
    let queuePrefix: string;

    beforeEach(() => {
        sandbox = sinon.createSandbox();
        queuePrefix = config.rabbit_exchange_name + "." + Ndic.name.toLowerCase();

        controller = new TrafficInfoController();

        sandbox.stub(controller["validator"], "Validate").callsFake(() => Promise.resolve(true));
        sandbox.stub(controller, "sendMessageToExchange" as any);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should have name", () => {
        expect(controller.name).not.to.be.undefined;
    });

    it("should have processData method", () => {
        expect(controller.processData).not.to.be.undefined;
    });

    it("should properly process data", async () => {
        await controller.processData(sourceDataTrafficInfo);
        sandbox.assert.calledOnce(controller["validator"].Validate as SinonSpy);
        sandbox.assert.calledOnce(controller["sendMessageToExchange"] as SinonSpy);
        sandbox.assert.calledWith(
            controller["sendMessageToExchange"] as SinonSpy,
            "input." + queuePrefix + ".saveTrafficInfo",
            JSON.stringify(sourceDataTrafficInfo.d2LogicalModel),
            { persistent: true }
        );
    });
});
