import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import request from "supertest";
import express, { Request, Response, NextFunction } from "@golemio/core/dist/shared/express";
import { HTTPErrorHandler } from "@golemio/core/dist/shared/golemio-errors";
import { AMQPConnector } from "@golemio/core/dist/input-gateway/connectors";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { ndicRouter } from "#ig/index";
import * as fs from "fs";

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);

chai.use(chaiAsPromised);

describe("NdicRouter", () => {
    const app = express();
    app.use(
        bodyParser.xml({
            limit: "2MB",
            xmlParseOptions: {
                explicitArray: false,
                normalize: true,
                normalizeTags: false, // Transform tags to lowercase
            },
        })
    );

    const trafficInfoInput = fs.readFileSync(__dirname + "/../data/traffic_info_data.xml", { encoding: "utf-8" });
    const trafficRestrictionsInput = fs.readFileSync(__dirname + "/../data/traffic_restrictions_data.xml", { encoding: "utf-8" });

    before(async () => {
        await AMQPConnector.connect();
        app.use("/ndic", ndicRouter);
        app.use((err: any, _req: Request, res: Response, _next: NextFunction) => {
            const error = HTTPErrorHandler.handle(err, log);
            if (error) {
                res.setHeader("Content-Type", "application/json; charset=utf-8");
                res.status(error.error_status || 500).send(error);
            }
        });
    });

    describe("Traffic Info", () => {
        it("should respond with 204 to POST /ndic/traffic-info (text/xml)", (done) => {
            request(app)
                .post("/ndic/traffic-info")
                .send(trafficInfoInput)
                .set("Content-Type", "text/xml; charset=utf-8")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 204 to POST /ndic/traffic-info (application/xml)", (done) => {
            request(app)
                .post("/ndic/traffic-info")
                .send(trafficInfoInput)
                .set("Content-Type", "application/xml; charset=utf-8")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /ndic/traffic-info with bad content type", (done) => {
            request(app)
                .post("/ndic/traffic-info")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /ndic/traffic-info with no content type", (done) => {
            request(app)
                .post("/ndic/traffic-info")
                .send(trafficInfoInput)
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /ndic/traffic-info with invalid data", (done) => {
            request(app)
                .post("/ndic/traffic-info")
                .send("<invalid-data></invalid-data>")
                .set("Content-Type", "text/xml; charset=utf-8")
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });

    describe("Traffic Restrictions", () => {
        it("should respond with 204 to POST /ndic/traffic-restrictions (text/xml)", (done) => {
            request(app)
                .post("/ndic/traffic-restrictions")
                .send(trafficRestrictionsInput)
                .set("Content-Type", "text/xml; charset=utf-8")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 204 to POST /ndic/traffic-restrictions (application/xml)", (done) => {
            request(app)
                .post("/ndic/traffic-restrictions")
                .send(trafficRestrictionsInput)
                .set("Content-Type", "application/xml; charset=utf-8")
                .expect(204)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /ndic/traffic-restrictions with bad content type", (done) => {
            request(app)
                .post("/ndic/traffic-restrictions")
                .send("value=0") // x-www-form-urlencoded upload
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 406 to POST /ndic/traffic-restrictions with no content type", (done) => {
            request(app)
                .post("/ndic/traffic-restrictions")
                .send(trafficRestrictionsInput)
                .unset("Content-Type")
                .expect(406)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });

        it("should respond with 422 to POST /ndic/traffic-restrictions with invalid data", (done) => {
            request(app)
                .post("/ndic/traffic-restrictions")
                .send("<invalid-data></invalid-data>")
                .set("Content-Type", "text/xml; charset=utf-8")
                .expect(422)
                .end((err, res) => {
                    if (err) {
                        return done(err);
                    }
                    done();
                });
        });
    });
});
