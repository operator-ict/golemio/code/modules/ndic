import { LegacyNdicWorker } from "#ie";
import { OsmPathService } from "#ie/helpers/OsmPathService";
import { ModuleContainerToken } from "#ie/ioc/ContainerToken";
import { NdicContainer } from "#ie/ioc/Di";
import { RsdTmcPointsModel } from "#ie/models/RsdTmcPointsModel";
import { GlobalNetworkMappingRepository } from "#ie/repository/GlobalNetworkMappingRepository";
import { GlobalNetworkOsmMappingModel } from "#sch/schemas/models/GlobalNetworkOsmMapping";
import { PostgresConnector } from "@golemio/core/dist/integration-engine";
import { SinonSandbox, SinonSpy, createSandbox } from "sinon";
import { sourceDataTrafficInfo } from "../data/traffic_info_07_00_12.989";
import { transformedDataTrafficInfo } from "../data/traffic_info_07_00_12.989_transformed";
import { sourceDataTrafficRestrictions } from "../data/traffic_restrictions_05_59_51.629";
import { transformedDataTrafficRestrictions } from "../data/traffic_restrictions_05_59_51.629_transformed";

describe("LegacyNdicWorker", () => {
    let sandbox: SinonSandbox;
    let worker: LegacyNdicWorker;

    beforeEach(() => {
        sandbox = createSandbox();

        sandbox.stub(PostgresConnector, "getConnection").callsFake(() =>
            Object.assign({
                define: sandbox.stub().callsFake(() => ({})),
                transaction: sandbox.stub().callsFake(() => Object.assign({ commit: sandbox.stub() })),
            })
        );
        NdicContainer.registerInstance(
            ModuleContainerToken.OsmPathService,
            Object.assign(
                {
                    addOsmPath: sandbox.stub().callsFake(() => Promise.resolve([])),
                },
                OsmPathService
            )
        );
        const rsdTmcPointsRepository = NdicContainer.resolve<RsdTmcPointsModel>(ModuleContainerToken.RsdTmcPointsRepository);
        sandbox.stub(rsdTmcPointsRepository, "GetAll").callsFake(() => Promise.resolve([]));

        sandbox.stub(GlobalNetworkOsmMappingModel, "init");

        const globalNetworkMappingRepository = NdicContainer.resolve<GlobalNetworkMappingRepository>(
            ModuleContainerToken.GlobalNetworkMappingRepository
        );
        sandbox.stub(globalNetworkMappingRepository, "findAll").callsFake(() => Promise.resolve([]));

        worker = new LegacyNdicWorker();

        sandbox
            .stub(worker["ndicTrafficInfoTransformation"], "transform")
            .callsFake(() => Promise.resolve(transformedDataTrafficInfo));
        sandbox.stub(worker["ndicTrafficInfoModel"], "save");

        sandbox
            .stub(worker["ndicTrafficRestrictionsTransformation"], "transform")
            .callsFake(() => Promise.resolve(transformedDataTrafficRestrictions));
        sandbox.stub(worker["ndicTrafficRestrictionsModel"], "save");
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should call correct methods by saveTrafficInfo method", async () => {
        await worker.saveTrafficInfo({ content: JSON.stringify(sourceDataTrafficInfo.d2LogicalModel) });
        sandbox.assert.calledOnce(worker["ndicTrafficInfoTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["ndicTrafficInfoModel"].save as SinonSpy);
    });

    it("should call correct methods by saveTrafficRestrictions method", async () => {
        await worker.saveTrafficRestrictions({ content: JSON.stringify(sourceDataTrafficRestrictions.d2LogicalModel) });
        sandbox.assert.calledOnce(worker["ndicTrafficRestrictionsTransformation"].transform as SinonSpy);
        sandbox.assert.calledOnce(worker["ndicTrafficRestrictionsModel"].save as SinonSpy);
    });
});
