import { NdicContainer } from "#ie/ioc/Di";
import { GlobalNetworkMappingRepository } from "#ie/repository/GlobalNetworkMappingRepository";
import { GlobalNetworkOsmMappingModel } from "#sch/schemas/models/GlobalNetworkOsmMapping";
import { ILogger } from "@golemio/core/dist/helpers";
import { IDatabaseConnector } from "@golemio/core/dist/helpers/data-access/postgres/IDatabaseConnector";
import { CoreToken } from "@golemio/core/dist/helpers/ioc/CoreToken";
import { GeneralError } from "@golemio/core/dist/shared/golemio-errors";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { createSandbox, SinonSandbox } from "sinon";
import { Readable } from "stream";

chai.use(chaiAsPromised);

describe("GlobalNetworkMappingRepository", () => {
    let sandbox: SinonSandbox;
    let repository: GlobalNetworkMappingRepository;

    beforeEach(async () => {
        sandbox = createSandbox();
        const connection = NdicContainer.resolve<IDatabaseConnector>(CoreToken.PostgresConnector);
        await connection.connect();
        const logger = NdicContainer.resolve<ILogger>(CoreToken.Logger);
        repository = new GlobalNetworkMappingRepository(connection, logger);
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("write new data", async () => {
        const mockData: Readable = fs.createReadStream(__dirname + "/../../data/globalNetworkMockData.csv");
        await repository.streamData(mockData);
        const result = await GlobalNetworkOsmMappingModel.findAll();
        chai.expect(result).to.have.lengthOf(8);
    });

    it("test error handling", async () => {
        const mockData: Readable = fs.createReadStream(__dirname + "/../../data/globalNetworkMockData.csv");
        sandbox.stub(repository, <any>"streamDataToTmp").throws(new Error("Error"));
        await chai.expect(repository.streamData(mockData)).to.be.rejectedWith(GeneralError);
    });
});
