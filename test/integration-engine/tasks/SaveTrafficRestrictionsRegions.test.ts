import { SaveTrafficRestrictionsRegions } from "#ie/tasks/SaveTrafficRestrictionsRegions";
import { PostgresConnector } from "@golemio/core/dist/integration-engine/connectors";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import fs from "fs";
import { SinonSandbox, createSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("SaveTrafficRestrictionsRegions", () => {
    let sandbox: SinonSandbox;
    let task: SaveTrafficRestrictionsRegions;
    let sourceDataTrafficRestrictions: any;

    beforeEach(async () => {
        sandbox = createSandbox();
        PostgresConnector.connect();
        task = new SaveTrafficRestrictionsRegions("test");
        sourceDataTrafficRestrictions = JSON.parse(
            fs.readFileSync(__dirname + "/../../data/traffic_restrictions_regions_data.json").toString()
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("should have queueName", async () => {
        expect(task.queueName).not.to.be.undefined;
        expect(task.queueName).is.equal("saveTrafficRestrictionsRegions");
    });

    it("should save new data", async () => {
        const spy1 = sandbox.spy(task, "consume");
        expect(task.consume({ content: Buffer.from(JSON.stringify(sourceDataTrafficRestrictions), "utf-8") } as any)).to.be
            .fulfilled;
        expect(spy1.calledOnce).to.be.true;
    });
});
