import { OsmPathService } from "#ie/helpers/OsmPathService";
import chai from "chai";
import chaiAsPromised from "chai-as-promised";
import { createSandbox, SinonSandbox } from "sinon";

chai.use(chaiAsPromised);

describe("OsmPathService", () => {
    let sandbox: SinonSandbox;
    let service: OsmPathService;

    beforeEach(() => {
        sandbox = createSandbox();
        service = new OsmPathService(
            // @ts-ignore
            {
                findAll: sandbox
                    .stub()
                    .callsFake(() =>
                        Promise.resolve([
                            { osm_path: "373453827,9545664915,9545664914,373453664" },
                            { osm_path: "373453664,9545664914,9545664915,373453827" },
                        ])
                    ),
            },
            // @ts-ignore
            {
                GetOne: sandbox
                    .stub()
                    .callsFake(() => Promise.resolve([344552878, 1990976558, 1974994055, 9178395511, 344552863, 344552864])),
            },
            // @ts-ignore
            {
                warn: sandbox.stub(),
            }
        );
    });

    afterEach(() => {
        sandbox.restore();
    });

    it("addOsmPath succefully", async () => {
        const data = [
            {
                alert_c_linear: {
                    alertCMethod2SecondaryPointLocation: {
                        alertCLocation: {
                            specificLocation: "9545664915",
                        },
                    },
                    alertCMethod2PrimaryPointLocation: {
                        alertCLocation: {
                            specificLocation: "9545664914",
                        },
                    },
                },
            },
            {
                global_network_linear: {
                    linearWithinLinearGNElement: [
                        {
                            sectionId: "373453827",
                        },
                        {
                            sectionId: "373453664",
                        },
                    ],
                },
            },
        ];
        const result = await service.addOsmPath(data as any);
        chai.expect(result).to.eql([
            {
                alert_c_linear: {
                    alertCMethod2SecondaryPointLocation: {
                        alertCLocation: {
                            specificLocation: "9545664915",
                        },
                    },
                    alertCMethod2PrimaryPointLocation: {
                        alertCLocation: {
                            specificLocation: "9545664914",
                        },
                    },
                },
                osm_path: [[344552878, 1990976558, 1974994055, 9178395511, 344552863, 344552864]],
            },
            {
                global_network_linear: {
                    linearWithinLinearGNElement: [
                        {
                            sectionId: "373453827",
                        },
                        {
                            sectionId: "373453664",
                        },
                    ],
                },
                osm_path: [
                    [373453827, 9545664915, 9545664914, 373453664],
                    [373453664, 9545664914, 9545664915, 373453827],
                ],
            },
        ]);
    });

    it("addOsmPath with error", async () => {
        const data = [
            {
                alert_c_linear: {
                    alertCMethod2SecondaryPointLocation: {
                        alertCLocation: {
                            specificLocation: "9545664915",
                        },
                    },
                    alertCMethod2PrimaryPointLocation: {
                        alertCLocation: {
                            specificLocation: "9545664914",
                        },
                    },
                },
            },
            {
                global_network_linear: {
                    linearWithinLinearGNElement: [
                        {
                            sectionId: "373453827",
                        },
                        {
                            sectionId: "373453664",
                        },
                    ],
                },
            },
        ];
        sandbox.stub(service, <any>"getGlobalNetworkSectionIds").throws(new Error("Test error"));
        const result = await service.addOsmPath(data as any);
        chai.expect(result).to.eql([
            {
                alert_c_linear: {
                    alertCMethod2SecondaryPointLocation: {
                        alertCLocation: {
                            specificLocation: "9545664915",
                        },
                    },
                    alertCMethod2PrimaryPointLocation: {
                        alertCLocation: {
                            specificLocation: "9545664914",
                        },
                    },
                },
                osm_path: [[344552878, 1990976558, 1974994055, 9178395511, 344552863, 344552864]],
            },
            {
                global_network_linear: {
                    linearWithinLinearGNElement: [
                        {
                            sectionId: "373453827",
                        },
                        {
                            sectionId: "373453664",
                        },
                    ],
                },
            },
        ]);
    });
});
