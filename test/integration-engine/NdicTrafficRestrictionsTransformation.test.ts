import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { TrafficRestrictionsTransformation } from "#ie/transformations/TrafficRestrictionsTransformation";
import { sourceDataTrafficRestrictions } from "../data/traffic_restrictions_05_59_51.629";
import { transformedDataTrafficRestrictions } from "../data/traffic_restrictions_05_59_51.629_transformed";

chai.use(chaiAsPromised);

describe("TrafficRestrictionsTransformation", () => {
    let transformation: TrafficRestrictionsTransformation;

    beforeEach(async () => {
        transformation = new TrafficRestrictionsTransformation();
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("NdicTrafficRestrictions");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const msgParsed = sourceDataTrafficRestrictions.d2LogicalModel;
        const data = await transformation.transform(msgParsed);
        expect(JSON.parse(JSON.stringify(data))).to.eql(transformedDataTrafficRestrictions);
        // removes all undefined for chai deep check
    });
});
