import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { TrafficInfoTransformation } from "#ie/transformations/TrafficInfoTransformation";
import { sourceDataTrafficInfo } from "../data/traffic_info_07_00_12.989";
import { transformedDataTrafficInfo } from "../data/traffic_info_07_00_12.989_transformed";

chai.use(chaiAsPromised);

describe("TrafficInfoTransformation", () => {
    let transformation: TrafficInfoTransformation;

    beforeEach(async () => {
        transformation = new TrafficInfoTransformation();
    });

    it("should have name", async () => {
        expect(transformation.name).not.to.be.undefined;
        expect(transformation.name).is.equal("NdicTrafficInfo");
    });

    it("should have transform method", async () => {
        expect(transformation.transform).not.to.be.undefined;
    });

    it("should properly transform collection", async () => {
        const msgParsed = sourceDataTrafficInfo.d2LogicalModel;
        const data = await transformation.transform(msgParsed);
        expect(JSON.parse(JSON.stringify(data))).to.eql(transformedDataTrafficInfo); // removes all undefined for chai deep check
    });
});
