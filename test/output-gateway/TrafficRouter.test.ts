import sinon from "sinon";
import request from "supertest";
import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { HTTPErrorHandler, IGolemioError } from "@golemio/core/dist/shared/golemio-errors";
import express, { NextFunction, Request, Response } from "@golemio/core/dist/shared/express";
import { log } from "@golemio/core/dist/output-gateway/Logger";
import { TrafficRestrictionsModel, TrafficRouter } from "#og";
import { routerRespData } from "../data/routerRespData";
import { OutputTrafficRestrictionsTransformation } from "#og/transformations/OutputTrafficRestrictionsTransformation";
import { LocationConfigOptions, RsdTmcOsmMappingRepository } from "@golemio/traffic-common/dist/output-gateway";

chai.use(chaiAsPromised);

describe("trafficRouter Router", () => {
    // Create clean express instance
    const app = express();
    // Basic configuration: create a sinon sandbox for testing
    let sandbox: any = null;

    const fakeRepository = {
        GetOne: async (options: LocationConfigOptions) => {
            if (options.ltStart === 25540 && options.ltEnd === 1279) {
                return [
                    151003135, 74279965, 74168337, 156250932, 156250937, 74153579, 976485311, 156250941, 156250944, 74244807,
                    74238396, 74177697, 1600950646, 156250948, 1600950600, 156250951, 1600950538, 74282499, 74084348, 191823018,
                    5506226185, 191823161, 191834765, 189021813, 189021814, 191834725, 189021808, 74177021, 189021825, 189021819,
                    262869174, 74269338, 191823171, 191823166, 74256918,
                ];
            }
            return [156250932, 156250937, 74153579, 976485311];
        },
    } as RsdTmcOsmMappingRepository;

    const router = new TrafficRouter(new TrafficRestrictionsModel(), new OutputTrafficRestrictionsTransformation(fakeRepository));

    beforeEach(() => {
        sandbox = sinon.createSandbox();
    });

    afterEach(() => {
        sandbox && sandbox.restore();
    });

    before(() => {
        // Mount the tested router to the express instance
        app.use("/traffic", router.router);
        app.use((err: any, req: Request, res: Response, next: NextFunction) => {
            const errObject: IGolemioError = HTTPErrorHandler.handle(err, log);
            log.silly("Error caught by the router error handler.");
            res.setHeader("Content-Type", "application/json; charset=utf-8");
            res.status(errObject.error_status || 500).send(errObject);
        });
    });

    it("should respond correctly to GET /traffic/restrictions", async () => {
        const response = await request(app)
            .get("/traffic/restrictions?moment=2022-04-26 18:14")
            .set("Accept", "application/json");
        expect(response.status).to.equal(200);
        expect(response.header["content-type"]).to.equal("application/json; charset=utf-8");
        expect(response.header["cache-control"]).to.equal("public, s-maxage=180, stale-while-revalidate=60");
        expect(response.body).to.eql(routerRespData);
    });
});
