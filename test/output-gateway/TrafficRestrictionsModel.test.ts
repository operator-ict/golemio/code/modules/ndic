import chai, { expect } from "chai";
import chaiAsPromised from "chai-as-promised";
import { TrafficRestrictionsModel } from "#og/models/TrafficRestrictionsModel";

chai.use(chaiAsPromised);

describe("TrafficRestrictionsModel", () => {
    const trafficRestrictionsModel: TrafficRestrictionsModel = new TrafficRestrictionsModel();

    it("should instantiate", () => {
        expect(trafficRestrictionsModel).not.to.be.undefined;
    });

    describe("GetAll", async () => {
        it("should return correct subset of items", async () => {
            const result = await trafficRestrictionsModel.GetAll({
                reqMoment: "2021-06-21 10:00:00.000",
            });
            expect(result).to.be.an.instanceOf(Array);
            expect(result.length).to.eql(3);
            expect(result[0].situation_id).to.eql("d5321261-0cf1-47ae-a1d6-39cb8aab3874");
            expect(result[1].situation_id).to.eql("d5321261-0cf1-47ae-a1d6-39cb8aab3874");
            expect(result[2].situation_id).to.eql("dc4e500c-6853-4232-a439-26889485f0bf");
        });
    });
});
