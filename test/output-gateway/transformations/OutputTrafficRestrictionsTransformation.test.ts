import { OutputTrafficRestrictionsTransformation } from "#og/transformations/OutputTrafficRestrictionsTransformation";
import { OutputApiSituationRecordType } from "#ie/transformations/TrafficRestrictionsInterface";
import { expect } from "chai";
import { RsdTmcOsmMappingRepository } from "@golemio/traffic-common/dist/output-gateway";

describe("OutputTrafficRestrictionsTransformation", () => {
    const transformation = new OutputTrafficRestrictionsTransformation(new RsdTmcOsmMappingRepository());

    it("Transforms OutputApiSituationRecordType correctly", () => {
        const expectations = {
            ConstructionWorks: OutputApiSituationRecordType.ConstructionWorks,
            GeneralObstruction: OutputApiSituationRecordType.GeneralObstruction,
            MaintenanceWorks: OutputApiSituationRecordType.MaintenanceWorks,
            RoadOrCarriagewayOrLaneManagement: OutputApiSituationRecordType.RoadOrCarriagewayOrLaneManagement,
        };

        for (let [key, expectedValue] of Object.entries(expectations)) {
            expect(transformation.getOutputApiSituationRecordType(key)).to.eql(expectedValue);
        }
    });
});
