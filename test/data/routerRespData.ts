/* eslint-disable max-len */

import { IOutputApiRestrictions, OutputApiSituationRecordType } from "#ie/transformations/TrafficRestrictionsInterface";

export const routerRespData: IOutputApiRestrictions = {
    modelBaseVersion: "2",
    situationPublicationLight: {
        lang: "cs",
        publicationTime: "2022-04-04T12:42:08+02:00",
        publicationCreator: {
            country: "cz",
            nationalIdentifier: "NDIC",
        },
        situation: [
            {
                id: "35089e12-9884-4cf8-9d71-1371b3fd8ec4",
                version: 5,
                situationRecord: [
                    {
                        id: "35089e12-9884-4cf8-9d71-1371b3fd8ec4_RoadOrCarriagewayOrLaneManagement",
                        situationRecordCreationTime: "2022-04-04T12:41:45+02:00",
                        situationRecordVersionTime: "2022-04-04T12:41:44+02:00",
                        startTime: "2022-03-14T01:00:00+01:00",
                        endTime: "2022-11-01T00:59:00+01:00",
                        type: OutputApiSituationRecordType.RoadOrCarriagewayOrLaneManagement,
                        version: 5,
                        generalPublicComment: {
                            cs: "silnice II/125, Kolín, okr. Kolín, uzavřeno pro těžká nákladní vozidla, stavební práce, práce na údržbě mostu, Od 14.03.2022 00:00 Do 31.10.2022 23:59, Uzavírka silnice č. II/125 v Kolíně – mostu ev. č.125-034 (Nový most) pro nákladní automobily nad 3,5 t a traktory. Od 1.5. bude sjezd na silnici III/3275 do ul. Starokolínská z Nového mostu umožněn vozidlům mimo nákladní automobily nad 3,5t a traktory ve směru od centra Kolína a výjezd ze silnice III/3275 z ul. Starokolínská na Nový most bude povolen vozidlům mimo nákladní automobily nad 3,5t a traktory, doprava ve směru na Zálabí., Objížďka - pro vozidla nad 3,5 tun: ulice Ovčárecká, v Kolíně po silnici II/125 a III/12551, dál po silnici I/38 přes Novou Ves I., Pňov – Předhradí a Oseček na dálničním sjezdu EXIT 39 na dálnici D11, po dálnici D11 na EXIT 42 a po silnici II/125 přes Velký Osek zpět do Kolna , Objížďka - pro vozidla nad 3,5 tun: objížďková trasa pro přístav/ul. Starokolínská v Kolíně: - po silnici II/125 a III/12551, dál po silnici I/38 směr Kut|ná Horana křižovatku se silnicí III/3277, na křižovatce se silnicí III/3277 vlevo směr Starý Kolín, před Starým Kolínem opět vlevo po silnici III/3275 zpět do Kolína , Vydal: Městský úřad Kolín",
                        },
                        sourceName: "RSD",
                        impact: {
                            delays: {
                                type: null,
                                timeValue: null,
                            },
                            numberOfLanesRestricted: null,
                            numberOfOperationalLanes: null,
                            trafficConstrictionType: null,
                            capacityRemaining: 0,
                        },
                        alertCLinear: {
                            type: "AlertCMethod2Linear",
                            alertCLocationCountryCode: "CZ",
                            alertCLocationTableNumber: "25",
                            alertCLocationTableVersion: "9.0",
                            alertCDirection: {
                                alertCDirectionCoded: "POSITIVE",
                            },
                            alertCMethod2PrimaryPointLocation: {
                                alertCLocation: {
                                    specificLocation: "16123",
                                },
                            },
                            alertCMethod2SecondaryPointLocation: {
                                alertCLocation: {
                                    specificLocation: "16121",
                                },
                            },
                        },
                        forVehiclesWithCharacteristicsOf: {
                            vehicleType: "lorry",
                        },
                        osmPath: [[156250932, 156250937, 74153579, 976485311]],
                    },
                ],
            },
            {
                id: "d5321261-0cf1-47ae-a1d6-39cb8aab3874",
                version: 6,
                situationRecord: [
                    {
                        id: "d5321261-0cf1-47ae-a1d6-39cb8aab3874_RoadOrCarriagewayOrLaneManagement",
                        situationRecordCreationTime: "2021-06-21T11:53:27+02:00",
                        situationRecordVersionTime: "2021-06-21T11:52:49+02:00",
                        startTime: "2021-06-21T09:15:00+02:00",
                        endTime: "2024-06-21T12:00:00+02:00",
                        type: OutputApiSituationRecordType.RoadOrCarriagewayOrLaneManagement,
                        version: 6,
                        generalPublicComment: {
                            cs: "D1 ve směru Praha, mezi 52 a 49 km, v termínu od 21. 06. 2021 07:15 do 21. 06. 2021 10:00, sekání trávy a údržba travních porostů, rozsah: zpevněná krajnice (1),přídatný pruh (2) - pro pomalá vozidla, připojovací, odbočovací, počet průjezdných pruhů: 2",
                        },
                        sourceName: "SSU",
                        impact: {
                            delays: {
                                type: null,
                                timeValue: null,
                            },
                            numberOfLanesRestricted: null,
                            numberOfOperationalLanes: 2,
                            trafficConstrictionType: null,
                            capacityRemaining: null,
                        },
                        alertCLinear: {
                            type: "AlertCMethod2Linear",
                            alertCLocationCountryCode: "CZ",
                            alertCLocationTableNumber: "25",
                            alertCLocationTableVersion: "8.0",
                            alertCDirection: {
                                alertCDirectionCoded: "NEGATIVE",
                            },
                            alertCMethod2PrimaryPointLocation: {
                                alertCLocation: {
                                    specificLocation: "1279",
                                },
                            },
                            alertCMethod2SecondaryPointLocation: {
                                alertCLocation: {
                                    specificLocation: "25540",
                                },
                            },
                        },
                        osmPath: [
                            [
                                151003135, 74279965, 74168337, 156250932, 156250937, 74153579, 976485311, 156250941, 156250944,
                                74244807, 74238396, 74177697, 1600950646, 156250948, 1600950600, 156250951, 1600950538, 74282499,
                                74084348, 191823018, 5506226185, 191823161, 191834765, 189021813, 189021814, 191834725, 189021808,
                                74177021, 189021825, 189021819, 262869174, 74269338, 191823171, 191823166, 74256918,
                            ],
                        ],
                    },
                ],
            },
            {
                id: "dc4e500c-6853-4232-a439-26889485f0bf",
                version: 10,
                situationRecord: [
                    {
                        id: "dc4e500c-6853-4232-a439-26889485f0bf_RoadOrCarriagewayOrLaneManagement",
                        situationRecordCreationTime: "2021-06-21T11:27:36+02:00",
                        situationRecordVersionTime: "2021-06-21T11:26:35+02:00",
                        startTime: "2021-06-21T09:45:00+02:00",
                        endTime: "2024-06-21T11:35:00+02:00",
                        type: OutputApiSituationRecordType.RoadOrCarriagewayOrLaneManagement,
                        version: 10,
                        generalPublicComment: {
                            cs: "D11 ve směru Hradec Králové, mezi 57.8 a 59.5 km, v termínu od 21. 06. 2021 07:45 do 21. 06. 2021 09:35, sekání trávy a údržba travních porostů, rozsah: zpevněná krajnice (1), počet průjezdných pruhů: 2",
                        },
                        sourceName: "SSU",
                        impact: {
                            delays: {
                                type: null,
                                timeValue: null,
                            },
                            numberOfLanesRestricted: 1,
                            numberOfOperationalLanes: null,
                            trafficConstrictionType: null,
                            capacityRemaining: null,
                        },
                        alertCLinear: {
                            type: "AlertCMethod2Linear",
                            alertCLocationCountryCode: "CZ",
                            alertCLocationTableNumber: "25",
                            alertCLocationTableVersion: "8.0",
                            alertCDirection: {
                                alertCDirectionCoded: "POSITIVE",
                            },
                            alertCMethod2PrimaryPointLocation: {
                                alertCLocation: {
                                    specificLocation: "25631",
                                },
                            },
                            alertCMethod2SecondaryPointLocation: {
                                alertCLocation: {
                                    specificLocation: "25632",
                                },
                            },
                        },
                        osmPath: [[156250932, 156250937, 74153579, 976485311]],
                    },
                ],
            },
        ],
    },
};
