import { ITrafficInfo } from "#ie/transformations/TrafficInfoInterface";

export const sourceDataTrafficInfo: { d2LogicalModel: ITrafficInfo } = {
    d2LogicalModel: {
        $: {
            modelBaseVersion: "2",
            xmlns: "http://datex2.eu/schema/2/2_0",
            "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        },
        exchange: {
            supplierIdentification: {
                country: "cz",
                nationalIdentifier: "DIC",
            },
        },
        payloadPublication: {
            $: {
                "xsi:type": "SituationPublication",
                lang: "cs",
            },
            publicationTime: "2021-05-19T07:00:12+02:00",
            publicationCreator: {
                country: "cz",
                nationalIdentifier: "NDIC",
            },
            situation: [
                {
                    $: {
                        id: "6bb8d753-0bb2-4b72-84d5-9c54070f4682",
                        version: "16",
                    },
                    situationVersionTime: "2021-05-19T06:59:16+02:00",
                    headerInformation: {
                        confidentiality: "noRestriction",
                        informationStatus: "real",
                        urgency: "normalUrgency",
                    },
                    situationRecord: {
                        $: {
                            version: "16",
                            "xsi:type": "AbnormalTraffic",
                            id: "6bb8d753-0bb2-4b72-84d5-9c54070f4682_AbnormalTraffic",
                        },
                        situationRecordCreationTime: "2021-05-19T06:59:16+02:00",
                        situationRecordVersionTime: "2021-05-19T06:59:16+02:00",
                        probabilityOfOccurrence: "riskOf",
                        source: {
                            sourceIdentification: "FCD",
                            sourceName: {
                                values: {
                                    value: {
                                        _: "Data FCD",
                                        $: {
                                            lang: "cs",
                                        },
                                    },
                                },
                            },
                        },
                        validity: {
                            validityStatus: "definedByValidityTimeSpec",
                            validityTimeSpecification: {
                                overallStartTime: "2021-05-19T06:25:15+02:00",
                                overallEndTime: "2021-05-19T07:29:15+02:00",
                            },
                        },
                        generalPublicComment: {
                            comment: {
                                values: {
                                    value: {
                                        _: "D0, km 60.6 až 62.7, ve směru D1, silný provoz 2 km",
                                        $: {
                                            lang: "cs",
                                        },
                                    },
                                },
                            },
                        },
                        groupOfLocations: {
                            $: {
                                "xsi:type": "Linear",
                            },
                            alertCLinear: {
                                $: {
                                    "xsi:type": "AlertCMethod2Linear",
                                },
                                alertCLocationCountryCode: "CZ",
                                alertCLocationTableNumber: "25",
                                alertCLocationTableVersion: "8.0",
                                alertCDirection: {
                                    alertCDirectionCoded: "negative",
                                },
                                alertCMethod2PrimaryPointLocation: {
                                    alertCLocation: {
                                        specificLocation: "1245",
                                    },
                                },
                                alertCMethod2SecondaryPointLocation: {
                                    alertCLocation: {
                                        specificLocation: "22988",
                                    },
                                },
                            },
                            linearWithinLinearElement: {
                                directionRelativeOnLinearSection: "aligned",
                                linearElement: {
                                    roadNumber: "D0",
                                },
                                fromPoint: {
                                    $: {
                                        "xsi:type": "DistanceFromLinearElementStart",
                                    },
                                    distanceAlong: "60604",
                                },
                                toPoint: {
                                    $: {
                                        "xsi:type": "DistanceFromLinearElementStart",
                                    },
                                    distanceAlong: "62597",
                                },
                            },
                            globalNetworkLinear: {
                                networkVersion: {
                                    values: {
                                        value: {
                                            _: "20.12",
                                            $: {
                                                lang: "cs",
                                            },
                                        },
                                    },
                                },
                                linearGeometryType: "continuous",
                                startPoint: {
                                    sjtskPointCoordinates: {
                                        sjtskX: "-730481.044",
                                        sjtskY: "-1043548.3035",
                                    },
                                },
                                endPoint: {
                                    sjtskPointCoordinates: {
                                        sjtskX: "-730836.354800001",
                                        sjtskY: "-1045600.896",
                                    },
                                },
                                linearWithinLinearGNElement: [
                                    {
                                        sectionId: "1397507",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "1",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "1397508",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "2",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "1278222",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "3",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "1278225",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "4",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "1278226",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "5",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3529694",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "6",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3514032",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "7",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3717394",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "8",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3717395",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "9",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3665922",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "10",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3318241",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "11",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3293721",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "12",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3529688",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "13",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3514030",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "14",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "730587",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "15",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "284816",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "16",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "1397503",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "17",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3665380",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "18",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "3728019",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "19",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                    {
                                        sectionId: "1397493",
                                        directionRelativeOnLinearSection: "aligned",
                                        orderOfSection: "20",
                                        fromPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "0",
                                        },
                                        toPoint: {
                                            $: {
                                                "xsi:type": "PercentageDistanceAlongLinearElement",
                                            },
                                            percentageDistanceAlong: "1",
                                        },
                                    },
                                ],
                            },
                            linearExtension: {
                                openlrExtendedLinear: {
                                    firstDirection: {
                                        openlrLocationReferencePoint: {
                                            openlrCoordinate: {
                                                latitude: "50.104842985834864",
                                                longitude: "14.589358730743161",
                                            },
                                            openlrLineAttributes: {
                                                openlrFunctionalRoadClass: "FRC0",
                                                openlrFormOfWay: "motorway",
                                                openlrBearing: "163",
                                            },
                                            openlrPathAttributes: {
                                                openlrLowestFRCToNextLRPoint: "FRC1",
                                                openlrDistanceToNextLRPoint: "2937",
                                            },
                                        },
                                        openlrLastLocationReferencePoint: {
                                            openlrCoordinate: {
                                                latitude: "50.078963891798992",
                                                longitude: "14.591913092765031",
                                            },
                                            openlrLineAttributes: {
                                                openlrFunctionalRoadClass: "FRC1",
                                                openlrFormOfWay: "multipleCarriageway",
                                                openlrBearing: "18",
                                            },
                                        },
                                        openlrOffsets: {
                                            openlrPositiveOffset: "831",
                                            openlrNegativeOffset: "0",
                                        },
                                    },
                                },
                            },
                        },
                        abnormalTrafficType: "heavyTraffic",
                    },
                },
                {
                    $: {
                        id: "8d28ec4b-4f5d-4dd3-b822-d35eada14a48",
                        version: "5",
                    },
                    situationVersionTime: "2021-05-19T07:00:03+02:00",
                    headerInformation: {
                        confidentiality: "noRestriction",
                        informationStatus: "real",
                        urgency: "normalUrgency",
                    },
                    situationRecord: [
                        {
                            $: {
                                version: "5",
                                "xsi:type": "MaintenanceWorks",
                                id: "8d28ec4b-4f5d-4dd3-b822-d35eada14a48_MaintenanceWorks",
                            },
                            situationRecordCreationTime: "2021-05-19T07:00:07+02:00",
                            situationRecordVersionTime: "2021-05-19T07:00:03+02:00",
                            probabilityOfOccurrence: "certain",
                            source: {
                                sourceIdentification: "SSU",
                                sourceName: {
                                    values: {
                                        value: {
                                            $: {
                                                lang: "cs",
                                            },
                                        },
                                    },
                                },
                            },
                            validity: {
                                validityStatus: "definedByValidityTimeSpec",
                                validityTimeSpecification: {
                                    overallStartTime: "2021-05-19T07:00:00+02:00",
                                    overallEndTime: "2021-05-19T18:00:00+02:00",
                                },
                            },
                            impact: {
                                numberOfOperationalLanes: "2",
                                originalNumberOfLanes: "2",
                            },
                            generalPublicComment: {
                                comment: {
                                    values: {
                                        value: {
                                            _:
                                                "D1 ve směru Praha, mezi 58 a 57.5 km, v termínu od 19. 05. " +
                                                "2021 07:00 do 19. 05. 2021 18:00, údržba a opravy PHS, plotů a " +
                                                "závor, rozsah: zpevněná krajnice (1),přídatný pruh (2) - pro " +
                                                "pomalá vozidla, připojovací, odbočovací, počet průjezdných pruhů: 2",
                                            $: {
                                                lang: "cs",
                                            },
                                        },
                                    },
                                },
                            },
                            groupOfLocations: {
                                $: {
                                    "xsi:type": "Linear",
                                },
                                alertCLinear: {
                                    $: {
                                        "xsi:type": "AlertCMethod2Linear",
                                    },
                                    alertCLocationCountryCode: "CZ",
                                    alertCLocationTableNumber: "25",
                                    alertCLocationTableVersion: "8.0",
                                    alertCDirection: {
                                        alertCDirectionCoded: "negative",
                                    },
                                    alertCMethod2PrimaryPointLocation: {
                                        alertCLocation: {
                                            specificLocation: "1282",
                                        },
                                    },
                                    alertCMethod2SecondaryPointLocation: {
                                        alertCLocation: {
                                            specificLocation: "1280",
                                        },
                                    },
                                },
                                linearWithinLinearElement: {
                                    directionRelativeOnLinearSection: "opposite",
                                    linearElement: {
                                        roadNumber: "D1",
                                    },
                                    fromPoint: {
                                        $: {
                                            "xsi:type": "DistanceFromLinearElementStart",
                                        },
                                        distanceAlong: "58000",
                                    },
                                    toPoint: {
                                        $: {
                                            "xsi:type": "DistanceFromLinearElementStart",
                                        },
                                        distanceAlong: "57500",
                                    },
                                },
                                globalNetworkLinear: {
                                    networkVersion: {
                                        values: {
                                            value: {
                                                _: "20.12",
                                                $: {
                                                    lang: "cs",
                                                },
                                            },
                                        },
                                    },
                                    linearGeometryType: "continuous",
                                    startPoint: {
                                        sjtskPointCoordinates: {
                                            sjtskX: "-702540",
                                            sjtskY: "-1090436",
                                        },
                                    },
                                    endPoint: {
                                        sjtskPointCoordinates: {
                                            sjtskX: "-702972",
                                            sjtskY: "-1090188",
                                        },
                                    },
                                    linearWithinLinearGNElement: [
                                        {
                                            sectionId: "3531012",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "0",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0.51388888888873",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3668520",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "1",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3709905",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "2",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3709904",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "3",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3531013",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "4",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3531014",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "5",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3512436",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "6",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0.0721393034825324",
                                            },
                                        },
                                    ],
                                },
                                linearExtension: {
                                    openlrExtendedLinear: {
                                        firstDirection: {
                                            openlrLocationReferencePoint: {
                                                openlrCoordinate: {
                                                    latitude: "49.711782870746283",
                                                    longitude: "15.067501923548",
                                                },
                                                openlrLineAttributes: {
                                                    openlrFunctionalRoadClass: "FRC0",
                                                    openlrFormOfWay: "motorway",
                                                    openlrBearing: "290",
                                                },
                                                openlrPathAttributes: {
                                                    openlrLowestFRCToNextLRPoint: "FRC0",
                                                    openlrDistanceToNextLRPoint: "2051",
                                                },
                                            },
                                            openlrLastLocationReferencePoint: {
                                                openlrCoordinate: {
                                                    latitude: "49.724098226340338",
                                                    longitude: "15.048652253666898",
                                                },
                                                openlrLineAttributes: {
                                                    openlrFunctionalRoadClass: "FRC0",
                                                    openlrFormOfWay: "motorway",
                                                    openlrBearing: "158",
                                                },
                                            },
                                            openlrOffsets: {
                                                openlrPositiveOffset: "261",
                                                openlrNegativeOffset: "1295",
                                            },
                                        },
                                    },
                                },
                            },
                            situationRecordExtension: {
                                situationRecordExtendedApproved: {
                                    safetyRelatedMessage: "True",
                                },
                            },
                            roadMaintenanceType: "maintenanceWork",
                        },
                        {
                            $: {
                                version: "5",
                                "xsi:type": "RoadOrCarriagewayOrLaneManagement",
                                id: "8d28ec4b-4f5d-4dd3-b822-d35eada14a48_RoadOrCarriagewayOrLaneManagement",
                            },
                            situationRecordCreationTime: "2021-05-19T07:00:07+02:00",
                            situationRecordVersionTime: "2021-05-19T07:00:03+02:00",
                            probabilityOfOccurrence: "certain",
                            source: {
                                sourceIdentification: "SSU",
                                sourceName: {
                                    values: {
                                        value: {
                                            $: {
                                                lang: "cs",
                                            },
                                        },
                                    },
                                },
                            },
                            validity: {
                                validityStatus: "definedByValidityTimeSpec",
                                validityTimeSpecification: {
                                    overallStartTime: "2021-05-19T07:00:00+02:00",
                                    overallEndTime: "2021-05-19T18:00:00+02:00",
                                },
                            },
                            impact: {
                                numberOfOperationalLanes: "2",
                                originalNumberOfLanes: "2",
                            },
                            generalPublicComment: {
                                comment: {
                                    values: {
                                        value: {
                                            _:
                                                "D1 ve směru Praha, mezi 58 a 57.5 km, v termínu od 19. 05. " +
                                                "2021 07:00 do 19. 05. 2021 18:00, údržba a opravy PHS, plotů " +
                                                "a závor, rozsah: zpevněná krajnice (1),přídatný pruh (2) - pro " +
                                                "pomalá vozidla, připojovací, odbočovací, počet průjezdných pruhů: 2",
                                            $: {
                                                lang: "cs",
                                            },
                                        },
                                    },
                                },
                            },
                            groupOfLocations: {
                                $: {
                                    "xsi:type": "Linear",
                                },
                                alertCLinear: {
                                    $: {
                                        "xsi:type": "AlertCMethod2Linear",
                                    },
                                    alertCLocationCountryCode: "CZ",
                                    alertCLocationTableNumber: "25",
                                    alertCLocationTableVersion: "8.0",
                                    alertCDirection: {
                                        alertCDirectionCoded: "negative",
                                    },
                                    alertCMethod2PrimaryPointLocation: {
                                        alertCLocation: {
                                            specificLocation: "1282",
                                        },
                                    },
                                    alertCMethod2SecondaryPointLocation: {
                                        alertCLocation: {
                                            specificLocation: "1280",
                                        },
                                    },
                                },
                                linearWithinLinearElement: {
                                    directionRelativeOnLinearSection: "opposite",
                                    linearElement: {
                                        roadNumber: "D1",
                                    },
                                    fromPoint: {
                                        $: {
                                            "xsi:type": "DistanceFromLinearElementStart",
                                        },
                                        distanceAlong: "58000",
                                    },
                                    toPoint: {
                                        $: {
                                            "xsi:type": "DistanceFromLinearElementStart",
                                        },
                                        distanceAlong: "57500",
                                    },
                                },
                                globalNetworkLinear: {
                                    networkVersion: {
                                        values: {
                                            value: {
                                                _: "20.12",
                                                $: {
                                                    lang: "cs",
                                                },
                                            },
                                        },
                                    },
                                    linearGeometryType: "continuous",
                                    startPoint: {
                                        sjtskPointCoordinates: {
                                            sjtskX: "-702540",
                                            sjtskY: "-1090436",
                                        },
                                    },
                                    endPoint: {
                                        sjtskPointCoordinates: {
                                            sjtskX: "-702972",
                                            sjtskY: "-1090188",
                                        },
                                    },
                                    linearWithinLinearGNElement: [
                                        {
                                            sectionId: "3531012",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "0",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0.51388888888873",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3668520",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "1",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3709905",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "2",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3709904",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "3",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3531013",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "4",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3531014",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "5",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3512436",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "6",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0.0721393034825324",
                                            },
                                        },
                                    ],
                                },
                                linearExtension: {
                                    openlrExtendedLinear: {
                                        firstDirection: {
                                            openlrLocationReferencePoint: {
                                                openlrCoordinate: {
                                                    latitude: "49.711782870746283",
                                                    longitude: "15.067501923548",
                                                },
                                                openlrLineAttributes: {
                                                    openlrFunctionalRoadClass: "FRC0",
                                                    openlrFormOfWay: "motorway",
                                                    openlrBearing: "290",
                                                },
                                                openlrPathAttributes: {
                                                    openlrLowestFRCToNextLRPoint: "FRC0",
                                                    openlrDistanceToNextLRPoint: "2051",
                                                },
                                            },
                                            openlrLastLocationReferencePoint: {
                                                openlrCoordinate: {
                                                    latitude: "49.724098226340338",
                                                    longitude: "15.048652253666898",
                                                },
                                                openlrLineAttributes: {
                                                    openlrFunctionalRoadClass: "FRC0",
                                                    openlrFormOfWay: "motorway",
                                                    openlrBearing: "158",
                                                },
                                            },
                                            openlrOffsets: {
                                                openlrPositiveOffset: "261",
                                                openlrNegativeOffset: "1295",
                                            },
                                        },
                                    },
                                },
                            },
                            situationRecordExtension: {
                                situationRecordExtendedApproved: {
                                    safetyRelatedMessage: "True",
                                },
                            },
                            complianceOption: "mandatory",
                            roadOrCarriagewayOrLaneManagementType: "laneClosures",
                        },
                    ],
                },
                {
                    $: {
                        id: "e181f604-c4bf-4ea4-8958-625df3679878",
                        version: "5",
                    },
                    situationVersionTime: "2021-05-19T07:00:03+02:00",
                    headerInformation: {
                        confidentiality: "noRestriction",
                        informationStatus: "real",
                        urgency: "urgent",
                    },
                    situationRecord: [
                        {
                            $: {
                                version: "5",
                                "xsi:type": "RoadOrCarriagewayOrLaneManagement",
                                id: "e181f604-c4bf-4ea4-8958-625df3679878_RoadOrCarriagewayOrLaneManagement",
                            },
                            situationRecordCreationTime: "2021-05-19T07:00:09+02:00",
                            situationRecordVersionTime: "2021-05-19T07:00:03+02:00",
                            probabilityOfOccurrence: "riskOf",
                            source: {
                                sourceIdentification: "RSD",
                                sourceName: {
                                    values: {
                                        value: {
                                            $: {
                                                lang: "cs",
                                            },
                                        },
                                    },
                                },
                            },
                            validity: {
                                validityStatus: "definedByValidityTimeSpec",
                                validityTimeSpecification: {
                                    overallStartTime: "2021-05-19T07:00:00+02:00",
                                    overallEndTime: "2021-05-28T18:00:00+02:00",
                                },
                            },
                            impact: {
                                capacityRemaining: "0",
                            },
                            generalPublicComment: {
                                comment: {
                                    values: {
                                        value: {
                                            _:
                                                "silnice III/2385, v katastru obce Kladno, okr. Kladno, uzavřeno, " +
                                                "stavební práce, Od 19.05.2021 07:00 Do 28.05.2021 18:00, oprava " +
                                                "železničního přejezdu P27, Objížďka - bez rozlišení: přes: silnice " +
                                                "III/2384 (ulice Doberská), Vydal: Magistrát města Kladna",
                                            $: {
                                                lang: "cs",
                                            },
                                        },
                                    },
                                },
                            },
                            groupOfLocations: {
                                $: {
                                    "xsi:type": "Linear",
                                },
                                alertCLinear: {
                                    $: {
                                        "xsi:type": "AlertCMethod2Linear",
                                    },
                                    alertCLocationCountryCode: "CZ",
                                    alertCLocationTableNumber: "25",
                                    alertCLocationTableVersion: "8.0",
                                    alertCDirection: {
                                        alertCDirectionCoded: "negative",
                                    },
                                    alertCMethod2PrimaryPointLocation: {
                                        alertCLocation: {
                                            specificLocation: "16285",
                                        },
                                    },
                                    alertCMethod2SecondaryPointLocation: {
                                        alertCLocation: {
                                            specificLocation: "16285",
                                        },
                                    },
                                },
                                globalNetworkLinear: {
                                    networkVersion: {
                                        values: {
                                            value: {
                                                _: "20.12",
                                                $: {
                                                    lang: "cs",
                                                },
                                            },
                                        },
                                    },
                                    linearGeometryType: "continuous",
                                    startPoint: {
                                        sjtskPointCoordinates: {
                                            sjtskX: "-765636.514772355",
                                            sjtskY: "-1035238.7901638",
                                        },
                                    },
                                    endPoint: {
                                        sjtskPointCoordinates: {
                                            sjtskX: "-765685.312230293",
                                            sjtskY: "-1035422.89324626",
                                        },
                                    },
                                    linearWithinLinearGNElement: [
                                        {
                                            sectionId: "3662854",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "0",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0.0936536587595321",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3662855",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "1",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0.838270565604998",
                                            },
                                        },
                                    ],
                                },
                                linearExtension: {
                                    openlrExtendedLinear: {
                                        firstDirection: {
                                            openlrLocationReferencePoint: {
                                                openlrCoordinate: {
                                                    latitude: "50.128398921027205",
                                                    longitude: "14.090182332631597",
                                                },
                                                openlrLineAttributes: {
                                                    openlrFunctionalRoadClass: "FRC4",
                                                    openlrFormOfWay: "singleCarriageway",
                                                    openlrBearing: "189",
                                                },
                                                openlrPathAttributes: {
                                                    openlrLowestFRCToNextLRPoint: "FRC4",
                                                    openlrDistanceToNextLRPoint: "1904",
                                                },
                                            },
                                            openlrLastLocationReferencePoint: {
                                                openlrCoordinate: {
                                                    latitude: "50.113811823219017",
                                                    longitude: "14.077919721117326",
                                                },
                                                openlrLineAttributes: {
                                                    openlrFunctionalRoadClass: "FRC4",
                                                    openlrFormOfWay: "singleCarriageway",
                                                    openlrBearing: "32",
                                                },
                                            },
                                            openlrOffsets: {
                                                openlrPositiveOffset: "16",
                                                openlrNegativeOffset: "1698",
                                            },
                                        },
                                    },
                                },
                            },
                            complianceOption: "mandatory",
                            roadOrCarriagewayOrLaneManagementType: "roadClosed",
                        },
                        {
                            $: {
                                version: "5",
                                "xsi:type": "ConstructionWorks",
                                id: "e181f604-c4bf-4ea4-8958-625df3679878_ConstructionWorks",
                            },
                            situationRecordCreationTime: "2021-05-19T07:00:09+02:00",
                            situationRecordVersionTime: "2021-05-19T07:00:03+02:00",
                            probabilityOfOccurrence: "riskOf",
                            source: {
                                sourceIdentification: "RSD",
                                sourceName: {
                                    values: {
                                        value: {
                                            $: {
                                                lang: "cs",
                                            },
                                        },
                                    },
                                },
                            },
                            validity: {
                                validityStatus: "definedByValidityTimeSpec",
                                validityTimeSpecification: {
                                    overallStartTime: "2021-05-19T07:00:00+02:00",
                                    overallEndTime: "2021-05-28T18:00:00+02:00",
                                },
                            },
                            impact: {
                                capacityRemaining: "0",
                            },
                            generalPublicComment: {
                                comment: {
                                    values: {
                                        value: {
                                            _:
                                                "silnice III/2385, v katastru obce Kladno, okr. Kladno, uzavřeno, " +
                                                "stavební práce, Od 19.05.2021 07:00 Do 28.05.2021 18:00, oprava " +
                                                "železničního přejezdu P27, Objížďka - bez rozlišení: přes: " +
                                                "silnice III/2384 (ulice Doberská), Vydal: Magistrát města Kladna",
                                            $: {
                                                lang: "cs",
                                            },
                                        },
                                    },
                                },
                            },
                            groupOfLocations: {
                                $: {
                                    "xsi:type": "Linear",
                                },
                                alertCLinear: {
                                    $: {
                                        "xsi:type": "AlertCMethod2Linear",
                                    },
                                    alertCLocationCountryCode: "CZ",
                                    alertCLocationTableNumber: "25",
                                    alertCLocationTableVersion: "8.0",
                                    alertCDirection: {
                                        alertCDirectionCoded: "negative",
                                    },
                                    alertCMethod2PrimaryPointLocation: {
                                        alertCLocation: {
                                            specificLocation: "16285",
                                        },
                                    },
                                    alertCMethod2SecondaryPointLocation: {
                                        alertCLocation: {
                                            specificLocation: "16285",
                                        },
                                    },
                                },
                                globalNetworkLinear: {
                                    networkVersion: {
                                        values: {
                                            value: {
                                                _: "20.12",
                                                $: {
                                                    lang: "cs",
                                                },
                                            },
                                        },
                                    },
                                    linearGeometryType: "continuous",
                                    startPoint: {
                                        sjtskPointCoordinates: {
                                            sjtskX: "-765636.514772355",
                                            sjtskY: "-1035238.7901638",
                                        },
                                    },
                                    endPoint: {
                                        sjtskPointCoordinates: {
                                            sjtskX: "-765685.312230293",
                                            sjtskY: "-1035422.89324626",
                                        },
                                    },
                                    linearWithinLinearGNElement: [
                                        {
                                            sectionId: "3662854",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "0",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0.0936536587595321",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "1",
                                            },
                                        },
                                        {
                                            sectionId: "3662855",
                                            directionRelativeOnLinearSection: "aligned",
                                            orderOfSection: "1",
                                            fromPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0",
                                            },
                                            toPoint: {
                                                $: {
                                                    "xsi:type": "PercentageDistanceAlongLinearElement",
                                                },
                                                percentageDistanceAlong: "0.838270565604998",
                                            },
                                        },
                                    ],
                                },
                                linearExtension: {},
                            },
                            constructionWorkType: "constructionWork",
                        },
                    ],
                },
            ],
        },
    },
};
