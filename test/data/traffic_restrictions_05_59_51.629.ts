import { ITrafficRestrictions } from "#ie/transformations/TrafficRestrictionsInterface";

export const sourceDataTrafficRestrictions: { d2LogicalModel: ITrafficRestrictions } = {
    d2LogicalModel: {
        $: {
            modelBaseVersion: "2",
            xmlns: "http://datex2.eu/schema/2/2_0",
            "xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
        },
        exchange: { supplierIdentification: { country: "cz", nationalIdentifier: "DIC" } },
        payloadPublication: {
            $: { "xsi:type": "SituationPublication", lang: "cs" },
            publicationTime: "2021-06-08T05:59:51+02:00",
            publicationCreator: { country: "cz", nationalIdentifier: "NDIC" },
            situation: {
                $: { id: "9053a16c-87e1-4141-964e-39b607a168f8", version: "5" },
                situationVersionTime: "2021-06-08T05:59:45+02:00",
                headerInformation: { confidentiality: "noRestriction", informationStatus: "real", urgency: "urgent" },
                situationRecord: {
                    $: {
                        version: "5",
                        "xsi:type": "RoadOrCarriagewayOrLaneManagement",
                        id: "9053a16c-87e1-4141-964e-39b607a168f8_RoadOrCarriagewayOrLaneManagement",
                    },
                    situationRecordCreationTime: "2021-06-08T05:59:45+02:00",
                    situationRecordVersionTime: "2021-06-08T05:59:45+02:00",
                    probabilityOfOccurrence: "certain",
                    source: { sourceIdentification: "SSU", sourceName: { values: { value: { $: { lang: "cs" } } } } },
                    validity: {
                        validityStatus: "definedByValidityTimeSpec",
                        validityTimeSpecification: {
                            overallStartTime: "2021-06-08T09:00:00+02:00",
                            overallEndTime: "2021-06-08T13:00:00+02:00",
                        },
                    },
                    impact: { numberOfOperationalLanes: "2", originalNumberOfLanes: "2" },
                    generalPublicComment: {
                        comment: {
                            values: {
                                value: {
                                    _: `D1 ve směru Praha, mezi 81.1 a 52.3 km, v termínu od 08. 06. 2021 09:00 do 08. \
06. 2021 13:00, údržba a opravy telematiky, rozsah: zpevněná krajnice (1),přídatný pruh (2) - \
pro pomalá vozidla, připojovací, odbočovací, počet průjezdných pruhů: 2`,
                                    $: { lang: "cs" },
                                },
                            },
                        },
                    },
                    groupOfLocations: {
                        $: { "xsi:type": "Linear" },
                        alertCLinear: {
                            $: { "xsi:type": "AlertCMethod2Linear" },
                            alertCLocationCountryCode: "CZ",
                            alertCLocationTableNumber: "25",
                            alertCLocationTableVersion: "8.0",
                            alertCDirection: { alertCDirectionCoded: "negative" },
                            alertCMethod2PrimaryPointLocation: { alertCLocation: { specificLocation: "25546" } },
                            alertCMethod2SecondaryPointLocation: { alertCLocation: { specificLocation: "25546" } },
                        },
                        linearWithinLinearElement: {
                            directionRelativeOnLinearSection: "opposite",
                            linearElement: { roadNumber: "D1" },
                            fromPoint: { $: { "xsi:type": "DistanceFromLinearElementStart" }, distanceAlong: "81099" },
                            toPoint: { $: { "xsi:type": "DistanceFromLinearElementStart" }, distanceAlong: "52300" },
                        },
                        globalNetworkLinear: {
                            networkVersion: { values: { value: { _: "20.12", $: { lang: "cs" } } } },
                            linearGeometryType: "continuous",
                            startPoint: { sjtskPointCoordinates: { sjtskX: "-690296", sjtskY: "-1106582" } },
                            endPoint: { sjtskPointCoordinates: { sjtskX: "-705078", sjtskY: "-1085568" } },
                            linearWithinLinearGNElement: [
                                {
                                    sectionId: "3656649",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "0",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0.0271247739603447",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3721423",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "1",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3721422",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "2",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1362974",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "3",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1657166",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "4",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1657167",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "5",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3070107",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "6",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3070109",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "7",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "2586318",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "8",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716936",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "9",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716919",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "10",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716925",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "11",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1362919",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "12",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1362916",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "13",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716920",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "14",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3512137",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "15",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3530265",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "16",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1362911",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "17",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1362884",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "18",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3661066",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "19",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3721442",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "20",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3571819",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "21",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1419782",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "22",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532142",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "23",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532140",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "24",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532143",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "25",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3707315",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "26",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3707316",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "27",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3661017",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "28",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3102875",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "29",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3512428",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "30",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3530262",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "31",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3530261",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "32",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1482787",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "33",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1482785",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "34",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3513643",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "35",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3530258",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "36",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3512427",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "37",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3530259",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "38",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3503886",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "39",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3503888",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "40",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716762",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "41",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3530260",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "42",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3512429",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "43",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3720895",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "44",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3720896",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "45",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3668308",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "46",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3503890",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "47",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3668307",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "48",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3720835",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "49",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3720834",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "50",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3709900",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "51",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3709901",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "52",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3667995",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "53",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3068859",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "54",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "2015268",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "55",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3720829",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "56",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3720831",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "57",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3668462",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "58",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532144",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "59",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1397926",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "60",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1397927",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "61",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1397928",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "62",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3503892",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "63",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3503893",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "64",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "2577677",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "65",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3513659",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "66",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532145",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "67",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1355578",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "68",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716299",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "69",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "2577671",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "70",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "2577673",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "71",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716287",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "72",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716286",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "73",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716285",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "74",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716281",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "75",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716280",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "76",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1355457",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "77",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1355454",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "78",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716278",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "79",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716275",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "80",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3068870",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "81",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3068872",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "82",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "2577669",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "83",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716255",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "84",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1355354",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "85",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "2577664",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "86",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "2577665",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "87",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3531829",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "88",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532146",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "89",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532147",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "90",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3512437",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "91",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3531012",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "92",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3668520",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "93",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3709905",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "94",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3709904",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "95",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3531013",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "96",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3531014",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "97",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3512436",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "98",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3512435",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "99",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532148",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "100",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3503362",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "101",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3456702",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "102",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1397905",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "103",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1397906",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "104",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1397907",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "105",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1657605",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "106",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1657606",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "107",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532149",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "108",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3512455",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "109",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "2577661",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "110",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3068866",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "111",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3068868",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "112",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1355064",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "113",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "1355058",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "114",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "716179",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "115",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3531827",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "116",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532150",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "117",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "2577653",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "118",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3513658",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "119",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532151",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "120",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3532152",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "121",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "1",
                                    },
                                },
                                {
                                    sectionId: "3513660",
                                    directionRelativeOnLinearSection: "aligned",
                                    orderOfSection: "122",
                                    fromPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0",
                                    },
                                    toPoint: {
                                        $: { "xsi:type": "PercentageDistanceAlongLinearElement" },
                                        percentageDistanceAlong: "0.521568627450975",
                                    },
                                },
                            ],
                        },
                        linearExtension: {
                            openlrExtendedLinear: {
                                firstDirection: {
                                    openlrLocationReferencePoint: [
                                        {
                                            openlrCoordinate: {
                                                latitude: "49.575376848916243",
                                                longitude: "15.268410988738133",
                                            },
                                            openlrLineAttributes: {
                                                openlrFunctionalRoadClass: "FRC0",
                                                openlrFormOfWay: "motorway",
                                                openlrBearing: "323",
                                            },
                                            openlrPathAttributes: {
                                                openlrLowestFRCToNextLRPoint: "FRC0",
                                                openlrDistanceToNextLRPoint: "14926",
                                            },
                                        },
                                        {
                                            openlrCoordinate: {
                                                latitude: "49.648245799965053",
                                                longitude: "15.124349285544502",
                                            },
                                            openlrLineAttributes: {
                                                openlrFunctionalRoadClass: "FRC0",
                                                openlrFormOfWay: "motorway",
                                                openlrBearing: "349",
                                            },
                                            openlrPathAttributes: {
                                                openlrLowestFRCToNextLRPoint: "FRC0",
                                                openlrDistanceToNextLRPoint: "680",
                                            },
                                        },
                                        {
                                            openlrCoordinate: {
                                                latitude: "49.654258697398909",
                                                longitude: "15.122605342742869",
                                            },
                                            openlrLineAttributes: {
                                                openlrFunctionalRoadClass: "FRC0",
                                                openlrFormOfWay: "motorway",
                                                openlrBearing: "349",
                                            },
                                            openlrPathAttributes: {
                                                openlrLowestFRCToNextLRPoint: "FRC0",
                                                openlrDistanceToNextLRPoint: "13833",
                                            },
                                        },
                                        {
                                            openlrCoordinate: {
                                                latitude: "49.752005371605868",
                                                longitude: "15.023842969790108",
                                            },
                                            openlrLineAttributes: {
                                                openlrFunctionalRoadClass: "FRC0",
                                                openlrFormOfWay: "motorway",
                                                openlrBearing: "304",
                                            },
                                            openlrPathAttributes: {
                                                openlrLowestFRCToNextLRPoint: "FRC0",
                                                openlrDistanceToNextLRPoint: "515",
                                            },
                                        },
                                    ],
                                    openlrLastLocationReferencePoint: {
                                        openlrCoordinate: { latitude: "49.753499655777311", longitude: "15.017182552238062" },
                                        openlrLineAttributes: {
                                            openlrFunctionalRoadClass: "FRC0",
                                            openlrFormOfWay: "motorway",
                                            openlrBearing: "92",
                                        },
                                    },
                                    openlrOffsets: { openlrPositiveOffset: "972", openlrNegativeOffset: "246" },
                                },
                            },
                        },
                    },
                    complianceOption: "mandatory",
                    roadOrCarriagewayOrLaneManagementType: "laneClosures",
                },
            },
        },
    },
};
