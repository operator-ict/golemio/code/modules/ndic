// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const http = require("http");
const fs = require("fs");
const express = require("express");
const { ContainerToken, OutputGatewayContainer } = require("@golemio/core/dist/output-gateway/ioc");
const { AMQPConnector } = require("@golemio/core/dist/input-gateway/connectors");

const app = express();
const server = http.createServer(app);
const postgresConnector = OutputGatewayContainer.resolve(ContainerToken.PostgresDatabase);

const bodyParser = require("body-parser");
require("body-parser-xml")(bodyParser);
const xml2js = require("xml2js");

const start = async () => {
    await AMQPConnector.connect();
    await postgresConnector.connect();

    app.use((req, res, next) => {
        let inputData = fs.readFileSync(path.resolve(__dirname, "../../data/traffic_restrictions_data.xml"), {
            encoding: "utf-8",
        });
        if (req.path === "ndic/traffic-info") {
            inputData = fs.readFileSync(path.resolve(__dirname, "../../data/traffic_info_data.xml"), {
                encoding: "utf-8",
            });
        }

        const parser = new xml2js.Parser({
            strict: true,
            explicitArray: false,
            normalize: false,
            normalizeTags: false,
        });
        parser.parseString(inputData, function (err, result) {
            req.body = result;
            if (err) {
                log.silly("Error caught by the router error handler.");
            }
            next();
        });
    });
    const { ndicRouter } = require("../../../src/input-gateway/NdicRouter");

    app.use("/ndic", ndicRouter);

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await postgresConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};
