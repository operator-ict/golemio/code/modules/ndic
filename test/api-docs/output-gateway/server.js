// Load reflection lib
require("@golemio/core/dist/shared/_global");

const path = require("path");
require("ts-node").register({ project: path.resolve(process.cwd(), "tsconfig.json") });
require("tsconfig-paths").register();

const http = require("http");
const express = require("express");
const { ContainerToken, OutputGatewayContainer } = require("@golemio/core/dist/output-gateway/ioc");

const app = express();
const server = http.createServer(app);
const postgresConnector = OutputGatewayContainer.resolve(ContainerToken.PostgresDatabase);

const start = async () => {
    await postgresConnector.connect();

    const { trafficRouter } = require("#og/TrafficRouter");

    app.use("/v2/traffic", trafficRouter);

    return new Promise((resolve) => {
        server.listen(3011, () => {
            resolve();
        });
    });
};

const stop = async () => {
    await postgresConnector.disconnect();
    server.close();
};

module.exports = {
    start,
    stop,
};
