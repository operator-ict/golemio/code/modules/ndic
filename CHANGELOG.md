# Changelog

All notable changes to this project will be documented in this file.

## [Unreleased]

## [1.4.2] - 2025-03-12

### Fixed

-   handle missing `openlrExtendedLinear` in `linearExtension`([ndic#16]https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/issues/16)

## [1.4.1] - 2025-03-03

### Changed

-   Optimize `/v2/traffic/restrictions` ([ndic#15](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/issues/15))

### Security

-   Fix `/v2/traffic/restrictions` SQL injection

## [1.4.0] - 2024-11-26

### Changed

-   OG part of generation of osm path on global network ([p0131#168](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/168))

## [1.3.1] - 2024-10-16

### Added

-   AsyncAPI documentation ([integration-engine#262](https://gitlab.com/operator-ict/golemio/code/integration-engine/-/issues/262))

## [1.3.0] - 2024-09-12

### Added

-   generation of osm path based on global network data IE part ([p0131#168](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/168))

### Changed

-   `.gitlab-ci.yml` cleanup ([devops#320](https://gitlab.com/operator-ict/golemio/devops/infrastructure/-/issues/320))

## [1.2.10] - 2024-08-16

### Added

-   add backstage metadata files
-   add .gitattributes file

## [1.2.9] - 2024-07-29

### Fixed

-   `error while saving to db` error by adding `situation_version` to PK ([ndic#14](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/issues/14))
-   Incorrect Sequelize data types

## [1.2.8] - 2024-06-03

### Added

-   add cache-control header to all responses ([core#106](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/106))

### Removed

-   remove redis useCacheMiddleware ([core#106](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/106))

## [1.2.7] - 2024-05-13

### Changed

-   Update Node.js to v20.12.2 Express to v4.19.2 ([core#102](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/102))

## [1.2.6] - 2024-04-10

### Added

-   Added new attributes to situation record output ([p0131#163](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/163))

## [1.2.5] - 2024-03-25

### Fixed

-   Fixed router query validation rules ([core#93](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/93))

## [1.2.4] - 2024-02-12

### Changed

-   retrospective addition of table comments ([ipt#131](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/161))

## [1.2.3] - 2023-10-26

### Changed

-   Change rsd_tmc_points schema to traffic ([schema-definitions#55](https://gitlab.com/operator-ict/golemio/code/modules/schema-definitions/-/issues/55))

## [1.2.2] - 2023-09-27

### Changed

-   API versioning - version moved to path ([core#80](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/80))

## [1.2.1] - 2023-09-04

### Changed

-   Add input gateway documentation ([ndic#11](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/issues/11))

## [1.2.0] - 2023-08-23

### Changed

-   Replace moment with DateTime wrapper ([core#68](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/68))
-   Run integration apidocs tests via Golemio CLI ([core#46](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/46))

## [1.1.9] - 2023-06-26

### Fixed

-   RsdTmcPointsModel changed parent class to integration engine model

## [1.1.8] - 2023-06-21

### Changed

-   Flip ltStart and ltEnd points

### Removed

-   Removed OSM path reversing

## [1.1.7] - 2023-06-14

### Changed

-   Typescript version update to 5.1.3 ([core#70](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/70))

## [1.1.6] - 2023-06-12

-   hotfix filter regions data ([ipt#138](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/138))

## [1.1.5] - 2023-05-31

### Changed

-   Update golemio errors ([core#62](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/62))
-   IE & OG handling of data for traffic restrictions from regions ([ipt#128](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/128))

## [1.1.4] - 2023-05-29

### Added

-   Ads new IG endpoint for traffic restrictions ([ipt#129](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/129))

### Changed

-   Use RsdTmcOsmMappingRepository from traffic-common module ([ipt#123](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/123))

## [1.1.3] - 2023-03-05

### Changed

-   Adjust IG logger emitter imports

## [1.1.2] - 2023-02-27

### Changed

-   Update Node.js to v18.14.0, Express to v4.18.2 ([core#50](https://gitlab.com/operator-ict/golemio/code/modules/core/-/issues/50))

## [1.1.1] - 2023-02-13

### Fixed

-   Updated documentation to match reality and improve description.

## [1.1.0] - 2023-01-23

### Changed

-   Migrate to npm

## [1.0.14] - 2023-01-04

### Fixed

-   Fix pagination / output filtering

## [1.0.13] - 2022-11-29

### Changed

-   Update gitlab-ci template

## [1.0.12] - 2022-10-11

### Changed

-   Fixed compliance with IDM API specs

## [1.0.11] - 2022-09-21

### Removed

-   Unused dependencies ([general#5](https://gitlab.com/operator-ict/golemio/code/modules/general/-/issues/5))

## [1.0.10] - 2022-06-21

### Changed

-   Typescript version update from 4.4.4 to 4.6.4

## [1.0.9] - 2022-05-18

### Added

-   Prepared schemas and mappers for strict validations([ndic#50](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/50))

## [1.0.8] - 2022-05-02

### Added

-   Migrations for postgre database([ndic#8](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/issues/8))

### Fixed

-   Output date format and tests ([intermodal#4](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/45))

## [1.0.7] - 2022-04-07

### Fixed

-   Query to get actual data for output API ([intermodal#40](https://gitlab.com/operator-ict/golemio/projekty/oict/p0131.-data-pro-intermod-ln-naviga-n-syst-m/-/issues/40))

## [1.0.5] - 2021-11-29

### Fixed

-   FGeneralPublicComment parsing error ([ndic#4](https://gitlab.com/operator-ict/golemio/code/modules/ndic/-/issues/4))

-   In output API do not show records that do not contain ALERTS data in the OUTPUT API ([d0092-jsdi-ndic#13](https://gitlab.com/operator-ict/golemio/projekty/rsd/d0092-jsdi-ndic/-/issues/13#note_741387901))

